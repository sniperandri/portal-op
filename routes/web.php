<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*-----------------index-----------------------*/
Auth::routes(['verify'=>true]);
Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
});

Route::get('kirim-email',[
    'uses' => 'TestController@kirim',
    'as'    => 'kirim'
]);

Route::get('/home',[
    'uses'  => 'HomeController@index',
    'as'    => 'home' ,
    'middleware' => ['role:superadmin|keuangan|kepegawaian|humas|renpro|desain|tarif|lala|fasilitas|bimus']
]);

Route::get('/verifikasi-sukses','HomeController@verifikasi');
/*--------------------end of index-------------*/

/*-------------------backend-------------------*/
Route::group(['prefix'=>'admin', 'middleware'=>['auth'],'as'=>'admin.'], function(){
    Route::group(['middleware'=>['role:superadmin|humas']], function(){
        Route::resource('kategori-berita', 'Backend\KategoriBeritaController');
        Route::resource('kategori-foto', 'Backend\KategoriFotoController');
        Route::resource('kategori-video', 'Backend\KategoriVideoController');
        Route::resource('kategori-hukum', 'Backend\KategoriHukumController');
        Route::resource('jenis-usaha', 'Backend\JenisUsahaController');
        Route::resource('jenis-pelayanan', 'Backend\JenisPelayananController');
        Route::resource('jenis-informasi', 'Backend\JenisInformasiController');
        Route::resource('jenis-laporan', 'Backend\JenisLaporanController');
        Route::resource('tampilan-depan', 'Backend\TampilanDepanController');
        Route::resource('berita', 'Backend\BeritaController');
        Route::put('berita/restore/{id}',[
            'uses' => 'Backend\BeritaController@restore',
            'as' => 'berita.restore'
        ]);
        Route::delete('berita/force-destroy/{id}',[
            'uses' => 'Backend\BeritaController@forceDestroy',
            'as' => 'berita.force-destroy'
        ]);
        Route::resource('link-terkait', 'Backend\LinkTerkaitController');
        Route::put('link-terkait/restore/{id}',[
            'uses' => 'Backend\LinkTerkaitController@restore',
            'as' => 'link-terkait.restore'
        ]);
        Route::delete('link-terkait/force-destroy/{id}',[
            'uses' => 'Backend\LinkTerkaitController@forceDestroy',
            'as' => 'link-terkait.force-destroy'
        ]);
        // Route::resource('reformasi-birokrasi', 'Backend\ReformasiBirokrasiController');
        // Route::put('reformasi-birokrasi/restore/{id}',[
        //     'uses' => 'Backend\ReformasiBirokrasiController@restore',
        //     'as' => 'reformasi-birokrasi.restore'
        // ]);
        // Route::delete('reformasi-birokrasi/force-destroy/{id}',[
        //     'uses' => 'Backend\ReformasiBirokrasiController@forceDestroy',
        //     'as' => 'reformasi-birokrasi.force-destroy'
        // ]);
        Route::resource('profil', 'Backend\ProfilController');
        Route::put('profil/restore/{id}',[
            'uses' => 'Backend\ProfilController@restore',
            'as' => 'profil.restore'
        ]);
        Route::delete('profil/force-destroy/{id}',[
            'uses' => 'Backend\ProfilController@forceDestroy',
            'as' => 'profil.force-destroy'
        ]);
        Route::resource('galeri-foto', 'Backend\GaleriFotoController');
        Route::put('galeri-foto/restore/{id}',[
            'uses' => 'Backend\GaleriFotoController@restore',
            'as' => 'galeri-foto.restore'
        ]);
        Route::delete('galeri-foto/force-destroy/{id}',[
            'uses' => 'Backend\GaleriFotoController@forceDestroy',
            'as' => 'galeri-foto.force-destroy'
        ]);
        Route::resource('galeri-video', 'Backend\GaleriVideoController');
        Route::put('galeri-video/restore/{id}',[
            'uses' => 'Backend\GaleriVideoController@restore',
            'as' => 'galeri-video.restore'
        ]);
        Route::delete('galeri-video/force-destroy/{id}',[
            'uses' => 'Backend\GaleriVideoController@forceDestroy',
            'as' => 'galeri-video.force-destroy'
        ]);
        Route::resource('pengaduan', 'Backend\PengaduanController');
        Route::put('pengaduan/restore/{id}',[
            'uses' => 'Backend\PengaduanController@restore',
            'as' => 'pengaduan.restore'
        ]);
        Route::delete('pengaduan/force-destroy/{id}',[
            'uses' => 'Backend\PengaduanController@forceDestroy',
            'as' => 'pengaduan.force-destroy'
        ]);

        Route::resource('ppid', 'Backend\PpidController');
        Route::put('ppid/restore/{id}',[
            'uses' => 'Backend\PpidController@restore',
            'as' => 'ppid.restore'
        ]);
        
        Route::delete('ppid/force-destroy/{id}',[
            'uses' => 'Backend\PpidController@forceDestroy',
            'as' => 'ppid.force-destroy'
        ]);
        Route::get('info/{data}', [
            'uses'=>'Backend\InformasiController@index',
            'as'    => 'info.informasi',
        ]);
        Route::get('info-ppid/{data}', [
            'uses'=>'Backend\InformasiController@index',
            'as'    => 'info.informasi',
        ]);

        Route::resource('sikk', 'Backend\SikkController');
        Route::put('sikk/restore/{id}',[
            'uses' => 'Backend\SikkController@restore',
            'as' => 'sikk.restore'
        ]);
        Route::delete('sikk/force-destroy/{id}',[
            'uses' => 'Backend\SikkController@forceDestroy',
            'as' => 'sikk.force-destroy'
        ]);
        Route::resource('pmku', 'Backend\PmkuController');
        Route::put('pmku/restore/{id}',[
            'uses' => 'Backend\PmkuController@restore',
            'as' => 'pmku.restore'
        ]);
        Route::delete('pmku/force-destroy/{id}',[
            'uses' => 'Backend\PmkuController@forceDestroy',
            'as' => 'pmku.force-destroy'
        ]);
        Route::resource('pelaporan', 'Backend\PelaporanController');
        Route::put('pelaporan/restore/{id}',[
            'uses' => 'Backend\PelaporanController@restore',
            'as' => 'pelaporan.restore'
        ]);
        Route::delete('pelaporan/force-destroy/{id}',[
            'uses' => 'Backend\PelaporanController@forceDestroy',
            'as' => 'pelaporan.force-destroy'
        ]);
    });
    Route::group(['middleware'=>['role:superadmin|fasilitas|humas']], function(){
        Route::get('fasilitas/{data}', [
            'uses'=>'Backend\InformasiController@index',
            'as'    => 'info.informasi',
        ]);
        Route::resource('informasi', 'Backend\InformasiController');
        Route::put('informasi/restore/{id}',[
            'uses' => 'Backend\InformasiController@restore',
            'as' => 'informasi.restore'
        ]);
        Route::delete('informasi/force-destroy/{id}',[
            'uses' => 'Backend\InformasiController@forceDestroy',
            'as' => 'informasi.force-destroy'
        ]);
        // Route::resource('pelayanan', 'Backend\PelayananController');
        // Route::put('pelayanan/restore/{id}',[
        //     'uses' => 'Backend\PelayananController@restore',
        //     'as' => 'pelayanan.restore'
        // ]);
        // Route::delete('pelayanan/force-destroy/{id}',[
        //     'uses' => 'Backend\PelayananController@forceDestroy',
        //     'as' => 'pelayanan.force-destroy'
        // ]);

        /*-------------------pandawa----------------------*/
        Route::resource('soptu', 'Backend\SoptuController');
        Route::get('sop-tata-usaha', [
            'uses'=>'Backend\SoptuController@index',
            'as'    => 'sop.tu',
        ]);
        Route::put('soptu/restore/{id}',[
            'uses' => 'Backend\SoptuController@restore',
            'as' => 'soptu.restore'
        ]);
        Route::delete('soptu/force-destroy/{id}',[
            'uses' => 'Backend\SoptuController@forceDestroy',
            'as' => 'soptu.force-destroy'
        ]);

        Route::resource('soprenbang', 'Backend\SoprenbangController');
        Route::get('sop-rencana-pengembangan', [
            'uses'=>'Backend\SoprenbangController@index',
            'as'    => 'sop.renbang',
        ]);
        Route::put('soprenbang/restore/{id}',[
            'uses' => 'Backend\SoprenbangController@restore',
            'as' => 'soprenbang.restore'
        ]);
        Route::delete('soprenbang/force-destroy/{id}',[
            'uses' => 'Backend\SoprenbangController@forceDestroy',
            'as' => 'soprenbang.force-destroy'
        ]);

        Route::resource('soplala', 'Backend\SoplalaController');
        Route::get('sop-lalu-lintas', [
            'uses'=>'Backend\SoplalaController@index',
            'as'    => 'sop.lala',
        ]);
        Route::put('soplala/restore/{id}',[
            'uses' => 'Backend\SoplalaController@restore',
            'as' => 'soplala.restore'
        ]);
        Route::delete('soplala/force-destroy/{id}',[
            'uses' => 'Backend\SoplalaController@forceDestroy',
            'as' => 'soplala.force-destroy'
        ]);

        Route::resource('rapat-bidang', 'Backend\RapatBidangController');
        Route::get('rapat-bidang', [
            'uses'=>'Backend\RapatBidangController@index',
            'as'    => 'rapat-bidang',
        ]);
        Route::put('rapat-bidang/restore/{id}',[
            'uses' => 'Backend\RapatBidangController@restore',
            'as' => 'rapat-bidang.restore'
        ]);
        Route::delete('rapat-bidang/force-destroy/{id}',[
            'uses' => 'Backend\RapatBidangController@forceDestroy',
            'as' => 'rapat-bidang.force-destroy'
        ]);

        Route::resource('rapat-lembaga', 'Backend\RapatLembagaController');
        Route::get('rapat-lembaga', [
            'uses'=>'Backend\RapatLembagaController@index',
            'as'    => 'rapat-lembaga',
        ]);
        Route::put('rapat-lembaga/restore/{id}',[
            'uses' => 'Backend\RapatLembagaController@restore',
            'as' => 'rapat-lembaga.restore'
        ]);
        Route::delete('rapat-lembaga/force-destroy/{id}',[
            'uses' => 'Backend\RapatLembagaController@forceDestroy',
            'as' => 'rapat-lembaga.force-destroy'
        ]);

        Route::resource('pelayanan-pnbp', 'Backend\PelayananPnbpController');
        Route::get('pelayanan-pnbp', [
            'uses'=>'Backend\PelayananPnbpController@index',
            'as'    => 'pelayanan-pnbp',
        ]);
        Route::put('pelayanan-pnbp/restore/{id}',[
            'uses' => 'Backend\PelayananPnbpController@restore',
            'as' => 'pelayanan-pnbp.restore'
        ]);
        Route::delete('pelayanan-pnbp/force-destroy/{id}',[
            'uses' => 'Backend\PelayananPnbpController@forceDestroy',
            'as' => 'pelayanan-pnbp.force-destroy'
        ]);

        Route::resource('pelayanan-pmku', 'Backend\PelayananPmkuController');
        Route::get('pelayanan-pmku', [
            'uses'=>'Backend\PelayananPmkuController@index',
            'as'    => 'pelayanan-pmku',
        ]);
        Route::put('pelayanan-pmku/restore/{id}',[
            'uses' => 'Backend\PelayananPmkuController@restore',
            'as' => 'pelayanan-pmku.restore'
        ]);
        Route::delete('pelayanan-pmku/force-destroy/{id}',[
            'uses' => 'Backend\PelayananPmkuController@forceDestroy',
            'as' => 'pelayanan-pmku.force-destroy'
        ]);

        Route::resource('pelayanan-inaportnet', 'Backend\PelayananInaportnetController');
        Route::get('pelayanan-inaportnet', [
            'uses'=>'Backend\PelayananInaportnetController@index',
            'as'    => 'pelayanan-inaportnet',
        ]);
        Route::put('pelayanan-inaportnet/restore/{id}',[
            'uses' => 'Backend\PelayananInaportnetController@restore',
            'as' => 'pelayanan-inaportnet.restore'
        ]);
        Route::delete('pelayanan-inaportnet/force-destroy/{id}',[
            'uses' => 'Backend\PelayananInaportnetController@forceDestroy',
            'as' => 'pelayanan-inaportnet.force-destroy'
        ]);
        /*-------------------end of pandawa---------------*/
    });
});

Route::get('kategori-berita-data', ['as'=>'admin.kategori-berita.data','uses'=>'Backend\KategoriBeritaController@getData']);
Route::get('kategori-foto-data', ['as'=>'admin.kategori-foto.data','uses'=>'Backend\KategoriFotoController@getData']);
Route::get('kategori-video-data', ['as'=>'admin.kategori-video.data','uses'=>'Backend\KategoriVideoController@getData']);
Route::get('kategori-hukum-data', ['as'=>'admin.kategori-hukum.data','uses'=>'Backend\KategoriHukumController@getData']);
Route::get('tampilan-depan-data', ['as'=>'admin.tampilan-depan.data','uses'=>'Backend\TampilanDepanController@getData']);
Route::get('jenis-usaha-data', ['as'=>'admin.jenis-usaha.data','uses'=>'Backend\JenisUsahaController@getData']);
Route::get('jenis-pelayanan-data', ['as'=>'admin.jenis-pelayanan.data','uses'=>'Backend\JenisPelayananController@getData']);
Route::get('jenis-informasi-data', ['as'=>'admin.jenis-informasi.data','uses'=>'Backend\JenisInformasiController@getData']);
Route::get('jenis-laporan-data', ['as'=>'admin.jenis-laporan.data','uses'=>'Backend\JenisLaporanController@getData']);


/*-------------------enf of backend-------------*/

/*-------------------frontend-------------------*/
Route::get('/',[
    'uses' => 'Frontend\TampilanDepanController@index',
    'as'    => 'tampilan-depan.index'
]);

Route::resource('berita', 'Frontend\BeritaController');
Route::get('/kategori-berita/{category}', [
    'uses' => 'Frontend\BeritaController@category',
    'as' => 'frontend.berita.kategori',
]);

Route::get('/profil/sejarah',[
    'uses'  => 'Frontend\ProfilController@sejarah',
    'as'    => 'profil.sejarah',
]);
Route::get('/profil/struktur',[
    'uses'  => 'Frontend\ProfilController@struktur',
    'as'    => 'profil.struktur',
]);
Route::get('/profil/visi-misi',[
    'uses'  => 'Frontend\ProfilController@visimisi',
    'as'    => 'profil.visimisi',
]);
Route::get('/profil/tupoksi',[
    'uses'  => 'Frontend\ProfilController@tupoksi',
    'as'    => 'profil.tupoksi',
]);
Route::resource('profil', 'Frontend\ProfilController');

Route::get('/galeri-foto',[
    'uses'  => 'Frontend\TampilanDepanController@galeriFoto',
    'as'    => 'galeri-foto', 
]);

Route::get('/galeri-video',[
    'uses'  => 'Frontend\TampilanDepanController@galeriVideo',
    'as'    => 'galeri-video', 
]);

Route::resource('pengaduan', 'Frontend\PengaduanController');

// Route::get('/reformasi-birokrasi',[
//     'uses'  => 'Frontend\ReformasiBirokrasiController@index',
//     'as'    => 'reformasi-birokrasi'
// ]);

Route::get('/ppid',[
    'uses'  => 'Frontend\PpidController@index',
    'as'    => 'ppid'
]);

Route::post('/ppid',[
    'uses'  => 'Frontend\PpidController@store',
    'as'    => 'ppid.store'
]);

Route::get('/ppid/form',[
    'uses'  => 'Frontend\PpidController@form',
    'as'    => 'ppid.form',
]);

Route::get('/ppid/dasar-hukum',[
    'uses'  => 'Frontend\PpidController@dasarHukum',
    'as'    => 'ppid.dasar-hukum',
]);

Route::get('/ppid/profil',[
    'uses'  => 'Frontend\PpidController@profil',
    'as'    => 'ppid.profil',
]);

Route::get('/ppid/maklumat-pelayanan',[
    'uses'  => 'Frontend\PpidController@maklumatPelayanan',
    'as'    => 'ppid.maklumat-pelayanan',
]);

Route::get('/ppid/standar-layanan',[
    'uses'  => 'Frontend\PpidController@standarLayanan',
    'as'    => 'ppid.standar-layanan',
]);

Route::get('/ppid/simpul-layanan',[
    'uses'  => 'Frontend\PpidController@simpulLayanan',
    'as'    => 'ppid.simpul-layanan',
]);

Route::get('/ppid/jumlah-permintaan-informasi',[
    'uses'  => 'Frontend\PpidController@jumlahPermintaanInformasi',
    'as'    => 'ppid.jumlah-permintaan-informasi',
]);

Route::get('/ppid/prosedur-permohonan',[
    'uses'  => 'Frontend\PpidController@prosedurPermohonan',
    'as'    => 'ppid.prosedur-permohonan',
]);

Route::get('/ppid/tata-cara-memperoleh-informasi-publik',[
    'uses'  => 'Frontend\PpidController@tataCaraInformasi',
    'as'    => 'ppid.tata-cara-memperoleh-informasi-publik',
]);

Route::get('/ppid/tata-cara-pengajuan-keberatan',[
    'uses'  => 'Frontend\PpidController@tataCaraKeberatan',
    'as'    => 'ppid.tata-cara-pengajuan-keberatan',
]);

Route::get('/ppid/hak-dan-kewajiban-badan-publik',[
    'uses'  => 'Frontend\PpidController@hakKewajibanBadanPublik',
    'as'    => 'ppid.hak-dan-kewajiban-badan-publik',
]);

Route::get('/ppid/hak-dan-kewajiban-pemohon-informasi',[
    'uses'  => 'Frontend\PpidController@hakKewajibanPemohon',
    'as'    => 'ppid.hak-dan-kewajiban-pemohon-informasi',
]);

Route::get('/ppid/formulir-permohonan-informasi',[
    'uses'  => 'Frontend\PpidController@formulirPermohonan',
    'as'    => 'ppid.formulir-permohonan',
]);

Route::get('/fasilitas-pelabuhan',[
    'uses'  => 'Frontend\FasilitasController@index',
    'as'    => 'fasilitas.index'
]);

Route::get('/fasilitas-pelabuhan/batas-dlkr-dlkp',[
    'uses'  => 'Frontend\FasilitasController@batas',
    'as'    => 'fasilitas.batas',
]);

Route::get('/fasilitas-pelabuhan/rekapitulasi-fasilitas-dan-peralatan',[
    'uses'  => 'Frontend\FasilitasController@rekapitulasi',
    'as'    => 'fasilitas.rekapitulasi',
]);

Route::get('/fasilitas-pelabuhan/fasilitas-dermaga',[
    'uses'  => 'Frontend\FasilitasController@dermaga',
    'as'    => 'fasilitas.dermaga',
]);

Route::get('/fasilitas-pelabuhan/fasilitas-gudang',[
    'uses'  => 'Frontend\FasilitasController@gudang',
    'as'    => 'fasilitas.gudang',
]);

Route::get('/fasilitas-pelabuhan/fasilitas-lapangan-penumpukan',[
    'uses'  => 'Frontend\FasilitasController@lapangan',
    'as'    => 'fasilitas.lapangan',
]);

Route::get('/fasilitas-pelabuhan/daerah-labuh',[
    'uses'  => 'Frontend\FasilitasController@daerahLabuh',
    'as'    => 'fasilitas.daerah-labuh',
]);

Route::get('/fasilitas-pelabuhan/breakwater',[
    'uses'  => 'Frontend\FasilitasController@breakwater',
    'as'    => 'fasilitas.breakwater',
]);
Route::get('/fasilitas-pelabuhan/rencana-induk-pelabuhan',[
    'uses'  => 'Frontend\FasilitasController@rencanaInduk',
    'as'    => 'fasilitas.rencana-induk',
]);

Route::get('/registrasi',[
    'uses'  => 'Frontend\RegistrasiController@index',
    'as'    => 'registrasi' 
]);

Route::get('/registrasi-perusahaan',[
    'uses'  => 'Frontend\RegistrasiController@tipePerusahaan',
    'as'    => 'registrasi.tipe-perusahaan'
]);

Route::post('/registrasi-perusahaan',[
    'uses'  => 'Frontend\RegistrasiController@tipePerusahaan',
    'as'    => 'registrasi.tipe-perusahaan'
]);

Route::post('/registrasi-pmku',[
    'uses'  => 'Frontend\RegistrasiController@pmku',
    'as'    => 'registrasi.pmku'
]);

Route::post('/registrasi-pmku-save',[
    'uses'  => 'Frontend\RegistrasiController@store',
    'as'    => 'registrasi.store'
]);

Route::post('/registrasi-pmku-cek-siupkk',[
    'uses'  => 'Frontend\RegistrasiController@cekSiupkk',
    'as'    => 'registrasi.cek-siupkk' 
]);

Route::get('/registrasi-pmku-cek-nib',[
    'uses'  => 'Frontend\RegistrasiController@cekNib',
    'as'    => 'registrasi.cek-nib' 
]);

Route::get('/sop',[
    'uses'  => 'Frontend\PelayananController@sop',
    'as'    => 'pelayanan.sop',
]);

Route::get('/rekomendasi',[
    'uses'  => 'Frontend\PelayananController@rekomendasi',
    'as'    => 'pelayanan.rekomendasi',
]);

Route::get('/info/{info}','Frontend\InformasiController@index');
Route::get('/info/kinerja-kantor-otoritas-pelabuhan/{slug}','Frontend\InformasiController@kinerja');
Route::get('/info/sop/{slug}','Frontend\InformasiController@sop');
//pandawa
Route::get('/info/sop-tata-usaha/{slug}','Frontend\InformasiController@sopTu');
Route::get('/info/sop-rencana-pengembangan/{slug}','Frontend\InformasiController@sopRenbang');
Route::get('/info/sop-lalu-lintas/{slug}','Frontend\InformasiController@sopLala');
Route::get('/info/rapat-bidang/{slug}','Frontend\InformasiController@rapatBidang');
Route::get('/info/rapat-lembaga/{slug}','Frontend\InformasiController@rapatLembaga');
Route::get('/info/pelayanan-pnbp/{slug}','Frontend\InformasiController@pelayananPnbp');
Route::get('/info/pelayanan-pmku/{slug}','Frontend\InformasiController@pelayananPmku');
Route::get('/info/pelayanan-inaportnet/{slug}','Frontend\InformasiController@pelayananInaportnet');
//endofpandawa
Route::get('/info/indeks-kepuasan-masyarakat/{slug}','Frontend\InformasiController@ikm');
Route::get('/info/reformasi-birokrasi/{slug}','Frontend\InformasiController@reformasi');
Route::get('/info/program-dan-kegiatan/{slug}','Frontend\InformasiController@renstra');
Route::get('/info/informasi-hukum/{slug}','Frontend\InformasiController@hukum');


Route::get('/informasi/file/{id}','Backend\InformasiController@removeFile')->name("admin.informasi.removeFile");
//pandawa
Route::get('/informasi/file/sop-tata-usaha','Backend\SoptuController@removeFile')->name("admin.soptu.removeFile");
Route::get('/informasi/file/sop-rencana-pengembangan','Backend\SoprenbangController@removeFile')->name("admin.soprenbang.removeFile");
Route::get('/informasi/file/sop-lalu-lintas','Backend\SoplalaController@removeFile')->name("admin.soplala.removeFile");
Route::get('/informasi/file/rapat-bidang','Backend\RapatBidangController@removeFile')->name("admin.rapat-bidang.removeFile");
Route::get('/informasi/file/rapat-lembaga','Backend\RapatLembagaController@removeFile')->name("admin.rapat-lembaga.removeFile");
Route::get('/informasi/file/pelayanan-pnbp','Backend\PelayananPnbpController@removeFile')->name("admin.pelayanan-pnbp.removeFile");
Route::get('/informasi/file/pelayanan-pmku','Backend\PelayananPmkuController@removeFile')->name("admin.pelayanan-pmku.removeFile");
Route::get('/informasi/file/pelayanan-inaportnet','Backend\PelayananInaportnetController@removeFile')->name("admin.pelayanan-inaportnet.removeFile");
//endofpandawa

Route::group(['middleware'=>['auth','verified']], function(){
    
    Route::get('/docking',[
        'uses'  => 'Frontend\PelayananController@docking',
        'as'    => 'pelayanan.docking',
    ]);
    Route::get('/docking',[
        'uses'  => 'Frontend\PelayananController@docking',
        'as'    => 'pelayanan.docking',
    ]);
    Route::post('/docking',[
        'uses'  => 'Frontend\PelayananController@store',
        'as'    => 'pelayanan.store',
    ]);
    Route::get('/fumigasi',[
        'uses'  => 'Frontend\PelayananController@fumigasi',
        'as'    => 'pelayanan.fumigasi',
    ]);
    Route::post('/fumigasi',[
        'uses'  => 'Frontend\PelayananController@store',
        'as'    => 'pelayanan.store',
    ]);
    Route::get('/bunker-darat',[
        'uses'  => 'Frontend\PelayananController@bunkerDarat',
        'as'    => 'pelayanan.bunker-darat',
    ]);
    Route::post('/bunker-darat',[
        'uses'  => 'Frontend\PelayananController@store',
        'as'    => 'pelayanan.store',
    ]);
    Route::get('/pelayanan-supplier',[
        'uses'  => 'Frontend\PelayananController@pelayananSupplier',
        'as'    => 'pelayanan.pelayanan-supplier',
    ]);
    Route::post('/pelayanan-supplier',[
        'uses'  => 'Frontend\PelayananController@store',
        'as'    => 'pelayanan.store',
    ]);
    Route::get('/rekomendasi-cabang-ap',[
        'uses'  => 'Frontend\PelayananController@rekomendasiCabangAp',
        'as'    => 'pelayanan.rekomendasi-cabang-ap',
    ]);
    Route::post('/rekomendasi-cabang-ap',[
        'uses'  => 'Frontend\PelayananController@store',
        'as'    => 'pelayanan.store',
    ]);
    Route::get('/rekomendasi-tps',[
        'uses'  => 'Frontend\PelayananController@rekomendasiTps',
        'as'    => 'pelayanan.rekomendasi-tps',
    ]);
    Route::post('/rekomendasi-tps',[
        'uses'  => 'Frontend\PelayananController@store',
        'as'    => 'pelayanan.store',
    ]);
    Route::get('/rekomendasi-cabang-siupkk',[
        'uses'  => 'Frontend\PelayananController@rekomendasiCabangSiupkk',
        'as'    => 'pelayanan.rekomendasi-cabang-siupkk',
    ]);
    Route::post('/rekomendasi-cabang-siupkk',[
        'uses'  => 'Frontend\PelayananController@store',
        'as'    => 'pelayanan.store',
    ]);
    Route::get('/rekomendasi-siup-pbm',[
        'uses'  => 'Frontend\PelayananController@rekomendasiSiupPbm',
        'as'    => 'pelayanan.rekomendasi-siup-pbm',
    ]);
    Route::post('/rekomendasi-siup-pbm',[
        'uses'  => 'Frontend\PelayananController@store',
        'as'    => 'pelayanan.store',
    ]);
    Route::get('/rekomendasi-sikk',[
        'uses'  => 'Frontend\PelayananController@rekomendasiSikk',
        'as'    => 'pelayanan.rekomendasi-sikk',
    ]);
    Route::post('/rekomendasi-sikk',[
        'uses'  => 'Frontend\PelayananController@storeSikk',
        'as'    => 'pelayanan.storeSikk',
    ]);
    Route::get('/sikk',[
        'uses'  => 'Frontend\PelayananController@sikk',
        'as'    => 'pelayanan.sikk',
    ]);
    Route::post('/sikk',[
        'uses'  => 'Frontend\PelayananController@storeSikk',
        'as'    => 'pelayanan.storeSikk',
    ]);
    Route::get('/pelaporan',[
        'uses'  => 'Frontend\PelaporanController@index',
        'as'    => 'pelaporan.index'
    ]);
    Route::get('/tambah-pelaporan',[
        'uses'  => 'Frontend\PelaporanController@create',
        'as'    => 'pelaporan.create'
    ]);
    Route::post('/pelaporan',[
        'uses'  => 'Frontend\PelaporanController@store',
        'as'    => 'pelaporan.store'
    ]);
});
/*-------------------enf of frontend-------------*/


/*-------------------system----------------------*/
Route::get('/dwelling-time',[
    'uses' => 'Backend\DwellingTimeController@fetchData',
    'as' => 'dwelling-time.fetch'
]);

Route::get('/dwelling-time-per-hari',[
    'uses' => 'Backend\DwellingTimeController@perDay',
    'as' => 'dwelling-time.day'
]);
/*-------------------end of system---------------*/

/*-------------------pandawa----------------------*/
Route::resource('pandawa', 'Frontend\PandawaController');
Route::get('sop-pelayanan', 'Frontend\PandawaController@sopPelayanan')->name('sop-pelayanan');
Route::get('informasi-hasil-rapat', 'Frontend\PandawaController@hasilRapat')->name('hasil-rapat');
Route::get('informasi-pelayanan', 'Frontend\PandawaController@informasiPelayanan')->name('informasi-pelayanan');
Route::get('perizinan-online', 'Frontend\PandawaController@perizinanOnline')->name('perizinan-online');
/*-------------------end of pandawa---------------*/

/*-------------------password----------------------*/
Route::get('/change-password', 'HomeController@showChangePasswordForm');
Route::post('/change-password', 'HomeController@changePassword')->name('change-password');
/*-------------------end of password---------------*/
