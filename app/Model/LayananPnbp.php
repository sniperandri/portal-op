<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LayananPnbp extends Model
{
    use SoftDeletes;
    
    protected $table = 'layanan_pnbp';
    
    protected $fillable = ['judul_informasi','konten','gambar','update_id','create_id','delete_id','bulan','tahun','slug'];

    public function scopeActive($query){
    	return $query->where("status_aktif","=",1);
    }

    public function scopeLatest($query){
    	return $query->orderBy("id DESC");
    }

    public function month(){
        return $this->belongsTo(Bulan::class,'bulan','id');
    }

    public function year(){
        return $this->belongsTo(Tahun::class,'tahun','id');
    }

    public function getRouteKeyName(){
        return 'slug';
    }
}
