<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\BackendController;
use App\Model\Pmku;
use App\Http\Requests;
use Carbon\Carbon;
use Auth;
use App\Http\Requests\Backend\PmkuStoreRequest;
use App\Http\Requests\Backend\PmkuUpdateRequest;

class PmkuController extends BackendController
{

    const PMKU_DIPROSES = 2;

    protected $uploadPath;

    private function statusList(){
      return [
        'active' => Pmku::active()->count(),
        'trash' => Pmku::onlyTrashed()->count(),
      ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $onlyTrashed = FALSE;

        $statusList = $this->statusList();

        $perPage = $this->limit;

        if(($status = $request->get('status')) && $status == 'trash'){
            $pmkus = Pmku::onlyTrashed()->paginate($perPage);
            $pmkusCount = Pmku::onlyTrashed()->count();
            $onlyTrashed = TRUE;
        }elseif($status == 'active'){
            $pmkus = Pmku::active()->paginate($perPage);
            $pmkusCount = Pmku::active()->count();
        }else{
            $pmkus = Pmku::paginate($perPage);
            $pmkusCount = Pmku::count();
        }

        return view("backend.pmku.index", compact('pmkus', 'pmkusCount', 'onlyTrashed', 'statusList','perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pmku = new Pmku();
        return view("backend.pmku.create", compact('pmku'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PmkuStoreRequest $request)
    {
        $data = $request->all();
        $data['created_id'] = Auth::user()->id;
        $data['created_at'] = Carbon::now();

        Pmku::create($data);

        return redirect("/admin/pmku")->with('message','Pelayanan PMKU Berhasil Ditambahkan');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pmku = Pmku::findOrFail($id);
        return view("backend.pmku.edit", compact('pmku'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PmkuUpdateRequest $request, $id)
    {
        $data = Pmku::findOrFail($id);

        $data['update_id'] = Auth::user()->id;
        $data['updated_at'] = Carbon::now();

        $data['status_registrasi'] = self::PMKU_DIPROSES;
        $data['balasan'] = $request->get('balasan');

        // $member = [
        //     'email' => $data['email'],
        //     'name'  => $data['nama'],
        //     'subject' => 'Balasan Pengaduan Whistleblower'
        // ];

        // Mail::send('email.balasan-whistleblower', ['data' => $data], function($m) use ($member){
        //     $m->to($member['email'], $member['name'])->cc('andri@zonakreatif.id')->subject($member['subject']);
        // });

        $data->update();

        return redirect("/admin/pmku")->with('message','Pelayanan PMKU Berhasil dibalas.');
    }

    public function show(){
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Pmku::findOrFail($id);
        $data['delete_id'] = Auth::user()->id;
        $data['status_aktif'] = 0;
        $data->update();
        $data->delete();

        return redirect('/admin/pmku')->with('trash-message', ['Pelayanan PMKU has been moved to the trash', $id]);
    }

    public function forceDestroy($id){
      $pmku = Pmku::withTrashed()->findOrFail($id);
      $pmku->forceDelete();

      return redirect('/admin/pmku?status=trash')->with('message','Pelayanan PMKU has been deleted permanently');
    }

    public function restore($id)
    {
      $pmku = Pmku::withTrashed()->findOrFail($id);
      $pmku->restore();

      return redirect('/admin/pmku')->with('message', 'Pelayanan PMKU has been restored from the trash');

    }
}
