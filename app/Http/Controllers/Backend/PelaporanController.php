<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\BackendController;
use App\Model\Pelaporan;
use App\Http\Requests;
use Carbon\Carbon;
use Auth;
use App\Http\Requests\Backend\PelaporanStoreRequest;
use App\Http\Requests\Backend\PelaporanUpdateRequest;

class PelaporanController extends BackendController
{

    const PELAPORAN_DIPROSES = 2;

    protected $uploadPath;

    private function statusList(){
      return [
        'active' => Pelaporan::active()->count(),
        'trash' => Pelaporan::onlyTrashed()->count(),
      ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $onlyTrashed = FALSE;

        $statusList = $this->statusList();

        $perPage = $this->limit;

        if(($status = $request->get('status')) && $status == 'trash'){
            $pelaporans = Pelaporan::onlyTrashed()->paginate($perPage);
            $pelaporansCount = Pelaporan::onlyTrashed()->count();
            $onlyTrashed = TRUE;
        }elseif($status == 'active'){
            $pelaporans = Pelaporan::active()->paginate($perPage);
            $pelaporansCount = Pelaporan::active()->count();
        }else{
            $pelaporans = Pelaporan::paginate($perPage);
            $pelaporansCount = Pelaporan::count();
        }

        return view("backend.pelaporan.index", compact('pelaporans', 'pelaporansCount', 'onlyTrashed', 'statusList','perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pelaporan = new Pelaporan();
        return view("backend.pelaporan.create", compact('pelaporan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PelaporanStoreRequest $request)
    {
        $data = $request->all();
        $data['created_id'] = Auth::user()->id;
        $data['created_at'] = Carbon::now();

        Pelaporan::create($data);

        return redirect("/admin/pelaporan")->with('message','Pelaporan Berhasil Ditambahkan');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pelaporan = Pelaporan::findOrFail($id);
        return view("backend.pelaporan.edit", compact('pelaporan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PelaporanUpdateRequest $request, $id)
    {
        $data = Pelaporan::findOrFail($id);

        $data['update_id'] = Auth::user()->id;
        $data['updated_at'] = Carbon::now();

        $data['status_registrasi'] = self::PELAPORAN_DIPROSES;
        $data['balasan'] = $request->get('balasan');

        // $member = [
        //     'email' => $data['email'],
        //     'name'  => $data['nama'],
        //     'subject' => 'Balasan Pengaduan Whistleblower'
        // ];

        // Mail::send('email.balasan-whistleblower', ['data' => $data], function($m) use ($member){
        //     $m->to($member['email'], $member['name'])->cc('andri@zonakreatif.id')->subject($member['subject']);
        // });

        $data->update();

        return redirect("/admin/pelaporan")->with('message','Pelaporan Berhasil dibalas.');
    }

    public function show(){
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Pelaporan::findOrFail($id);
        $data['delete_id'] = Auth::user()->id;
        $data['status_aktif'] = 0;
        $data->update();
        $data->delete();

        return redirect('/admin/pelaporan')->with('trash-message', ['Pelaporan has been moved to the trash', $id]);
    }

    public function forceDestroy($id){
      $pelaporan = Pelaporan::withTrashed()->findOrFail($id);
      $pelaporan->forceDelete();

      return redirect('/admin/pelaporan?status=trash')->with('message','Pelaporan has been deleted permanently');
    }

    public function restore($id)
    {
      $pelaporan = Pelaporan::withTrashed()->findOrFail($id);
      $pelaporan->restore();

      return redirect('/admin/pelaporan')->with('message', 'Pelaporan has been restored from the trash');

    }
}
