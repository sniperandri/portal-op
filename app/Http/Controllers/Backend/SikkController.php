<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\BackendController;
use App\Model\PelayananSikk;
use App\Http\Requests;
use Carbon\Carbon;
use Auth;
use App\Http\Requests\Backend\PelayananSikkStoreRequest;
use App\Http\Requests\Backend\PelayananSikkUpdateRequest;

class SikkController extends BackendController
{

    const PELAYANAN_SIKK_DIPROSES = 2;

    protected $uploadPath;

    private function statusList(){
      return [
        'active' => PelayananSikk::active()->count(),
        'trash' => PelayananSikk::onlyTrashed()->count(),
      ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $onlyTrashed = FALSE;

        $statusList = $this->statusList();

        $perPage = $this->limit;

        if(($status = $request->get('status')) && $status == 'trash'){
            $pelayanans = PelayananSikk::onlyTrashed()->paginate($perPage);
            $pelayanansCount = PelayananSikk::onlyTrashed()->count();
            $onlyTrashed = TRUE;
        }elseif($status == 'active'){
            $pelayanans = PelayananSikk::active()->paginate($perPage);
            $pelayanansCount = PelayananSikk::active()->count();
        }else{
            $pelayanans = PelayananSikk::paginate($perPage);
            $pelayanansCount = PelayananSikk::count();
        }

        return view("backend.sikk.index", compact('pelayanans', 'pelayanansCount', 'onlyTrashed', 'statusList','perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pelayanan = new PelayananSikk();
        return view("backend.sikk.create", compact('pelayanan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PelayananSikkStoreRequest $request)
    {
        $data = $request->all();
        $data['created_id'] = Auth::user()->id;
        $data['created_at'] = Carbon::now();

        PelayananSikk::create($data);

        return redirect("/admin/sikk")->with('message','Pelayanan Sikk Berhasil Ditambahkan');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pelayanan = PelayananSikk::findOrFail($id);
        return view("backend.sikk.edit", compact('pelayanan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PelayananSikkUpdateRequest $request, $id)
    {
        $data = PelayananSikk::findOrFail($id);

        $data['update_id'] = Auth::user()->id;
        $data['updated_at'] = Carbon::now();

        $data['status_pelayanan'] = self::PELAYANAN_SIKK_DIPROSES;
        $data['balasan'] = $request->get('balasan');

        // $member = [
        //     'email' => $data['email'],
        //     'name'  => $data['nama'],
        //     'subject' => 'Balasan Pengaduan Whistleblower'
        // ];

        // Mail::send('email.balasan-whistleblower', ['data' => $data], function($m) use ($member){
        //     $m->to($member['email'], $member['name'])->cc('andri@zonakreatif.id')->subject($member['subject']);
        // });

        $data->update();

        return redirect("/admin/sikk")->with('message','Pelayanan Sikk Berhasil dibalas.');
    }

    public function show(){
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = PelayananSikk::findOrFail($id);
        $data['delete_id'] = Auth::user()->id;
        $data['status_aktif'] = 0;
        $data->update();
        $data->delete();

        return redirect('/admin/sikk')->with('trash-message', ['Pelayanan SIKK has been moved to the trash', $id]);
    }

    public function forceDestroy($id){
      $pelayanan = PelayananSikk::withTrashed()->findOrFail($id);
      $pelayanan->forceDelete();

      return redirect('/admin/sikk?status=trash')->with('message','Pelayanan SIKK has been deleted permanently');
    }

    public function restore($id)
    {
      $pelayanan = PelayananSikk::withTrashed()->findOrFail($id);
      $pelayanan->restore();

      return redirect('/admin/sikk')->with('message', 'Pelayanan SIKK has been restored from the trash');

    }
}
