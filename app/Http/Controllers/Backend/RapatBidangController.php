<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\BackendController;
use App\Model\RapatBidang;
use App\Http\Requests;
use Carbon\Carbon;
use Auth;
use App\Http\Requests\Backend\RapatbidangStoreRequest;
use App\Http\Requests\Backend\RapatbidangUpdateRequest;

class RapatBidangController extends BackendController
{


    private function statusList(){
      return [
        'active' => RapatBidang::active()->count(),
        'trash' => RapatBidang::onlyTrashed()->count(),
      ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $onlyTrashed = FALSE;

        $statusList = $this->statusList();

        $perPage = $this->limit;

        if(($status = $request->get('status')) && $status == 'trash'){
            $informasis = RapatBidang::onlyTrashed()->latest()->paginate($perPage);
            $informasisCount = RapatBidang::onlyTrashed()->count();
            $onlyTrashed = TRUE;
        }elseif($status == 'active'){
            $informasis = RapatBidang::active()->paginate($perPage);
            $informasisCount = RapatBidang::active()->count();
        }else{
            $informasis = RapatBidang::latest()->active()->paginate($perPage);
            $informasisCount = RapatBidang::count();
        }

        return view("backend.rapat-bidang.index", compact('informasis', 'informasisCount', 'onlyTrashed', 'statusList','perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $informasi = new RapatBidang();
        return view("backend.rapat-bidang.create", compact('informasi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RapatbidangStoreRequest $request)
    {
        $data = $request->all();
        $data['create_id'] = Auth::user()->id;
        $data['created_at'] = Carbon::now();
        $bulan = date('m');
        $tahun = date('Y');
        $data['slug'] = slugify($data['judul_informasi']);

        if($request->hasFile('gambar')){
            $path = $request->file('gambar')->store('informasi/'.$tahun.'/'.$bulan);
            $data['gambar'] = $path;
        }

        RapatBidang::create($data);

        return redirect("/admin/rapat-bidang")->with('message','Informasi Berhasil Ditambahkan');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $informasi = RapatBidang::findOrFail($id);
        return view("backend.rapat-bidang.edit", compact('informasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RapatbidangUpdateRequest $request, $id)
    {
        $data = $request->all();
        $data['updated_id'] = Auth::user()->id;
        $data['updated_at'] = Carbon::now();
        $data['slug'] = slugify($data['judul_informasi']);

        $informasi = RapatBidang::findOrFail($id);
        $oldImage = $informasi->gambar;

        if($request->hasFile('gambar')){
            $bulan = date('m');
            $tahun = date('Y');
            $path = $request->file('gambar')->store('informasi/'.$tahun.'/'.$bulan);
            $data['gambar'] = $path;
        }
        $informasi->update($data);
        if($oldImage !== $informasi->gambar){
          $this->removeImage($oldImage);
        }

        return redirect("/admin/rapat-bidang")->with('message','Informasi Berhasil di update');
    }

    public function show(){
        
    }

    private function removeImage($image){
      if(!empty($image)){
          $imagePath = storage_path().'/app/'.$image;
          $ext = substr(strrchr($image,'.'),1);

        if(file_exists($imagePath)) unlink($imagePath);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = RapatBidang::findOrFail($id);
        $data['delete_id'] = Auth::user()->id;
        $data['status_aktif'] = 0;
        $data->update();
        $data->delete();

        return redirect('/admin/rapat-bidang')->with('trash-message', ['SOP Tata Usaha has been moved to the trash', $id]);
    }

    public function forceDestroy($id){
      $informasi = RapatBidang::withTrashed()->findOrFail($id);
      $informasi->forceDelete();

      return redirect('/admin/rapat-bidang?status=trash')->with('message','SOP Tata Usaha has been deleted permanently');
    }

    public function restore($id)
    {
      $informasi = RapatBidang::withTrashed()->findOrFail($id);
      $informasi->restore();

      return redirect('/admin/rapat-bidang')->with('message', 'SOP Tata Usaha has been restored from the trash');

    }

    public function removeFile($id)
    {
        $data = RapatBidang::findOrFail($id);
        $oldImage = $data->gambar;
        $this->removeImage($oldImage);
        $data->gambar = null;
        $data->update();
        return redirect('/admin/rapat-bidang')->with('trash-message', ['Attachment has been moved to the trash', $id]);
    }
}
