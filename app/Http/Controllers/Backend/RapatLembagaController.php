<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\BackendController;
use App\Model\RapatLembaga;
use App\Http\Requests;
use Carbon\Carbon;
use Auth;
use App\Http\Requests\Backend\RapatlembagaStoreRequest;
use App\Http\Requests\Backend\RapatlembagaUpdateRequest;

class RapatLembagaController extends BackendController
{


    private function statusList(){
      return [
        'active' => RapatLembaga::active()->count(),
        'trash' => RapatLembaga::onlyTrashed()->count(),
      ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $onlyTrashed = FALSE;

        $statusList = $this->statusList();

        $perPage = $this->limit;

        if(($status = $request->get('status')) && $status == 'trash'){
            $informasis = RapatLembaga::onlyTrashed()->latest()->paginate($perPage);
            $informasisCount = RapatLembaga::onlyTrashed()->count();
            $onlyTrashed = TRUE;
        }elseif($status == 'active'){
            $informasis = RapatLembaga::active()->paginate($perPage);
            $informasisCount = RapatLembaga::active()->count();
        }else{
            $informasis = RapatLembaga::latest()->active()->paginate($perPage);
            $informasisCount = RapatLembaga::count();
        }

        return view("backend.rapat-lembaga.index", compact('informasis', 'informasisCount', 'onlyTrashed', 'statusList','perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $informasi = new RapatLembaga();
        return view("backend.rapat-lembaga.create", compact('informasi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RapatlembagaStoreRequest $request)
    {
        $data = $request->all();
        $data['create_id'] = Auth::user()->id;
        $data['created_at'] = Carbon::now();
        $bulan = date('m');
        $tahun = date('Y');
        $data['slug'] = slugify($data['judul_informasi']);

        if($request->hasFile('gambar')){
            $path = $request->file('gambar')->store('informasi/'.$tahun.'/'.$bulan);
            $data['gambar'] = $path;
        }

        RapatLembaga::create($data);

        return redirect("/admin/rapat-lembaga")->with('message','Informasi Berhasil Ditambahkan');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $informasi = RapatLembaga::findOrFail($id);
        return view("backend.rapat-lembaga.edit", compact('informasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RapatlembagaUpdateRequest $request, $id)
    {
        $data = $request->all();
        $data['updated_id'] = Auth::user()->id;
        $data['updated_at'] = Carbon::now();
        $data['slug'] = slugify($data['judul_informasi']);

        $informasi = RapatLembaga::findOrFail($id);
        $oldImage = $informasi->gambar;

        if($request->hasFile('gambar')){
            $bulan = date('m');
            $tahun = date('Y');
            $path = $request->file('gambar')->store('informasi/'.$tahun.'/'.$bulan);
            $data['gambar'] = $path;
        }
        $informasi->update($data);
        if($oldImage !== $informasi->gambar){
          $this->removeImage($oldImage);
        }

        return redirect("/admin/rapat-lembaga")->with('message','Informasi Berhasil di update');
    }

    public function show(){
        
    }

    private function removeImage($image){
      if(!empty($image)){
          $imagePath = storage_path().'/app/'.$image;
          $ext = substr(strrchr($image,'.'),1);

        if(file_exists($imagePath)) unlink($imagePath);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = RapatLembaga::findOrFail($id);
        $data['delete_id'] = Auth::user()->id;
        $data['status_aktif'] = 0;
        $data->update();
        $data->delete();

        return redirect('/admin/rapat-lembaga')->with('trash-message', ['SOP Tata Usaha has been moved to the trash', $id]);
    }

    public function forceDestroy($id){
      $informasi = RapatLembaga::withTrashed()->findOrFail($id);
      $informasi->forceDelete();

      return redirect('/admin/rapat-lembaga?status=trash')->with('message','SOP Tata Usaha has been deleted permanently');
    }

    public function restore($id)
    {
      $informasi = RapatLembaga::withTrashed()->findOrFail($id);
      $informasi->restore();

      return redirect('/admin/rapat-lembaga')->with('message', 'SOP Tata Usaha has been restored from the trash');

    }

    public function removeFile($id)
    {
        $data = RapatLembaga::findOrFail($id);
        $oldImage = $data->gambar;
        $this->removeImage($oldImage);
        $data->gambar = null;
        $data->update();
        return redirect('/admin/rapat-lembaga')->with('trash-message', ['Attachment has been moved to the trash', $id]);
    }
}
