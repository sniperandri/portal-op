<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\BackendController;
use App\Model\PelayananPmku;
use App\Http\Requests;
use Carbon\Carbon;
use Auth;
use App\Http\Requests\Backend\PelayananpmkuStoreRequest;
use App\Http\Requests\Backend\PelayananpmkuUpdateRequest;

class PelayananPmkuController extends BackendController
{


    private function statusList(){
      return [
        'active' => PelayananPmku::active()->count(),
        'trash' => PelayananPmku::onlyTrashed()->count(),
      ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $onlyTrashed = FALSE;

        $statusList = $this->statusList();

        $perPage = $this->limit;

        if(($status = $request->get('status')) && $status == 'trash'){
            $informasis = PelayananPmku::onlyTrashed()->latest()->paginate($perPage);
            $informasisCount = PelayananPmku::onlyTrashed()->count();
            $onlyTrashed = TRUE;
        }elseif($status == 'active'){
            $informasis = PelayananPmku::active()->paginate($perPage);
            $informasisCount = PelayananPmku::active()->count();
        }else{
            $informasis = PelayananPmku::latest()->active()->paginate($perPage);
            $informasisCount = PelayananPmku::count();
        }

        return view("backend.pelayanan-pmku.index", compact('informasis', 'informasisCount', 'onlyTrashed', 'statusList','perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $informasi = new PelayananPmku();
        return view("backend.pelayanan-pmku.create", compact('informasi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PelayananpmkuStoreRequest $request)
    {
        $data = $request->all();
        $data['create_id'] = Auth::user()->id;
        $data['created_at'] = Carbon::now();
        $bulan = date('m');
        $tahun = date('Y');
        $data['slug'] = slugify($data['judul_informasi']);

        if($request->hasFile('gambar')){
            $path = $request->file('gambar')->store('informasi/'.$tahun.'/'.$bulan);
            $data['gambar'] = $path;
        }

        PelayananPmku::create($data);

        return redirect("/admin/pelayanan-pmku")->with('message','Informasi Berhasil Ditambahkan');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $informasi = PelayananPmku::findOrFail($id);
        return view("backend.pelayanan-pmku.edit", compact('informasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PelayananpmkuUpdateRequest $request, $id)
    {
        $data = $request->all();
        $data['updated_id'] = Auth::user()->id;
        $data['updated_at'] = Carbon::now();
        $data['slug'] = slugify($data['judul_informasi']);

        $informasi = PelayananPmku::findOrFail($id);
        $oldImage = $informasi->gambar;

        if($request->hasFile('gambar')){
            $bulan = date('m');
            $tahun = date('Y');
            $path = $request->file('gambar')->store('informasi/'.$tahun.'/'.$bulan);
            $data['gambar'] = $path;
        }
        $informasi->update($data);
        if($oldImage !== $informasi->gambar){
          $this->removeImage($oldImage);
        }

        return redirect("/admin/pelayanan-pmku")->with('message','Informasi Berhasil di update');
    }

    public function show(){
        
    }

    private function removeImage($image){
      if(!empty($image)){
          $imagePath = storage_path().'/app/'.$image;
          $ext = substr(strrchr($image,'.'),1);

        if(file_exists($imagePath)) unlink($imagePath);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = PelayananPmku::findOrFail($id);
        $data['delete_id'] = Auth::user()->id;
        $data['status_aktif'] = 0;
        $data->update();
        $data->delete();

        return redirect('/admin/pelayanan-pmku')->with('trash-message', ['SOP Tata Usaha has been moved to the trash', $id]);
    }

    public function forceDestroy($id){
      $informasi = PelayananPmku::withTrashed()->findOrFail($id);
      $informasi->forceDelete();

      return redirect('/admin/pelayanan-pmku?status=trash')->with('message','SOP Tata Usaha has been deleted permanently');
    }

    public function restore($id)
    {
      $informasi = PelayananPmku::withTrashed()->findOrFail($id);
      $informasi->restore();

      return redirect('/admin/pelayanan-pmku')->with('message', 'SOP Tata Usaha has been restored from the trash');

    }

    public function removeFile($id)
    {
        $data = PelayananPmku::findOrFail($id);
        $oldImage = $data->gambar;
        $this->removeImage($oldImage);
        $data->gambar = null;
        $data->update();
        return redirect('/admin/pelayanan-pmku')->with('trash-message', ['Attachment has been moved to the trash', $id]);
    }
}
