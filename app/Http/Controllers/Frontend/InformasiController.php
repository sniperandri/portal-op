<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Informasi;
use App\Model\SopTu;
use App\Model\SopRenbang;
use App\Model\SopLala;
use App\Model\RapatBidang;
use App\Model\RapatLembaga;
use App\Model\PelayananPnbp;
use App\Model\PelayananPmku;
use App\Model\PelayananInaportnet;

class InformasiController extends Controller
{
    protected $limit = 50;

    public function index(Request $request, $info="data-dan-informasi"){
        $perPage = $this->limit;

        switch($info){
            case 'informasi-publik':
                    $konten = Informasi::where('jenis_informasi_id',1)->first();
                break;
            case 'program-dan-kegiatan':
                    $kontens = Informasi::where('jenis_informasi_id',2)->latest()->paginate($perPage);
                    $kontensCount = Informasi::where('jenis_informasi_id',2)->count();
                    return view('frontend.informasi.renstra',compact('kontens','kontensCount','perPage'));
                break;
            case 'data-dan-informasi':
                    $konten = Informasi::where('jenis_informasi_id',3)->first();
                break;
            case 'kinerja-kantor-otoritas-pelabuhan':
                    $kontens = Informasi::where('jenis_informasi_id',4)->latest()->paginate($perPage);
                    $kontensCount = Informasi::where('jenis_informasi_id',4)->count();
                    return view('frontend.informasi.lapkin',compact('kontens','kontensCount','perPage'));
                break;
            case 'sop':
                    $kontens = Informasi::where('jenis_informasi_id',31)->latest()->paginate($perPage);
                    $kontensCount = Informasi::where('jenis_informasi_id',31)->count();
                    return view('frontend.informasi.sop',compact('kontens','kontensCount','perPage'));
                break;
            case 'informasi-hukum':
                    $kontens = Informasi::where('jenis_informasi_id',5)->latest()->paginate($perPage);
                    $kontensCount = Informasi::where('jenis_informasi_id',5)->count();
                    return view('frontend.informasi.hukum',compact('kontens','kontensCount','perPage'));
                break;
            case 'tarif-pnbp':
                    $konten = Informasi::where('jenis_informasi_id',6)->first();
                break;
            case 'indeks-kepuasan-masyarakat':
                    $kontens = Informasi::where('jenis_informasi_id',7)->latest()->paginate($perPage);
                    $kontensCount = Informasi::where('jenis_informasi_id',7)->count();
                    return view('frontend.informasi.ikm',compact('kontens','kontensCount','perPage'));
                break;
            case 'reformasi-birokrasi':
                    $kontens = Informasi::where('jenis_informasi_id',28)->latest()->paginate($perPage);
                    $kontensCount = Informasi::where('jenis_informasi_id',28)->count();
                    return view('frontend.informasi.reformasi-birokrasi',compact('kontens','kontensCount','perPage'));
                break;
            case 'sop-tata-usaha':
                    $kontens = SopTu::latest()->paginate($perPage);
                    $kontensCount = SopTu::count();
                    return view('frontend.informasi.soptu',compact('kontens','kontensCount','perPage'));
                break;
            case 'sop-rencana-pengembangan':
                    $kontens = SopRenbang::latest()->paginate($perPage);
                    $kontensCount = SopRenbang::count();
                    return view('frontend.informasi.soprenbang',compact('kontens','kontensCount','perPage'));
                break;
            case 'sop-lalu-lintas':
                    $kontens = SopLala::latest()->paginate($perPage);
                    $kontensCount = SopLala::count();
                    return view('frontend.informasi.soplala',compact('kontens','kontensCount','perPage'));
                break;
            case 'rapat-bidang':
                    $kontens = RapatBidang::latest()->paginate($perPage);
                    $kontensCount = RapatBidang::count();
                    return view('frontend.informasi.rapat-bidang',compact('kontens','kontensCount','perPage'));
                break;
            case 'rapat-lembaga':
                    $kontens = RapatLembaga::latest()->paginate($perPage);
                    $kontensCount = RapatLembaga::count();
                    return view('frontend.informasi.rapat-lembaga',compact('kontens','kontensCount','perPage'));
                break;
            case 'pelayanan-pnbp':
                    $kontens = PelayananPnbp::latest()->paginate($perPage);
                    $kontensCount = PelayananPnbp::count();
                    return view('frontend.informasi.pelayanan-pnbp',compact('kontens','kontensCount','perPage'));
                break;
            case 'pelayanan-pmku':
                    $kontens = PelayananPmku::latest()->paginate($perPage);
                    $kontensCount = PelayananPmku::count();
                    return view('frontend.informasi.pelayanan-pmku',compact('kontens','kontensCount','perPage'));
                break;
            case 'pelayanan-inaportnet':
                    $kontens = PelayananInaportnet::latest()->paginate($perPage);
                    $kontensCount = PelayananInaportnet::count();
                    return view('frontend.informasi.pelayanan-inaportnet',compact('kontens','kontensCount','perPage'));
                break;
            default:
                break;
        }
        return view('frontend.informasi.index',compact('konten','perPage'));
    }

    public function kinerja($slug){
        $kinerja = Informasi::where('slug',$slug)->first();

        return view('frontend.informasi.detail.kinerja', compact('kinerja'));
    }

    public function sop($slug){
        $sop = Informasi::where('slug',$slug)->first();

        return view('frontend.informasi.detail.sop', compact('sop'));
    }

    public function ikm($slug){
        $ikm = Informasi::where('slug',$slug)->first();

        return view('frontend.informasi.detail.ikm', compact('ikm'));
    }

    public function reformasi($slug){
        $reformasi = Informasi::where('slug',$slug)->first();

        return view('frontend.informasi.detail.reformasi', compact('reformasi'));
    }

    public function renstra($slug){
        $renstra = Informasi::where('slug',$slug)->first();

        return view('frontend.informasi.detail.renstra', compact('renstra'));
    }

    public function hukum($slug){
        $hukum = Informasi::where('slug',$slug)->first();

        return view('frontend.informasi.detail.hukum', compact('hukum'));
    }

    public function sopTu($slug){
        $sop = SopTu::where('slug',$slug)->first();

        return view('frontend.informasi.detail.soptu', compact('sop'));
    }

    public function sopRenbang($slug){
        $sop = SopRenbang::where('slug',$slug)->first();

        return view('frontend.informasi.detail.soprenbang', compact('sop'));
    }

    public function sopLala($slug){
        $sop = SopLala::where('slug',$slug)->first();

        return view('frontend.informasi.detail.soplala', compact('sop'));
    }

    public function rapatBidang($slug){
        $sop = RapatBidang::where('slug',$slug)->first();

        return view('frontend.informasi.detail.rapat-bidang', compact('sop'));
    }

    public function rapatLembaga($slug){
        $sop = RapatLembaga::where('slug',$slug)->first();

        return view('frontend.informasi.detail.rapat-lembaga', compact('sop'));
    }

    public function pelayananPnbp($slug){
        $sop = PelayananPnbp::where('slug',$slug)->first();

        return view('frontend.informasi.detail.pelayanan-pnbp', compact('sop'));
    }

    public function pelayananPmku($slug){
        $sop = PelayananPmku::where('slug',$slug)->first();

        return view('frontend.informasi.detail.pelayanan-pmku', compact('sop'));
    }

    public function pelayananInaportnet($slug){
        $sop = PelayananInaportnet::where('slug',$slug)->first();

        return view('frontend.informasi.detail.pelayanan-inaportnet', compact('sop'));
    }
}