@extends('layouts.frontend.main')

@section('pageTitle', 'Perizinan Online')
<style>
  .flex-container{
    display: flex;
    justify-content: center;
  }
  .desc a{
    text-decoration: none;
    color: #1F2A22;
  }
  .desc a:hover{
    text-decoration: none;
    color: #3C3487;
    font-size: 22px;
  }
</style>
@section('content')
	<section>
	    <div class="content-header">
	        <img src="{{ asset('frontend-asset/images/head.png') }}" />
	        <div class="container">
	            <div class="row">
	                <div class="col">
	                    <div class="desc">
	                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>APLIKASI SI PANDAWA</span></small>
	                        <h2>Perizinan Online</h2>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
		<div class="content-box bg-grey">
		    <div class="container">
		        <div class="row">
		            <div class="col">
		                <label class="content-label">Perizinan Online</label>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-md-4">
		                <div class="function-box">
		                    <span><i class="fa fa-info-circle"></i></span>
		                    <div class="desc">
		                        <h5><a href="{{ route('registrasi') }}">Registrasi PMKU</a></h5>
		                        <p>
		                            Informasi Pelayanan Pemberitahuan Melakukan Kegiatan Usaha (PMKU).
		                        </p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-md-4">
		                <div class="function-box">
		                    <span><i class="fa fa-check-circle"></i></span>
		                    <div class="desc">
		                        <h5><a href="{{ route('pelayanan.rekomendasi') }}">Rekomendasi</a></h5>
		                        <p>
		                            Pelayanan Rekomendasi, di antaranya Rekomendasi Pembukaan Kantor Cabang Pelayaran, Rekomendasi Cabang Keagenan Kapal, Rekomendasi Surat Ijin Kerja Keruk, Rekomendasi JPT, Rekomendasi PBM, dan Rekomendasi TPS.
		                        </p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-md-4">
		                <div class="function-box">
		                    <span><i class="fa fa-envelope-square"></i></span>
		                    <div class="desc">
		                        <h5><a href="{{ route('pelayanan.sikk') }}">Surat Izin Kerja Keruk</a></h5>
		                        <p>
		                            Informasi Layanan Surat Izin Kerja Keruk.
		                        </p>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	    <div class="info-box">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6">
	                    <h4>How can we help you?</h4>
	                </div>
	                <div class="col-md-6 right">
	                    <a href="#" class="btn btn-blue">Send Feedback <i class="fa fa-chevron-right"></i></a>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
@endsection
@section('script')
    <script>
        $(function(){
            $("ul.list>li a").on('click',function(){
				$('li').removeClass();
                $(this).parent().addClass('active');
            });
        });
    </script>
@endsection