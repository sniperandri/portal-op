@extends('layouts.frontend.main')

@section('pageTitle', 'Berita')
<style>
  .flex-container{
    display: flex;
    justify-content: center;
  }
  .desc a{
    text-decoration: none;
    color: #1F2A22;
  }
  .desc a:hover{
    text-decoration: none;
    color: #3C3487;
    font-size: 22px;
  }
</style>
@section('content')
	<section>
	    <div class="content-header">
	        <img src="{{ asset('frontend-asset/images/head.png') }}" />
	        <div class="container">
	            <div class="row">
	                <div class="col">
	                    <div class="desc">
	                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>Pandawa</span></small>
	                        <h2>APLIKASI SI PANDAWA</h2>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
		<div class="content-box bg-grey">
		    <div class="container">
		        <div class="row">
		            <div class="col">
		                <label class="content-label">APLIKASI SI PANDAWA</label>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-md-4">
		                <div class="function-box">
		                    <span><i class="fa fa-sitemap"></i></span>
		                    <div class="desc">
		                        <h5><a href="{{ route('sop-pelayanan') }}">SOP : Kinerja Pelayanan Tersertifikasi ISO 9001:2015</a></h5>
		                        <p>
		                            Standard Operation Procedure Pelayanan di Kantor Otoritas Pelabuhan Utama Tanjung Priok.
		                        </p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-md-4">
		                <div class="function-box">
		                    <span><i class="fa fa-comments"></i></span>
		                    <div class="desc">
		                        <h5><a href="{{ route('hasil-rapat') }}">Hubungan Antar Lembaga</a></h5>
		                        <p>
		                            Pelayanan Terpadu dan Terintegrasi serta Coffee Morning.
		                        </p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-md-4">
		                <div class="function-box">
		                    <span><i class="fa fa-rss"></i></span>
		                    <div class="desc">
		                        <h5><a href="{{ route('informasi-pelayanan') }}">Sistem Informasi Digital Publik</a></h5>
		                        <p>
		                            Info Pelayanan Kantor Otoritas Pelabuhan Utama Tanjung Priok seperti Layanan Perizinan Online serta Pelaporan Kegiatan Usaha.
		                        </p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-md-4">
		                <div class="function-box">
		                    <span><i class="fa fa-newspaper-o"></i></span>
		                    <div class="desc">
		                        <h5><a href="{{ route('berita.index') }}">Warta Bishop</a></h5>
		                        <p>
		                            Informasi dan Berita mengenai kegiatan di Kantor Otoritas Pelabuhan Utama Tanjung Priok.
		                        </p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-md-4">
		                <div class="function-box">
		                    <span><i class="fa fa-volume-control-phone"></i></span>
		                    <div class="desc">
		                        <h5><a href="https://hangouts.google.com/" target="_blank">Aplikasi Rapat Online</a></h5>
		                        <p>
		                            Aplikasi Rapat Online menggunakan Google Hangout.
		                        </p>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	    <div class="info-box">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6">
	                    <h4>How can we help you?</h4>
	                </div>
	                <div class="col-md-6 right">
	                    <a href="#" class="btn btn-blue">Send Feedback <i class="fa fa-chevron-right"></i></a>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
@endsection
@section('script')
    <script>
        $(function(){
            $("ul.list>li a").on('click',function(){
				$('li').removeClass();
                $(this).parent().addClass('active');
            });
        });
    </script>
@endsection