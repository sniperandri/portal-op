@extends('layouts.frontend.main')

@section('pageTitle', 'SOP Pelayanan')
<style>
  .flex-container{
    display: flex;
    justify-content: center;
  }
  .desc a{
    text-decoration: none;
    color: #1F2A22;
  }
  .desc a:hover{
    text-decoration: none;
    color: #3C3487;
    font-size: 22px;
  }
</style>
@section('content')
	<section>
	    <div class="content-header">
	        <img src="{{ asset('frontend-asset/images/head.png') }}" />
	        <div class="container">
	            <div class="row">
	                <div class="col">
	                    <div class="desc">
	                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>APLIKASI SI PANDAWA</span></small>
	                        <h2>SOP Pelayanan</h2>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
		<div class="content-box bg-grey">
		    <div class="container">
		        <div class="row">
		            <div class="col">
		                <label class="content-label">SOP Pelayanan</label>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-md-4">
		                <div class="function-box">
		                    <span><i class="fa fa-leanpub"></i></span>
		                    <div class="desc">
		                        <h5><a href="{{ url('/info/sop-tata-usaha') }}">BAGIAN TATA USAHA</a></h5>
		                        <p>
		                            Standard Operation Procedure (SOP) Pelayanan di Kantor Otoritas Pelabuhan Utama Tanjung Priok Bidang Tata Usaha (TU).
		                        </p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-md-4">
		                <div class="function-box">
		                    <span><i class="fa fa-road"></i></span>
		                    <div class="desc">
		                        <h5><a href="{{ url('/info/sop-rencana-pengembangan') }}">BIDANG PERENCANAAN DAN PEMBANGUNAN</a></h5>
		                        <p>
		                            Standard Operation Procedure (SOP) Pelayanan di Kantor Otoritas Pelabuhan Utama Tanjung Priok Bidang Perencanaan dan Pengembangan (Renbang).
		                        </p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-md-4">
		                <div class="function-box">
		                    <span><i class="fa fa-map-signs"></i></span>
		                    <div class="desc">
		                        <h5><a href="{{ url('/info/sop-lalu-lintas') }}">BIDANG LALU LINTAS LAUT, OPERASI, DAN USAHA KEPELABUHANAN</a></h5>
		                        <p>
		                            Standard Operation Procedure (SOP) Pelayanan di Kantor Otoritas Pelabuhan Utama Tanjung Priok Bidang Lalu Lintas (LALA).
		                        </p>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	    <div class="info-box">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6">
	                    <h4>How can we help you?</h4>
	                </div>
	                <div class="col-md-6 right">
	                    <a href="#" class="btn btn-blue">Send Feedback <i class="fa fa-chevron-right"></i></a>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
@endsection
@section('script')
    <script>
        $(function(){
            $("ul.list>li a").on('click',function(){
				$('li').removeClass();
                $(this).parent().addClass('active');
            });
        });
    </script>
@endsection