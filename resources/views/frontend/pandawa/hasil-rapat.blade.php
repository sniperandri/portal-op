@extends('layouts.frontend.main')

@section('pageTitle', 'Hubungan Antar Lembaga')
<style>
  .flex-container{
    display: flex;
    justify-content: center;
  }
  .desc a{
    text-decoration: none;
    color: #1F2A22;
  }
  .desc a:hover{
    text-decoration: none;
    color: #3C3487;
    font-size: 22px;
  }
</style>
@section('content')
	<section>
	    <div class="content-header">
	        <img src="{{ asset('frontend-asset/images/head.png') }}" />
	        <div class="container">
	            <div class="row">
	                <div class="col">
	                    <div class="desc">
	                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>APLIKASI SI PANDAWA</span></small>
	                        <h2>Hubungan Antar Lembaga</h2>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
		<div class="content-box bg-grey">
		    <div class="container">
		        <div class="row">
		            <div class="col">
		                <label class="content-label">Hubungan Antar Lembaga</label>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-md-6">
		                <div class="function-box">
		                    <span><i class="fa fa-users"></i></span>
		                    <div class="desc">
		                        <h5><a href="{{ url('/info/rapat-bidang') }}">Pelayanan Terpadu dan Terintegrasi</a></h5>
		                        <p>
		                            Hubungan Antar Lembaga Antar Bidang di Lingkungan Kantor Otoritas Pelabuhan Utama Tanjung Priok.
		                        </p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-md-6">
		                <div class="function-box">
		                    <span><i class="fa fa-comments"></i></span>
		                    <div class="desc">
		                        <h5><a href="{{ url('/info/rapat-lembaga') }}">Coffee Morning</a></h5>
		                        <p>
		                            Hubungan Antar Lembaga Kantor Otoritas Pelabuhan Utama Tanjung Priok dengan Lembaga lainnya.
		                        </p>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	    <div class="info-box">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6">
	                    <h4>How can we help you?</h4>
	                </div>
	                <div class="col-md-6 right">
	                    <a href="#" class="btn btn-blue">Send Feedback <i class="fa fa-chevron-right"></i></a>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
@endsection
@section('script')
    <script>
        $(function(){
            $("ul.list>li a").on('click',function(){
				$('li').removeClass();
                $(this).parent().addClass('active');
            });
        });
    </script>
@endsection