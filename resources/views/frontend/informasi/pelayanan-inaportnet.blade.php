@extends('layouts.frontend.main')

@section('pageTitle', 'Informasi Pelayanan Inaportnet')

@section('content')
<section>
    <div class="content-header">
        <img src="{{ asset('frontend-asset/images/bg-header3.png') }}" />
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="desc">
                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>Informasi Layanan</span></small>
                        <h2>Layanan Inaportnet</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-box">
        <div class="container">
            <div class="row">
                <div class="col">
                    <label class="content-label">Informasi Pelayanan Inaportnet</label>
                </div>
            </div>
        	@include('frontend.message')
            <div class="row">
                <div class="col-md-12">
					<table class="table table-striped">
					  <tr>
					    <th style="width: 10px">No</th>
					    <th>Judul</th>
					    <th class="text-left" style="width: 200px">Download Informasi Pelayanan Inaportnet</th>
					  </tr>
                      <?php $no = paging_number($perPage);?>
                        @foreach($kontens as $konten)
					    <tr>
					      <td>{{ $no }}</td>
					      <td><a href="{{ url('info').'/pelayanan-inaportnet/'.$konten->slug }}">{{ $konten->judul_informasi }}</a></td>
					      <td align="center">
                            @if($konten->gambar)
					        <a href="{{ url($konten->gambar) }}" class="btn btn-sm btn-block btn-success" download><i class="fa fa-download"></i> Download</a>
                            @else
                            -
                            @endif
					      </td>
				        {!! Form::close() !!}
					    </tr>
                        <?php $no++;?>
                        @endforeach
					</table>
                </div>
            </div>
            <a href="{{ URL::previous() }}"> <span class="btn btn-sm btn-warning"><i class="fa fa-arrow-circle-left"></i> Kembali</span></a>
        </div>
    </div>
</section>
@endsection