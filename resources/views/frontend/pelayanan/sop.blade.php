@extends('layouts.frontend.main')

@section('pageTitle', 'SOP Pelayanan')

@section('content')
	<section>
	    <div class="content-header">
	        <img src="{{ asset('frontend-asset/images/bg-header3.png') }}" />
	        <div class="container">
	            <div class="row">
	                <div class="col">
	                    <div class="desc">
	                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>Pelayanan</span></small>
	                        <h2>SOP Pelayanan</h2>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="content-box">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <label class="content-label">SOP Pelayanan</label>
                    </div>
                </div>
	            <div class="row">
	                <div class="col">
	                    <div class="contact-form">
                            <label class="contact-label"><span>Alur Pelayanan</span></label>
                                <div class="row">
                                    <div class="col-md-12">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	</section>
@endsection