<table class="table table-striped">
  <tr>
    <th style="width: 10px">No</th>
    <th>Nomor Pelayanan</th>
    <th>Keterangan Peta</th>
    <th>Rencana Kedalaman</th>
    <th>Volume</th>
    <th>Detail Permohonan</th>
    <th class="text-center">Status Pelayanan</th>
    <th class="text-center" style="width: 200px">Aksi</th>
  </tr>
  <?php $no = paging_number($perPage);?>
  @foreach($pelayanans as $pelayanan)
  	<tr>
  	  <td>{{ $no }}.</td>
      <td>{{ $pelayanan->nomor_pelayanan }}</td>
      <td>{{ $pelayanan->keterangan_peta }}</td>
      <td>{{ $pelayanan->rencana_kedalaman }}</td>
      <td>{{ $pelayanan->volume }}</td>
  	  <td>
        <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modalDetail" data-no_pengaduan="{{ $pelayanan->no_pengaduan }}" data-nama="{{ $pelayanan->nama }}" data-jenis_id="{{ $pelayanan->jenis_id }}" data-nomor_id="{{ $pelayanan->nomor_id }}" data-email="{{ $pelayanan->email }}" data-instansi="{{ $pelayanan->instansi }}" data-alamat="{{ $pelayanan->alamat }}" data-pesan="{{ $pelayanan->pesan }}" data-balasan="{{ $pelayanan->balasan }}" data-attachment= "{{ url($pelayanan->namafile ? $pelayanan->namafile : 'empty.jpg') }}" style="width: 150px;"><i class="fa fa-search"></i> Detail Permohonan</button>
      </td>
      @if($pelayanan->status_pelayanan == 1)
        <td align="center"><span class="badge badge-danger badge-sm">Belum Diproses</span></td>
      @else 
    <td align="center"><span class="badge badge-success badge-sm">Sudah Diproses oleh {{ $pelayanan->updater->name }}</span></td>
      @endif
      <td align="center">
        @if($pelayanan->status_pelayanan == 1)
        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.sikk.destroy', $pelayanan->id]]) !!}
        <a href="{{ route('admin.sikk.edit', $pelayanan->id) }}" class="btn btn-sm btn-primary btn-block" title="Edit">
          <i class="fa fa-comment text-black"></i> Proses
        </a>
        @else 
        <p> - </p>
        @role('superadmin')
          <button onclick="return confirm('Apakah Anda yakin untuk menghapus pelayanan sikk?')" type="submit" class="btn btn-sm btn-danger btn-block" title="Move to Trash">
            <i class="fa fa-trash"></i> Hapus
          </button>
        @endrole
        @endif
        {!! Form::close() !!}
      </td>
  	</tr>
    <?php $no++;?>
  @endforeach
  @include('backend.sikk.modal')
</table>
@include('backend.sikk.dashscript')