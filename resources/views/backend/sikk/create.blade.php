@extends('layouts.backend.main')
@section('pageTitle','Tambah Pelayanan SIKK')
@section('breadcrumbTitle','Tambah Pelayanan SIKK')
@section('breadcrumbParent','Pengaturan  Pelayanan SIKK')

@section('content')
	<div class="content-wrapper">
		@include('layouts.backend.breadcrumb')
		<!-- Main content -->
		    <div class="row">
		      <div class="col-lg-12">
		        {!! Form::model($pelayanan, [
		            'method' => 'POST',
		            'route' => 'sikk.store',
		            'id' => 'sikk-form'
		        ]) !!}

		        @include('backend.sikk.form')

		        {!! Form::close() !!}
		      </div>
		    </div>
		  <!-- ./row -->
	</div>
	@include('layouts.backend.footer')
@endsection
@include('backend.sikk.script')