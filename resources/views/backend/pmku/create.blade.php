@extends('layouts.backend.main')
@section('pageTitle','Tambah Pelayanan PMKU')
@section('breadcrumbTitle','Tambah Pelayanan PMKU')
@section('breadcrumbParent','Pengaturan  Pelayanan PMKU')

@section('content')
	<div class="content-wrapper">
		@include('layouts.backend.breadcrumb')
		<!-- Main content -->
		    <div class="row">
		      <div class="col-lg-12">
		        {!! Form::model($pmku, [
		            'method' => 'POST',
		            'route' => 'admin.pmku.store',
		            'id' => 'pmku-form'
		        ]) !!}

		        @include('backend.pmku.form')

		        {!! Form::close() !!}
		      </div>
		    </div>
		  <!-- ./row -->
	</div>
	@include('layouts.backend.footer')
@endsection
@include('backend.pmku.script')