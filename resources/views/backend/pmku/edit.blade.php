@extends('layouts.backend.main')
@section('pageTitle','Update Pelayanan SIKK')
@section('breadcrumbTitle','Update Pelayanan SIKK')
@section('breadcrumbParent','Pengaturan / Pelayanan SIKK')

@section('content')
	<div class="content-wrapper">
		@include('layouts.backend.breadcrumb')
		<!-- Main content -->
		    <div class="row">
		      <div class="col-lg-12">
		        {!! Form::model($pmku, [
		            'method' => 'PUT',
		            'route' => ['admin.pmku.update', $pmku->id],
		            'id' => 'pmku-form',
		            'files'=> TRUE,
		        ]) !!}

		        @include('backend.pmku.form')

		        {!! Form::close() !!}
		      </div>
		    </div>
		  <!-- ./row -->
	</div>
	@include('layouts.backend.footer')
@endsection