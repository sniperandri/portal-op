<table class="table table-striped">
  <tr>
    <th style="width: 10px">No</th>
    <th>Nomor SIUP</th>
    <th>NPWP</th>
    <th>Nama Perusahaan</th>
    <th>Detil Permohonan</th>
    <th class="text-center">Status Pelayanan</th>
    <th class="text-center" style="width: 200px">Aksi</th>
  </tr>
  <?php $no = paging_number($perPage);?>
  @foreach($pmkus as $pmku)
  	<tr>
  	  <td>{{ $no }}.</td>
      <td>{{ $pmku->nomor_siup }}</td>
      <td>{{ $pmku->npwp }}</td>
      <td>{{ $pmku->nama_perusahaan }}</td>
  	  <td>
        <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modalDetail" data-no_pengaduan="{{ $pmku->no_pengaduan }}" data-nama="{{ $pmku->nama }}" data-jenis_id="{{ $pmku->jenis_id }}" data-nomor_id="{{ $pmku->nomor_id }}" data-email="{{ $pmku->email }}" data-instansi="{{ $pmku->instansi }}" data-alamat="{{ $pmku->alamat }}" data-pesan="{{ $pmku->pesan }}" data-balasan="{{ $pmku->balasan }}" data-attachment= "{{ url($pmku->namafile ? $pmku->namafile : 'empty.jpg') }}" style="width: 150px;"><i class="fa fa-search"></i> Detail Permohonan</button>
      </td>
      @if($pmku->status_registrasi == 1)
        <td align="center"><span class="badge badge-danger badge-sm">Belum Diproses</span></td>
      @else 
    <td align="center"><span class="badge badge-success badge-sm">Sudah Diproses oleh {{ $pmku->updater->name }}</span></td>
      @endif
      <td align="center">
        @if($pmku->status_registrasi == 1)
        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.pmku.destroy', $pmku->id]]) !!}
        <a href="{{ route('admin.pmku.edit', $pmku->id) }}" class="btn btn-sm btn-primary btn-block" title="Edit">
          <i class="fa fa-comment text-black"></i> Proses
        </a>
        @else 
        <p> - </p>
        @role('superadmin')
          <button onclick="return confirm('Apakah Anda yakin untuk menghapus pelayanan pmku?')" type="submit" class="btn btn-sm btn-danger btn-block" title="Move to Trash">
            <i class="fa fa-trash"></i> Hapus
          </button>
        @endrole
        @endif
        {!! Form::close() !!}
      </td>
  	</tr>
    <?php $no++;?>
  @endforeach
  @include('backend.pmku.modal')
</table>
@include('backend.pmku.dashscript')