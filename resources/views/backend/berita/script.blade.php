@section('script')
  <script type="text/javascript">
    $('#title').on('blur',function(){
      var theTitle = this.value.toLowerCase().trim(),
        slugInput = $('#slug'),
        theSlug = theTitle.replace(/&/g, '-and-')
            .replace(/[^a-z0-9]+/g, '-')
            .replace(/\-\-+/g, '-')
            .replace(/^-+|-+$/g, '');

      slugInput.val(theSlug);
    });

    // var konten = document.getElementById("konten");
    //      CKEDITOR.replace(konten,{
    //      language:'en-gb'
    //    });
    //    CKEDITOR.config.allowedContent = true;
    //    CKEDITOR.config.height = 400;
    //    CKEDITOR.config.extraPlugins = ['justify','colorbutton'];

    // $('.js-selectize').selectize({
    //   sortField: 'text'
    // });

    $('#summernote').summernote({
      height: 300,                 // set editor height
      minHeight: null,             // set minimum height of editor
      maxHeight: null,             // set maximum height of editor
      focus: true                  // set focus to editable area after initializing summernote
    });

    $('.input-tags').selectize({
    delimiter: ',',
    persist: false,
    create: function(input) {
        return {
            value: input,
            text: input
        }
    }
    });
  </script>
@endsection
