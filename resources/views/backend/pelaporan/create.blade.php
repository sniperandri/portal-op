@extends('layouts.backend.main')
@section('pageTitle','Tambah Informasi')
@section('breadcrumbTitle','Tambah Informasi')
@section('breadcrumbParent','Pengaturan / Informasi')

@section('content')
	<div class="content-wrapper">
		@include('layouts.backend.breadcrumb')
		<!-- Main content -->
		    <div class="row">
		      <div class="col-lg-12">
		        {!! Form::model($pelaporan, [
		            'method' => 'POST',
		            'route' => 'admin.pelaporan.store',
								'id' => 'link-terkait-form',
								'files'=> TRUE,
		        ]) !!}

		        @include('backend.pelaporan.form')

		        {!! Form::close() !!}
		      </div>
		    </div>
		  <!-- ./row -->
	</div>
@endsection
@include('backend.pelaporan.script')