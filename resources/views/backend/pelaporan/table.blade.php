<table class="table table-striped">
  <tr>
    <th style="width: 10px">No</th>
    <th>Nomor Laporan</th>
    <th>Judul Laporan</th>
    <th class="text-left">Perusahaan</th>
    <th class="text-left">Bulan</th>
    <th class="text-left">Tahun</th>
    <th class="text-center">File Laporan</th>
    <th class="text-center">Aksi</th>
  </tr>
  <?php $no = paging_number($perPage);?>
  @foreach($pelaporans as $pelaporan)
    <tr>
      <td>{{ $no }}.</td>
      <td>{{ $pelaporan->no_pelaporan }}</td>
      <td>{{ $pelaporan->judul_laporan }}</td>
      <td>{{ $pelaporan->pmku->nama_perusahaan }}</td>
      <td>{{ $pelaporan->getbulan->nama }}</td>
      <td>{{ $pelaporan->gettahun->tahun }}</td>
      @if($pelaporan->konten)
        <?php
            $path = $pelaporan->konten;
            $ext = pathinfo($path, PATHINFO_EXTENSION);
        ?>
        @if($ext == 'pdf')
        <td align="center">
          <a href='{{ url($path) }}' class="btn btn-sm btn-info btn-block" title="View PDF" target="blank" ><i class="fa fa-eye"></i> View PDF</a>
          
          <!-- di tambahin tombol hapus di sini -->
          &nbsp;

          <a onclick="return confirm('Apakah Anda yakin untuk menghapus data?')" href="{{ route('admin.pelaporan.removeFile', $pelaporan->id) }}" class="btn btn-sm btn-danger btn-block" title="Move to Trash">
            <i class="fa fa-trash"></i> Hapus PDF
          </a>
        </td>
          @else 
                <td align="center"><a href="{{ url($path) }}" target="_blank">Download</a></td>
        @endif
      @else
        <td align="center"> - </td>
      @endif
      <td align="center">
        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.pelaporan.destroy', $pelaporan->id]]) !!}
        <a href="{{ route('admin.pelaporan.edit', $pelaporan->id) }}" class="btn btn-sm btn-warning btn-block" title="Edit">
          <i class="fa fa-edit text-black"></i> Edit
        </a>
          @role('superadmin')
          <button onclick="return confirm('Apakah Anda yakin untuk menghapus data?')" type="submit" class="btn btn-sm btn-danger btn-block" title="Move to Trash">
            <i class="fa fa-trash"></i> Hapus
          </button>
          @endrole
        {!! Form::close() !!}
      </td>
    </tr>
    <?php $no++;?>
  @endforeach
</table>