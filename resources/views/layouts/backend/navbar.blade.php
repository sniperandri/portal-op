  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand border-bottom navbar-dark bg-info">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>
        </ul>
    
        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Messages Dropdown Menu -->

            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                  <i class="fa fa-bell"></i>
                  <span class="badge badge-warning navbar-badge">0</span>
                </a>
            </li>

            <li class="nav-item dropdown">
            
                <a class="nav-link btn btn-dark btn-sm" href="{{ route('change-password') }}" title="Ubah Password">
                    <i class="fa fa-key"></i> Ubah Password
                </a>
            
            </li>
            &nbsp;&nbsp;
            <li class="nav-item dropdown">
    
                <a class="nav-link btn btn-dark btn-sm" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();" title="Logout">
                    <i class="fa fa-sign-out"></i> Logout
                </a>
            
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
        
        
            </li>
        </ul>
        </nav>
        <!-- /.navbar -->