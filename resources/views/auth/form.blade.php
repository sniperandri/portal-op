<div class="wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-th"></i> Ganti Password</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
                <div class="card-body">
                  @if (session('error'))
                    <div class="alert alert-danger">
                      {{ session('error') }}
                    </div>
                  @endif
                  @if (session('success'))
                    <div class="alert alert-success">
                      {{ session('success') }}
                    </div>
                  @endif

                  <div class="form-group {{ $errors->has('current-password') ? 'has-error' : ''}} m-input">
                    {!! Form::label('Password Sekarang') !!}

                    {!! Form::password('current-password', ['class'=> 'form-control']) !!}

                    @if($errors->has('current-password'))
                    <span class="help-block label-danger">{{ $errors->first('current-password') }}</span>
                    @endif
                  </div>

                  <div class="form-group {{ $errors->has('new-password') ? 'has-error' : ''}} m-input">
                    {!! Form::label('Password Baru') !!}

                    {!! Form::password('new-password', ['class'=> 'form-control']) !!}

                    @if($errors->has('new-password'))
                    <span class="help-block label-danger">{{ $errors->first('new-password') }}</span>
                    @endif
                  </div>

                  <div class="form-group {{ $errors->has('new-password_confirmation') ? 'has-error' : ''}} m-input">
                    {!! Form::label('Konfirmasi Password Baru') !!}

                    {!! Form::password('new-password_confirmation', ['class'=> 'form-control']) !!}

                    @if($errors->has('new-password_confirmation'))
                    <span class="help-block label-danger">{{ $errors->first('new-password_confirmation') }}</span>
                    @endif
                  </div>


                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-key"></i>  Ganti Password</button>
                  <a href="{{ route('home') }}" class="btn btn-warning"><i class="fa fa-undo"></i> Cancel</a>
                </div>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          <!-- right column -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@include('backend.informasi.script')