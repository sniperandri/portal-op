@extends('layouts.backend.main')
@section('pageTitle','Ganti Password')
@section('breadcrumbTitle','Ganti Password')
@section('breadcrumbParent','Ganti Password')

@section('content')
	<div class="content-wrapper">
		@include('layouts.backend.breadcrumb')
		<!-- Main content -->
		    <div class="row">
		      <div class="col-lg-12">
		        {!! Form::open([
		            'method' => 'POST',
		            'route' => 'change-password',
								'id' => 'link-terkait-form',
		        ]) !!}

		        @include('auth.form')

		        {!! Form::close() !!}
		      </div>
		    </div>
		  <!-- ./row -->
	</div>
@endsection