# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.9)
# Database: db_portal
# Generation Time: 2018-08-08 04:11:18 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table berita
# ------------------------------------------------------------

DROP TABLE IF EXISTS `berita`;

CREATE TABLE `berita` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view_count` int(11) NOT NULL,
  `create_id` int(11) DEFAULT NULL,
  `update_id` int(11) DEFAULT NULL,
  `delete_id` int(11) DEFAULT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status_aktif` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `berita_author_id_foreign` (`author_id`),
  CONSTRAINT `berita_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table front_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `front_menu`;

CREATE TABLE `front_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `nama_menu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `urutan_menu` int(11) NOT NULL,
  `create_id` int(11) DEFAULT NULL,
  `update_id` int(11) DEFAULT NULL,
  `delete_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status_aktif` tinyint(4) NOT NULL DEFAULT '1',
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `front_menu` WRITE;
/*!40000 ALTER TABLE `front_menu` DISABLE KEYS */;

INSERT INTO `front_menu` (`id`, `parent_id`, `nama_menu`, `urutan_menu`, `create_id`, `update_id`, `delete_id`, `created_at`, `updated_at`, `deleted_at`, `status_aktif`, `slug`, `url`)
VALUES
	(1,0,'Profil',1,NULL,NULL,NULL,'2018-06-22 18:30:03','2018-08-06 12:34:45',NULL,1,'profil',NULL),
	(2,0,'Berita',2,NULL,NULL,NULL,'2018-06-22 18:30:17','2018-06-22 18:37:29',NULL,1,'berita',NULL),
	(3,0,'Galeri Foto',3,NULL,NULL,NULL,'2018-06-22 18:30:29','2018-06-22 18:37:38',NULL,1,'galeri-foto',NULL),
	(4,0,'Pelayanan',4,NULL,NULL,NULL,'2018-06-22 18:30:41','2018-08-06 12:43:41',NULL,1,'registrasi-pmku',NULL),
	(5,0,'Hubungi Kami',5,NULL,NULL,NULL,'2018-06-22 18:30:56','2018-06-22 18:30:56',NULL,1,'hubungi-kami',NULL);

/*!40000 ALTER TABLE `front_menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table galeri_foto
# ------------------------------------------------------------

DROP TABLE IF EXISTS `galeri_foto`;

CREATE TABLE `galeri_foto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caption` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `namafile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_id` int(11) DEFAULT NULL,
  `update_id` int(11) DEFAULT NULL,
  `delete_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status_aktif` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `galeri_foto` WRITE;
/*!40000 ALTER TABLE `galeri_foto` DISABLE KEYS */;

INSERT INTO `galeri_foto` (`id`, `title`, `caption`, `namafile`, `extension`, `create_id`, `update_id`, `delete_id`, `created_at`, `updated_at`, `deleted_at`, `status_aktif`)
VALUES
	(1,'','','3','jpg',NULL,NULL,NULL,NULL,NULL,NULL,1);

/*!40000 ALTER TABLE `galeri_foto` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kategori
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_id` int(11) DEFAULT NULL,
  `update_id` int(11) DEFAULT NULL,
  `delete_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status_aktif` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `kategori_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2018_06_13_053403_alter_users_add_slugbio_column',1),
	(4,'2018_06_13_215231_front_menu',1),
	(5,'2018_06_13_215849_alter_table_frontmenu_add_slug',1),
	(6,'2018_06_18_141439_table_tampilan_depan',1),
	(7,'2018_06_19_142506_create_table_galerifoto',1),
	(8,'2018_06_19_180004_alter_frontmenu_add_url',1),
	(9,'2018_06_21_063656_create_category_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table tampilan_depan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tampilan_depan`;

CREATE TABLE `tampilan_depan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode_tampilan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_id` int(11) DEFAULT NULL,
  `update_id` int(11) DEFAULT NULL,
  `delete_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status_aktif` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `tampilan_depan` WRITE;
/*!40000 ALTER TABLE `tampilan_depan` DISABLE KEYS */;

INSERT INTO `tampilan_depan` (`id`, `kode_tampilan`, `content`, `foto`, `create_id`, `update_id`, `delete_id`, `created_at`, `updated_at`, `deleted_at`, `status_aktif`)
VALUES
	(1,'tagline','Keselamatan Berlayar adalah Prioritas Kami',NULL,NULL,NULL,NULL,'2018-06-22 18:32:14','2018-06-22 18:32:14',NULL,1),
	(2,'titleOne','Pelayanan Pelabuhan',NULL,NULL,NULL,NULL,'2018-06-22 18:32:40','2018-06-22 18:32:40',NULL,1),
	(3,'titleTwo','Kegiatan Otoritas',NULL,NULL,NULL,NULL,'2018-06-22 18:32:59','2018-06-22 18:32:59',NULL,1),
	(4,'titleThree','News Informasi',NULL,NULL,NULL,NULL,'2018-06-22 18:33:13','2018-06-22 18:33:13',NULL,1),
	(5,'headOffice','Jl. Palmas, No.1, Pelabuhan Tanjung Priok, 14310 <br />\r\nJl. Raya Pelabuhan, Tj. Priok <br />\r\nKota Jakarta Utara, Indonesia',NULL,NULL,NULL,NULL,'2018-06-22 18:33:49','2018-06-22 18:33:49',NULL,1),
	(6,'slideOneTitle','Berita Pelabuhan',NULL,NULL,NULL,NULL,'2018-06-22 18:34:39','2018-08-01 11:50:44',NULL,1),
	(7,'slideOneContent','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis expedita labore neque repudiandae repellendus provident doloremque similique! Libero vitae id, nihil iste nobis est rerum.',NULL,NULL,NULL,NULL,'2018-06-22 18:35:02','2018-06-22 18:35:02',NULL,1),
	(8,'slideTwoTitle','Slide Kedua',NULL,NULL,NULL,NULL,'2018-06-22 18:35:20','2018-06-22 18:35:20',NULL,1),
	(9,'slideTwoContent','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis expedita labore neque repudiandae repellendus provident doloremque similique! Libero vitae id, nihil iste nobis est rerum.',NULL,NULL,NULL,NULL,'2018-06-22 18:35:30','2018-06-22 18:35:30',NULL,1),
	(10,'slideThreeTitle','Slide Ketiga',NULL,NULL,NULL,NULL,'2018-06-22 18:35:41','2018-06-22 18:35:41',NULL,1),
	(11,'slideThreeContent','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis expedita labore neque repudiandae repellendus provident doloremque similique! Libero vitae id, nihil iste nobis est rerum.',NULL,NULL,NULL,NULL,'2018-06-22 18:35:48','2018-06-22 18:35:48',NULL,1),
	(12,'footer','Kementerian Perhubungan Republik Indonesia',NULL,NULL,NULL,NULL,'2018-08-06 14:56:28','2018-08-06 14:56:28',NULL,1);

/*!40000 ALTER TABLE `tampilan_depan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `slug`, `bio`)
VALUES
	(1,'superadmin','superadmin@portalop.com','$2y$10$bdVYLXWf/5TKS.Q343lZFOxcdr2tFkOnwfOW25aAu1T5hPXqtFN0S','QJyQF65hgWM5A2OwBYzzf7q6zuog3D1U2VKMNyDllOKXF4TN0LjQjtWEbUnd','2018-06-21 08:38:18',NULL,'superadmin','Super Administrator');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
