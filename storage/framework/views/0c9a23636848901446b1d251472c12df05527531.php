<div class="form-group<?php echo e($errors->has('title') ? ' is-invalid' : ''); ?>">
<?php echo Form::label('Kategori Foto'); ?> 
<?php
    if($errors->has('title')){
        $invalid = 'form-control is-invalid';
    }else{
        $invalid = 'form-control';
    }
?>
<?php echo Form::text('title', null, ['class'=>$invalid, 'placeholder' => 'Judul Kategori Foto']); ?>

<?php echo $errors->first('title', '<p class="invalid-feedback">:message</p>'); ?>

</div>

<?php echo Form::submit('Simpan', ['class'=>'btn btn-info']); ?>