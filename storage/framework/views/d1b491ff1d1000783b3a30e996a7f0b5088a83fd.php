<table class="table table-striped">
  <tr>
    <th style="width: 10px">No</th>
    <th>Link Terkait</th>
    <th>Deskripsi</th>
    <th class="text-center">Logo</th>
    <th class="text-center">Aksi</th>
  </tr>
  <?php $no = paging_number($perPage);?>
  <?php $__currentLoopData = $linkTerkaits; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $linkTerkait): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  	<tr>
  	  <td><?php echo e($no); ?>.</td>
      <td><?php echo e($linkTerkait->nama_instansi); ?></td>
      <td><?php echo e($linkTerkait->deskripsi); ?></td>
      <td align="center"><img src="<?php echo e(url($linkTerkait->logo_instansi)); ?>" height="70px" /></td>
      <td align="center">
        <?php echo Form::open(['method' => 'DELETE', 'route' => ['admin.link-terkait.destroy', $linkTerkait->id]]); ?>

        <a href="<?php echo e(route('admin.link-terkait.edit', $linkTerkait->id)); ?>" class="btn btn-sm btn-warning btn-block" title="Edit">
          <i class="fa fa-edit text-black"></i> Edit
        </a>
          <button onclick="return confirm('Apakah Anda yakin untuk menghapus data?')" type="submit" class="btn btn-sm btn-danger btn-block" title="Move to Trash">
            <i class="fa fa-trash"></i> Hapus
          </button>
        <?php echo Form::close(); ?>

      </td>
  	</tr>
    <?php $no++;?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>