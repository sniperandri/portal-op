<div class="form-group<?php echo e($errors->has('kode_jenis_usaha') ? ' is-invalid' : ''); ?>">
<?php echo Form::label('Kode Jenis Usaha'); ?> 
<?php
    if($errors->has('kode_jenis_usaha')){
        $invalid = 'form-control is-invalid';
    }else{
        $invalid = 'form-control';
    }
?>
<?php echo Form::text('kode_jenis_usaha', null, ['class'=>$invalid, 'placeholder' => 'Kode Jenis Usaha']); ?>

<?php echo $errors->first('kode_jenis_usaha', '<p class="invalid-feedback">:message</p>'); ?>

</div>

<div class="form-group<?php echo e($errors->has('jenis_usaha') ? ' is-invalid' : ''); ?>">
<?php echo Form::label('Jenis Usaha'); ?> 
<?php
    if($errors->has('jenis_usaha')){
        $invalid = 'form-control is-invalid';
    }else{
        $invalid = 'form-control';
    }
?>
<?php echo Form::text('jenis_usaha', null, ['class'=>$invalid, 'placeholder' => 'Jenis Usaha']); ?>

<?php echo $errors->first('jenis_usaha', '<p class="invalid-feedback">:message</p>'); ?>

</div>

<?php echo Form::submit('Simpan', ['class'=>'btn btn-info']); ?>