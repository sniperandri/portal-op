<?php $__env->startSection('title','Edit Kategori Foto'); ?>
<?php $__env->startSection('breadcrumb','Edit Kategori Foto'); ?>
<?php $__env->startSection('content'); ?>
        <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <?php echo $__env->make('layouts.backend.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo e(url('/admin/kategori-foto')); ?>">Pengaturan Foto</a></li>
                            <li class="breadcrumb-item"> Kategori Foto</li>
                            <li class="breadcrumb-item active"> Edit Kategori Foto</li>
                        </ol>
                        <div class="card border-primary mb-3 card-info">
                        <div class="card-header">Edit Kategori Foto</div>
                            <div class="card-body">
                                <?php echo Form::model($kategoriFoto,[
                                    'url'       => route('admin.kategori-foto.update', $kategoriFoto->id),
                                    'method'    => 'put',
                                    'class'     => 'form-horizontal' 
                                ]); ?>

                                
                                <?php echo $__env->make('backend.kategori-foto._form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                </div>
            </div>
    
          </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>