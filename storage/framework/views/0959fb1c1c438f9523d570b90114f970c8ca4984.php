<div class="wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-th"></i> Edit Link Terkait</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
                <div class="card-body">
                  <div class="form-group <?php echo e($errors->has('title') ? 'has-error' : ''); ?> m-input">
                    <?php echo Form::label('Judul'); ?>


                    <?php echo Form::text('title', null, ['class'=> 'form-control']); ?>


                    <?php if($errors->has('title')): ?>
                    <span class="help-block label-danger"><?php echo e($errors->first('title')); ?></span>
                    <?php endif; ?>
                  </div>

                  <div class="form-group <?php echo e($errors->has('konten') ? 'has-error' : ''); ?> m-input">
                    <?php echo Form::label('Konten'); ?>


                    <?php echo Form::textarea('konten', null, ['class'=> 'form-control', 'rows'=>8, 'id'=>'konten']); ?>


                    <?php if($errors->has('konten')): ?>
                    <span class="help-block label-danger"><?php echo e($errors->first('konten')); ?></span>
                    <?php endif; ?>
                  </div>

                  <div class="form-group <?php echo e($errors->has('gambar1') ? 'has-error' : ''); ?> m-input">
                    <?php echo Form::label('Foto'); ?>

                    &nbsp;<br>
                    <?php if($profil->gambar1): ?>
                      <a href="<?php echo e(url($profil->gambar1)); ?>" target="_blank"><img src="<?php echo e(url($profil->gambar1)); ?>" width="50%"></a><br>
                    <?php endif; ?>
                    <?php echo Form::file('gambar1', null, ['class'=> 'form-control']); ?>


                    <?php if($errors->has('gambar1')): ?>
                    <span class="help-block label-danger"><?php echo e($errors->first('gambar1')); ?></span>
                    <?php endif; ?>
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-info"><i class="fa fa-save"></i>  <?php echo e($profil->exists ? 'Update' : 'Save'); ?></button>
                  <a href="<?php echo e(route('admin.link-terkait.index')); ?>" class="btn btn-warning"><i class="fa fa-undo"></i> Cancel</a>
                </div>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          <!-- right column -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
