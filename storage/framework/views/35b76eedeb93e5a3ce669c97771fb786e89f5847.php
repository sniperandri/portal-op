<?php $__env->startSection('breadcrumb','Tampilan Depan'); ?>
<?php $__env->startSection('content'); ?>
        <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <?php echo $__env->make('layouts.backend.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('layouts._flash', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo e(url('/home')); ?>">Pengaturan Tampilan Depan</a></li>
                            <li class="breadcrumb-item active"> Tampilan Depan</li>
                        </ol>
                        <div class="card border-primary mb-3 card-info">
                        <div class="card-header">Daftar Tampilan Depan</div>
                        <div class="card-body">
                        <?php if (app('laratrust')->hasRole('superadmin')) : ?>
                        <p><a href="<?php echo e(route('admin.tampilan-depan.create')); ?>" class="btn btn-outline-info"><span><i class="fa fa-plus-circle"></i></span> Tambah Tampilan Depan</a></p>
                        <?php endif; // app('laratrust')->hasRole ?>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Kode Tampilan</th>
                                        <th>Konten</th>
                                        <th id="tampilkan">Tampilkan</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        </div>
                </div>
            </div>
    
          </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script>
    $(function(){
        $('table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '<?php echo route('admin.tampilan-depan.data'); ?>',
            columns: [
                { data: 'kode_tampilan', name: 'kode_tampilan' },
                { data: 'konten', name: 'konten' },
                { data: 'tampilkan', name: 'tampilkan' },
                { data: 'action', name: 'action', searchable: false, orderable: false }
            ],
            rowCallback: function( row, data, index ) {
                if ( data.tampilkan == 1 ) {
                    $('td:eq(2)', row).html('Ya');
                } else {
                    $('td:eq(2)', row).html('Tidak');
                }
            }
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>