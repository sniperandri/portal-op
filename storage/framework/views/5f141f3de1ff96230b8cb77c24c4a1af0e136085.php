<?php $__env->startSection('breadcrumb','Tambah Kategori Video'); ?>
<?php $__env->startSection('content'); ?>
        <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <?php echo $__env->make('layouts.backend.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo e(url('/admin/kategori-video')); ?>">Pengaturan Video</a></li>
                            <li class="breadcrumb-item"> Kategori Video</li>
                            <li class="breadcrumb-item active"> Tambah Kategori Video</li>
                        </ol>
                        <div class="card border-primary mb-3 card-info">
                        <div class="card-header">Tambah Kategori Video</div>
                            <div class="card-body">
                                <?php echo Form::open([
                                    'url'       => route('admin.kategori-video.store'),
                                    'method'    => 'post',
                                    'class'     => 'form-horizontal' 
                                ]); ?>

                                
                                <?php echo $__env->make('backend.kategori-video._form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                </div>
            </div>
    
          </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>