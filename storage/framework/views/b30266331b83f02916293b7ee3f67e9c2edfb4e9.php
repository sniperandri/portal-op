<table class="table table-striped">
  <tr>
    <th style="width: 10px">No</th>
    <th>Nomor Permohonan</th>
    <th>Nama Pemohon</th>
    <th>Detail Permohonan</th>
    <th>Status PPID</th>
    <th class="text-center" style="width: 200px">Aksi</th>
  </tr>
  <?php $no = paging_number($perPage);?>
  <?php $__currentLoopData = $ppids; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ppid): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  	<tr>
  	  <td><?php echo e($no); ?>.</td>
      <td><?php echo e($ppid->no_ppid); ?></td>
      <td><?php echo e($ppid->nama_lengkap); ?> <br>
          <?php echo e($ppid->email); ?>

      </td>
  	  <td>
      <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modalDetail" data-no_ppid="<?php echo e($ppid->no_ppid); ?>" data-fullname="<?php echo e($ppid->nama_lengkap); ?>" data-alamat="<?php echo e($ppid->alamat); ?>" data-pekerjaan="<?php echo e($ppid->pekerjaan); ?>" data-jenis_id="<?php echo e($ppid->jenis_id); ?>" data-nomor_id="<?php echo e($ppid->nomor_id); ?>" data-email="<?php echo e($ppid->email); ?>" data-telepon="<?php echo e($ppid->telepon); ?>" data-rincian_info="<?php echo e($ppid->rincian_info); ?>" data-tujuan_info="<?php echo e($ppid->tujuan_info); ?>" data-cara_info="<?php echo e($ppid->cara_info); ?>" data-cara_salinan="<?php echo e($ppid->cara_salinan); ?>" data-file_berkas="<?php echo e($ppid->file_berkas); ?>" data-balasan="<?php echo e($ppid->balasan); ?>" style="width: 150px;"><i class="fa fa-search"></i> Detail Permohonan</button>
      </td>
      <?php if($ppid->status_ppid == 1): ?>
        <td><span class="badge badge-danger badge-sm">Belum Diproses</span></td>
      <?php else: ?> 
        <td><span class="badge badge-success badge-sm">Sudah Diproses</span></td>
      <?php endif; ?>
      <td align="center">
        <?php if($ppid->status_ppid == 1): ?>
        <?php echo Form::open(['method' => 'DELETE', 'route' => ['admin.ppid.destroy', $ppid->id]]); ?>

        <a href="<?php echo e(route('admin.ppid.edit', $ppid->id)); ?>" class="btn btn-sm btn-primary btn-block" title="Edit">
          <i class="fa fa-comment text-black"></i> Proses
        </a>
        <?php else: ?> 
        <p> - </p>
        <?php if (app('laratrust')->hasRole('superadmin')) : ?>
          <button onclick="return confirm('Apakah Anda yakin untuk menghapus ppid?')" type="submit" class="btn btn-sm btn-danger btn-block" title="Move to Trash">
            <i class="fa fa-trash"></i> Hapus
          </button>
        <?php endif; // app('laratrust')->hasRole ?>
        <?php endif; ?>
        <?php echo Form::close(); ?>

      </td>
  	</tr>
    <?php $no++;?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php echo $__env->make('backend.ppid.modal', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</table>
<?php echo $__env->make('backend.ppid.dashscript', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>