<div class="wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-th"></i> Tambah Link Terkait</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
                <div class="card-body">
                  <div class="form-group <?php echo e($errors->has('nama_instansi') ? 'has-error' : ''); ?> m-input">
                    <?php echo Form::label('Link Website Instansi'); ?>


                    <?php echo Form::text('nama_instansi', null, ['class'=> 'form-control']); ?>


                    <?php if($errors->has('nama_instansi')): ?>
                    <span class="help-block label-danger"><?php echo e($errors->first('nama_instansi')); ?></span>
                    <?php endif; ?>
                  </div>

                  <div class="form-group <?php echo e($errors->has('deskripsi') ? 'has-error' : ''); ?> m-input">
                    <?php echo Form::label('Deskripsi'); ?>


                    <?php echo Form::textarea('deskripsi', null, ['class'=> 'form-control', 'rows'=>8]); ?>


                    <?php if($errors->has('deskripsi')): ?>
                    <span class="help-block label-danger"><?php echo e($errors->first('deskripsi')); ?></span>
                    <?php endif; ?>
                  </div>

                  <div class="form-group <?php echo e($errors->has('logo_instansi') ? 'has-error' : ''); ?> m-input">
                    <?php echo Form::label('Logo Instansi'); ?>

                    &nbsp;<br>
                    <?php if($linkTerkait->logo_instansi): ?>
                      <a href="<?php echo e(url($linkTerkait->logo_instansi)); ?>" target="_blank"><img src="<?php echo e(url($linkTerkait->logo_instansi)); ?>" width="10%"></a><br>
                    <?php endif; ?>
                    <?php echo Form::file('logo_instansi', null, ['class'=> 'form-control']); ?>


                    <?php if($errors->has('logo_instansi')): ?>
                    <span class="help-block label-danger"><?php echo e($errors->first('logo_instansi')); ?></span>
                    <?php endif; ?>
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-info"><i class="fa fa-save"></i>  <?php echo e($linkTerkait->exists ? 'Update' : 'Save'); ?></button>
                  <a href="<?php echo e(route('admin.link-terkait.index')); ?>" class="btn btn-warning"><i class="fa fa-undo"></i> Cancel</a>
                </div>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          <!-- right column -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
