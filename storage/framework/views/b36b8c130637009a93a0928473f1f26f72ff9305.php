<?php $__env->startSection('pageTitle', 'Berita'); ?>

<?php $__env->startSection('content'); ?>
	<section>
	    <div class="content-header">
	        <img src="<?php echo e(asset('frontend-asset/images/head.png')); ?>" />
	        <div class="container">
	            <div class="row">
	                <div class="col">
	                    <div class="desc">
	                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>Berita</span></small>
	                        <h2>Berita Terbaru</h2>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="news-detail">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-9">
	                    <p class="breadcrumb-list">
	                        <span><a href="#">Portal OP</a></span>
	                        <span><a href="#">Berita</a></span>
	                        <span>Berita Terbaru</span>
	                    </p>
	                    <?php if(!$news->count()): ?>
	                      <div class="news-desc">
		                      <?php if($term = request('term')): ?>
		                        <div class="alert alert-danger">
		                          <p>Berita Tentang <strong><?php echo e($term); ?></strong> Tidak Ditemukan</p>
		                        </div>
		                      <?php endif; ?>
	                      </div>
	                    <?php else: ?>
		                    <div class="news-desc">
		                    	<?php echo $__env->make('layouts.frontend.alert', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		                    </div>
		                    
		                    <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $new): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		                    <div class="news-desc">
		                        <article>
		                            <div class="news-header">
		                                <h5><a href="<?php echo e(route('berita.show', $new->slug)); ?>"><strong><?php echo e($new->title); ?></strong></a></h5>
		                                <div class="news-nav">
		                                    <div class="row">
		                                        <div class="col-md-9">
		                                            <small><i class="fa fa-clock-o"></i> <?php echo e($new->date); ?></small>|
		                                            <small>Oleh : <?php echo e(ucwords($new->author->name)); ?></small>|
		                                            <small>Kategori: <?php echo e($new->category->title); ?></small>
		                                        </div>
		                                        <div class="col-md-3 right">
		                                            <small><i class="fa fa-comment-o"></i> No Comments</small>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                            <?php if($new->image): ?>
		                            <div class="news-img">
		                                <img src="<?php echo e(url($new->image)); ?>" width="100%" />
		                            </div>
		                            <?php else: ?>
		                            <br>
		                            <?php endif; ?>
		                            <?php echo htmlspecialchars_decode(stripslashes($new->body)); ?>

		                        </article>
		                        <div class="sharethis-inline-share-buttons"></div>
		                    </div>
		                    <hr>
		                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		                <?php endif; ?>
	                </div>
	                <?php echo $__env->make('frontend.berita.sidebar-berita', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	            </div>
	        </div>
	    </div>
	    <div class="info-box">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6">
	                    <h4>How can we help you?</h4>
	                </div>
	                <div class="col-md-6 right">
	                    <a href="#" class="btn btn-blue">Send Feedback <i class="fa fa-chevron-right"></i></a>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $(function(){
            $("ul.list>li a").on('click',function(){
				$('li').removeClass();
                $(this).parent().addClass('active');
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>