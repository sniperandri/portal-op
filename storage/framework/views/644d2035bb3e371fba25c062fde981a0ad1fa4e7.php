<div class="wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-th"></i> Tambah Informasi</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
                <div class="card-body">
                  <div class="form-group <?php echo e($errors->has('jenis_informasi_id') ? 'has-error' : ''); ?>">
                    <?php echo Form::label('Jenis Informasi'); ?>


                    <?php echo Form::select('jenis_informasi_id', App\Model\JenisInformasi::pluck('nama','id'), null, ['class'=> 'js-selectize form-control','placeholder' => 'Pilih Jenis Informasi','name'=>'jenis_informasi_id']); ?>


                    <?php if($errors->has('jenis_informasi_id')): ?>
                    <span class="help-block text-danger"><?php echo e($errors->first('jenis_informasi_id')); ?></span>
                    <?php endif; ?>
                  </div>

                  <div class="form-group <?php echo e($errors->has('judul_informasi') ? 'has-error' : ''); ?> m-input">
                    <?php echo Form::label('Judul Informasi'); ?>


                    <?php echo Form::text('judul_informasi', null, ['class'=> 'form-control']); ?>


                    <?php if($errors->has('judul_informasi')): ?>
                    <span class="help-block label-danger"><?php echo e($errors->first('judul_informasi')); ?></span>
                    <?php endif; ?>
                  </div>

                  <div class="form-group <?php echo e($errors->has('konten') ? 'has-error' : ''); ?> m-input">
                    <?php echo Form::label('Konten'); ?>


                    <?php echo Form::textarea('konten', null, ['class'=> 'form-control','id'=>'konten']); ?>


                    <?php if($errors->has('konten')): ?>
                    <span class="help-block label-danger"><?php echo e($errors->first('konten')); ?></span>
                    <?php endif; ?>
                  </div>

                  <div class=" bulan form-group <?php echo e($errors->has('bulan') ? 'has-error' : ''); ?>">
                    <?php echo Form::label('Bulan'); ?>


                    <?php echo Form::select('bulan',App\Model\Bulan::pluck('nama','id'), null, ['class'=> 'js-selectize bulan form-control','placeholder' => 'Pilih Bulan (Opsional)']); ?>


                    <?php if($errors->has('bulan')): ?>
                    <span class="help-block text-danger"><?php echo e($errors->first('bulan')); ?></span>
                    <?php endif; ?>
                  </div>

                  <div class="tahun form-group <?php echo e($errors->has('tahun') ? 'has-error' : ''); ?>">
                    <?php echo Form::label('Tahun'); ?>


                    <?php echo Form::select('tahun', App\Model\Tahun::pluck('tahun','id'), null, ['class'=> 'js-selectize tahun form-control','placeholder' => 'Pilih Tahun (Opsional)','id'=>'tahun']); ?>


                    <?php if($errors->has('tahun')): ?>
                    <span class="help-block text-danger"><?php echo e($errors->first('tahun')); ?></span>
                    <?php endif; ?>
                  </div>

                  <div class="form-group <?php echo e($errors->has('gambar') ? 'has-error' : ''); ?> m-input">
                    <?php echo Form::label('Gambar / Dokumen PDF'); ?>

                    &nbsp;<br>
                    <?php if($informasi->gambar): ?>
                      <?php
                          $path = $informasi->gambar;
                          $ext = pathinfo($path, PATHINFO_EXTENSION);
                      ?>
                      <?php if($ext == 'pdf'): ?>
                        <object type="application/pdf" data="<?php echo e(url($path)); ?>" width="50%" height="500" style="height: 85vh;" class="domisili">No Support</object><br><br>
                      <?php else: ?> 
                        <a href="<?php echo e(url($path)); ?>" target="_blank"><img src="<?php echo e(url($path)); ?>" width="50%"></a><br>
                      <?php endif; ?>
                    <?php else: ?>
                    <?php endif; ?>
  
                    <?php echo Form::file('gambar', null, ['class'=> 'form-control']); ?>

                    <?php if($errors->has('gambar')): ?>
                    <span class="help-block label-danger"><?php echo e($errors->first('gambar')); ?></span>
                    <?php endif; ?>
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-info"><i class="fa fa-save"></i>  <?php echo e($informasi->exists ? 'Update' : 'Save'); ?></button>
                  <a href="<?php echo e(route('admin.informasi.index')); ?>" class="btn btn-warning"><i class="fa fa-undo"></i> Cancel</a>
                </div>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          <!-- right column -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php echo $__env->make('backend.informasi.script', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>