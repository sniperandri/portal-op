<div class="wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-th"></i> Tambah PPID</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
                <div class="card-body">
                    <div class="form-group <?php echo e($errors->has('balasan') ? 'has-error' : ''); ?> m-input">
                      <label>Balasan<sup>*</sup></label>

                      <?php echo Form::textarea('balasan', null, ['class'=> 'form-control','rows'=>8, 'placeholder' => 'Balasan Admin Otoritas Pelabuhan']); ?>


                      <?php if($errors->has('balasan')): ?>
                      <span class="help-block badge badge-danger"><?php echo e($errors->first('balasan')); ?></span>
                      <?php endif; ?>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-success"><i class="fa fa-paper-plane"></i>  <?php echo e($ppid->exists ? 'Balas dan Kirim Email' : 'Save'); ?></button>
                  <a href="<?php echo e(route('admin.ppid.index')); ?>" class="btn btn-warning"><i class="fa fa-undo"></i> Cancel</a>
                </div>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          <!-- right column -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
