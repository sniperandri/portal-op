<table class="table table-striped">
  <tr>
    <th style="width: 10px">No</th>
    <th>Judul Informasi</th>
    <th class="text-center">Gambar</th>
    <th class="text-center">Aksi</th>
  </tr>
  <?php $no = paging_number($perPage);?>
  <?php $__currentLoopData = $informasis; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $informasi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>
      <td><?php echo e($no); ?>.</td>
      <td><?php echo e($informasi->judul_informasi); ?></td>
      <?php if($informasi->gambar): ?>
        <?php
            $path = $informasi->gambar;
            $ext = pathinfo($path, PATHINFO_EXTENSION);
        ?>
        <?php if($ext == 'pdf'): ?>
        <td align="center">
          <a href='<?php echo e(url($path)); ?>' class="btn btn-sm btn-info btn-block" title="View PDF" target="blank" ><i class="fa fa-eye"></i> View PDF</a>
          
          <!-- di tambahin tombol hapus di sini -->
          &nbsp;

          <a onclick="return confirm('Apakah Anda yakin untuk menghapus data?')" href="<?php echo e(route('admin.informasi.removeFile', $informasi->id)); ?>" class="btn btn-sm btn-danger btn-block" title="Move to Trash">
            <i class="fa fa-trash"></i> Hapus PDF
          </a>
        </td>
          <?php else: ?> 
                <td align="center"><a href="<?php echo e(url($path)); ?>" target="_blank"><img src="<?php echo e(url($path)); ?>" height="70px" /></a></td>
        <?php endif; ?>
      <?php else: ?>
        <td align="center"> - </td>
      <?php endif; ?>
      <td align="center">
        <?php echo Form::open(['method' => 'DELETE', 'route' => ['admin.informasi.destroy', $informasi->id]]); ?>

        <a href="<?php echo e(route('admin.informasi.edit', $informasi->id)); ?>" class="btn btn-sm btn-warning btn-block" title="Edit">
          <i class="fa fa-edit text-black"></i> Edit
        </a>
          <?php if (app('laratrust')->hasRole('superadmin')) : ?>
          <button onclick="return confirm('Apakah Anda yakin untuk menghapus data?')" type="submit" class="btn btn-sm btn-danger btn-block" title="Move to Trash">
            <i class="fa fa-trash"></i> Hapus
          </button>
          <?php endif; // app('laratrust')->hasRole ?>
        <?php echo Form::close(); ?>

      </td>
    </tr>
    <?php $no++;?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>