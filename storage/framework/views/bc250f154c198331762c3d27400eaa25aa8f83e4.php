<?php $__env->startSection('pageTitle','Update Informasi'); ?>
<?php $__env->startSection('breadcrumbTitle','Update Informasi'); ?>
<?php $__env->startSection('breadcrumbParent','Pengaturan / Informasi'); ?>

<?php $__env->startSection('content'); ?>
	<div class="content-wrapper">
		<?php echo $__env->make('layouts.backend.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<!-- Main content -->
		    <div class="row">
		      <div class="col-lg-12">
		        <?php echo Form::model($informasi, [
		            'method' => 'PUT',
		            'route' => ['admin.informasi.update', $informasi->id],
		            'id' => 'link-terkait-form',
		            'files'=> TRUE,
		        ]); ?>


		        <?php echo $__env->make('backend.informasi.form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

		        <?php echo Form::close(); ?>

		      </div>
		    </div>
		  <!-- ./row -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.informasi.script', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>