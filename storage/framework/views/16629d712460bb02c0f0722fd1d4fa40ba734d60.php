<table class="table table-striped">
  <tr>
    <th style="width: 10px">No</th>
    <th>Nomor Permohonan</th>
    <th>Nama Pemohon</th>
    <th>Pesan</th>
    <th>Detail Permohonan</th>
    <th class="text-center">Status Pengaduan</th>
    <th class="text-center" style="width: 200px">Aksi</th>
  </tr>
  <?php $no = paging_number($perPage);?>
  <?php $__currentLoopData = $pengaduans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pengaduan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  	<tr>
  	  <td><?php echo e($no); ?>.</td>
      <td><?php echo e($pengaduan->no_pengaduan); ?></td>
      <td><?php echo e($pengaduan->nama); ?> <br>
          <?php echo e($pengaduan->email); ?>

      </td>
      <td><?php echo e($pengaduan->pesan); ?></td>
  	  <td>
        <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modalDetail" data-no_pengaduan="<?php echo e($pengaduan->no_pengaduan); ?>" data-nama="<?php echo e($pengaduan->nama); ?>" data-jenis_id="<?php echo e($pengaduan->jenis_id); ?>" data-nomor_id="<?php echo e($pengaduan->nomor_id); ?>" data-email="<?php echo e($pengaduan->email); ?>" data-instansi="<?php echo e($pengaduan->instansi); ?>" data-alamat="<?php echo e($pengaduan->alamat); ?>" data-pesan="<?php echo e($pengaduan->pesan); ?>" data-balasan="<?php echo e($pengaduan->balasan); ?>" data-attachment= "<?php echo e(url($pengaduan->namafile ? $pengaduan->namafile : 'empty.jpg')); ?>" style="width: 150px;"><i class="fa fa-search"></i> Detail Permohonan</button>
      </td>
      <?php if($pengaduan->status_pengaduan == 1): ?>
        <td align="center"><span class="badge badge-danger badge-sm">Belum Diproses</span></td>
      <?php else: ?> 
    <td align="center"><span class="badge badge-success badge-sm">Sudah Diproses oleh <?php echo e($pengaduan->updater->name); ?></span></td>
      <?php endif; ?>
      <td align="center">
        <?php if($pengaduan->status_pengaduan == 1): ?>
        <?php echo Form::open(['method' => 'DELETE', 'route' => ['admin.pengaduan.destroy', $pengaduan->id]]); ?>

        <a href="<?php echo e(route('admin.pengaduan.edit', $pengaduan->id)); ?>" class="btn btn-sm btn-primary btn-block" title="Edit">
          <i class="fa fa-comment text-black"></i> Proses
        </a>
        <?php else: ?> 
        <p> - </p>
        <?php if (app('laratrust')->hasRole('superadmin')) : ?>
          <button onclick="return confirm('Apakah Anda yakin untuk menghapus pengaduan?')" type="submit" class="btn btn-sm btn-danger btn-block" title="Move to Trash">
            <i class="fa fa-trash"></i> Hapus
          </button>
        <?php endif; // app('laratrust')->hasRole ?>
        <?php endif; ?>
        <?php echo Form::close(); ?>

      </td>
  	</tr>
    <?php $no++;?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php echo $__env->make('backend.pengaduan.modal', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</table>
<?php echo $__env->make('backend.pengaduan.dashscript', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>