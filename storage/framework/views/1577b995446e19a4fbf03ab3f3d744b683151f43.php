<?php $__env->startSection('pageTitle','Update Link Terkait'); ?>
<?php $__env->startSection('breadcrumbTitle','Update Link Terkait'); ?>
<?php $__env->startSection('breadcrumbParent','Pengaturan / Link Terkait'); ?>

<?php $__env->startSection('content'); ?>
	<div class="content-wrapper">
		<?php echo $__env->make('layouts.backend.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<!-- Main content -->
		    <div class="row">
		      <div class="col-lg-12">
		        <?php echo Form::model($linkTerkait, [
		            'method' => 'PUT',
		            'route' => ['admin.link-terkait.update', $linkTerkait->id],
		            'id' => 'link-terkait-form',
		            'files'=> TRUE,
		        ]); ?>


		        <?php echo $__env->make('backend.link-terkait.form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

		        <?php echo Form::close(); ?>

		      </div>
		    </div>
		  <!-- ./row -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>