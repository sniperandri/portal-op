<table class="table table-striped">
  <tr>
    <th style="width: 10px">No</th>
    <th>Title</th>
    <th class="text-center">Caption</th>
    <th class="text-center">Nama File</th>
    <th class="text-center">Aksi</th>
  </tr>
  <?php $no = paging_number($perPage);?>
  <?php $__currentLoopData = $galeriFotos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $galeriFoto): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  	<tr>
  	  <td><?php echo e($no); ?>.</td>
  	  <td><?php echo e($galeriFoto->title); ?></td>
  	  <td align="center"><?php echo e($galeriFoto->caption); ?></td>
  	  <td align="center"><?php echo e($galeriFoto->namafile); ?></td>
      <td align="center" ="center">
        <?php echo Form::open(['style'=>'display:inline-block;', 'method' => 'PUT', 'route' => ['admin.galeri-foto.restore', $galeriFoto->id]]); ?>

          <button title="Restore" class="btn btn-warning btn-sm btn-block">
              <i class="fa fa-refresh text-black"></i> Restore
          </button>
        <?php echo Form::close(); ?>

        <?php echo Form::open(['style'=>'display:inline-block;', 'method' => 'DELETE', 'route' => ['admin.galeri-foto.force-destroy', $galeriFoto->id]]); ?>

          <button title="Permanent Delete" onclick="return confirm('You are about to delete foto permanently. Are you sure?')" type="submit" class="btn btn-danger btn-sm btn-block">
            <i class="fa fa-times-circle"></i> Hapus Permanen
          </button>
        <?php echo Form::close(); ?>

      </td>
  	</tr>
    <?php $no++;?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>