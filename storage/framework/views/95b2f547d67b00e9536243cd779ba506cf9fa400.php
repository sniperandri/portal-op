<div class="form-group<?php echo e($errors->has('kode_pelayanan') ? ' is-invalid' : ''); ?>">
<?php echo Form::label('Kode Pelayanan'); ?> 
<?php
    if($errors->has('kode_pelayanan')){
        $invalid = 'form-control is-invalid';
    }else{
        $invalid = 'form-control';
    }
?>
<?php echo Form::text('kode_pelayanan', null, ['class'=>$invalid, 'placeholder' => 'Kode Pelayanan']); ?>

<?php echo $errors->first('kode_pelayanan', '<p class="invalid-feedback">:message</p>'); ?>

</div>

<div class="form-group<?php echo e($errors->has('nama_pelayanan') ? ' is-invalid' : ''); ?>">
<?php echo Form::label('Nama Pelayanan'); ?> 
<?php
    if($errors->has('nama_pelayanan')){
        $invalid = 'form-control is-invalid';
    }else{
        $invalid = 'form-control';
    }
?>
<?php echo Form::text('nama_pelayanan', null, ['class'=>$invalid, 'placeholder' => 'Nama Pelayanan']); ?>

<?php echo $errors->first('nama_pelayanan', '<p class="invalid-feedback">:message</p>'); ?>

</div>

<div class="form-group<?php echo e($errors->has('keterangan') ? ' is-invalid' : ''); ?>">
<?php echo Form::label('Keterangan Pelayanan'); ?> 
<?php
    if($errors->has('keterangan')){
        $invalid = 'form-control is-invalid';
    }else{
        $invalid = 'form-control';
    }
?>
<?php echo Form::textarea('keterangan', null, ['class'=> 'form-control','rows'=>8, 'placeholder' => '']); ?>

<?php echo $errors->first('keterangan', '<p class="invalid-feedback">:message</p>'); ?>

</div>

<div class="form-group <?php echo e($errors->has('tipe_form') ? 'has-error' : ''); ?> m-input">
    <label>Tipe Form<sup>*</sup></label>

    <?php echo Form::select('tipe_form', ['1'=>1,'2'=>2], null, ['class'=> 'js-selectize form-control','placeholder' => 'Pilih Tipe Formulir']); ?>


    <?php if($errors->has('tipe_form')): ?>
    <span class="help-block badge badge-danger"><?php echo e($errors->first('tipe_form')); ?></span>
    <?php endif; ?>
</div>

<?php echo Form::submit('Simpan', ['class'=>'btn btn-info']); ?>