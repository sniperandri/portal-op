<?php $__env->startSection('pageTitle', 'Profile'); ?>

<?php $__env->startSection('content'); ?>
	<section>
	    <div class="content-header">
	        <img src="<?php echo e(asset('frontend-asset/images/bg-header2.jpg')); ?>" />
	        <div class="container">
	            <div class="row">
	                <div class="col">
	                    <div class="desc">
	                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>Galeri </span></small>
	                        <h2>Galeri Video</h2>
	                    </div>
	                </div>
	            </div>
	        </div>
		</div>
		<div class="info-box">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<h4>Tampilkan Kategori : </h4>
						</div>
						<div class="col-md-9">
							<?php $__currentLoopData = $kategoriVideos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kategoriVideos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="btn btn-blue"><input style="cursor: pointer;" id="<?php echo e($kategoriVideos->slug); ?>" type="checkbox" aria-label="<?php echo e($kategoriVideos->title); ?>" name="kategori" checked="checked" value="<?php echo e($kategoriVideos->slug); ?>">  <?php echo e($kategoriVideos->title); ?> <i class="fa fa-chevron-down"></i></div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</div>
					</div>
				</div>
			</div>
	    <div class="content-box">
	        <div class="container">
	            <div class="row">
	            	<?php $__currentLoopData = $gallerys; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	            		<div class="col-md-4 gambar">
							<div class="news-box" data-kategori="<?php echo e($gallery->kategoriVideo->slug); ?>">
	            		        <div class="img-box">
	            		            <a class="gallery-image-link" href="<?php echo e(url($gallery->link_video)); ?>" data-fancybox="gallery-set" data-caption="<?php echo e($gallery->judul_video); ?>"><iframe width="350" height="180" src="<?php echo e($gallery->link_video); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
	            		        </div>
	            		        <div class="desc">
	            		            <h5><a href="#"><?php echo e($gallery->judul_video); ?></a></h5>
	            		            <small><i class="fa fa-clock-o"></i> <?php echo e(date('d M Y H:i:s', strtotime($gallery->created_at))); ?></small>
	            		        </div>
	            		    </div>
	            		</div>
	            	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	            </div>
	        </div>
	    </div>
	    <div class="info-box">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6">
	                    
	                </div>
	                <div class="col-md-6 right">
	                    
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script>
	$(document).ready(function(){
		$("#kategori-satu").on('change',function(evt){
			updateProductView("kategori-satu", evt.target.checked);
		});
		$("#kategori-dua").on('change',function(evt){
			updateProductView("kategori-dua", evt.target.checked);
		});
		$("#kategori-tiga").on('change',function(evt){
			updateProductView("kategori-tiga", evt.target.checked);
		});

		function updateProductView(categoryName, bVisible) {
            // get a list of the product items for the given category.
            // Use the data attributes to narrow the list
            var dataSelectorVal = "";
            switch (categoryName) {
            case "kategori-satu":
                dataSelectorVal = "[data-kategori='kategori-satu']";
                break;
            case "kategori-dua":
                dataSelectorVal = "[data-kategori='kategori-dua']";
                break;
            case "kategori-tiga":
                dataSelectorVal = "[data-kategori='kategori-tiga']";
                break;
            }
            // use the has() function to select the li tags that are product items
            // that contain the h2 tag with the corresponding data attribute value
            $(".gambar").has(dataSelectorVal).css('display', bVisible ? "" : "none");
        }
	});

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>