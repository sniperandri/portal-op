<?php $__env->startSection('pageTitle', 'Whistleblower'); ?>

<?php $__env->startSection('content'); ?>
	<section>
	    <div class="content-header">
	        <img src="<?php echo e(asset('frontend-asset/images/bg-header3.png')); ?>" />
	        <div class="container">
	            <div class="row">
	                <div class="col">
	                    <div class="desc">
	                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>Kontak</span></small>
	                        <h2>Whistleblower</h2>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="content-box">
	        <div class="container">
	            <div class="row">
	                <div class="col">
	                    <label class="content-label">Whistleblower</label>
	                </div>
	            </div>
            	<?php echo $__env->make('frontend.pengaduan.message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	            <?php echo Form::model($pengaduan, [
	                'method' => 'POST',
	                'route' => 'pengaduan.store',
	                'id' => 'frontend-pengaduan-form',
	                'files' => true,
	            ]); ?>

	            <div class="row">
	                <div class="col">
	                    <div class="contact-form">
	                        <form role="form">
	                        	<p align="justify"><em>Form whistleblower ini digunakan untuk memproses pengaduan dan pemberian informasi oleh whistleblower sehubungan dengan adanya perbuatan yang melanggar perundang-undangan, peraturan/standar, kode etik, dan kebijakan, serta tindakan lain yang sejenis berupa ancaman langsung atas kepentingan umum, serta Korupsi, Kolusi, dan Nepotisme (KKN).</em></p>
	                        	<p align="justify"><em>Whistleblower adalah seseorang yang melaporkan perbuatan berindikasi tindak pidana/pelanggaran dan memiliki akses informasi yang memadai atas terjadinya indikasi tindakan tersebut.</em></p>
	                        	<p align="justify"><strong><em>Anda tidak perlu khawatir terungkapnya identitas diri Anda karena Kantor Otoritas Pelabuhan Tanjung Priok akan merahasiakan identitas diri Anda sebagai whistleblower. Kami menghargai informasi yang Anda laporkan. Fokus kami kepada materi informasi yang Anda laporkan.</em></strong></p>
	                            <label class="contact-label"><span>Data Pengirim</span></label>
	                            <div class="row"> 
	                                <div class="col-md-12">
	                                	<div class="form-group <?php echo e($errors->has('nama') ? 'has-error' : ''); ?> m-input">
	                                	  <label>Nama <sup>*</sup></label>

	                                	  <?php echo Form::text('nama', null, ['class'=> 'form-control']); ?>


	                                	  <?php if($errors->has('nama')): ?>
	                                	  <span class="help-block badge badge-danger"><?php echo e($errors->first('nama')); ?></span>
	                                	  <?php endif; ?>
										</div>
										
										<div class="form-group <?php echo e($errors->has('jenis_id') ? 'has-error' : ''); ?> m-input">
											<label>Jenis Identitas<sup>*</sup></label>

											<?php echo Form::select('jenis_id',['KTP' => 'KTP', 'NPWP' => 'NPWP', 'SIM' => 'SIM'],null,['class'=>'selectpicker','title' => 'Pilih jenis identitas ...','id'=>'identitas-id','data-live-search'=>'true']); ?>


											<?php if($errors->has('jenis_id')): ?>
											<span class="help-block badge badge-danger"><?php echo e($errors->first('jenis_id')); ?></span>
											<?php endif; ?>
										</div>

										<div class="form-group <?php echo e($errors->has('nomor_id') ? 'has-error' : ''); ?> m-input">
											<label>Nomor Identitas<sup>*</sup></label>

											<?php echo Form::text('nomor_id', null, ['class'=> 'form-control']); ?>


											<?php if($errors->has('nomor_id')): ?>
											<span class="help-block badge badge-danger"><?php echo e($errors->first('nomor_id')); ?></span>
											<?php endif; ?>
										</div>

	                                	<div class="form-group <?php echo e($errors->has('email') ? 'has-error' : ''); ?> m-input">
	                                	  <label>Email <sup>*</sup></label>

	                                	  <?php echo Form::text('email', null, ['class'=> 'form-control']); ?>


	                                	  <?php if($errors->has('email')): ?>
	                                	  <span class="help-block badge badge-danger"><?php echo e($errors->first('email')); ?></span>
	                                	  <?php endif; ?>
	                                	</div>
	                                	<div class="form-group <?php echo e($errors->has('instansi') ? 'has-error' : ''); ?> m-input">
	                                	  <label>Instansi</label>

	                                	  <?php echo Form::text('instansi', null, ['class'=> 'form-control']); ?>


	                                	  <?php if($errors->has('instansi')): ?>
	                                	  <span class="help-block badge badge-danger"><?php echo e($errors->first('instansi')); ?></span>
	                                	  <?php endif; ?>
	                                	</div>
	                                    <div class="form-group <?php echo e($errors->has('alamat') ? 'has-error' : ''); ?> m-input">
	                                      <label>Alamat</label>

	                                      <?php echo Form::textarea('alamat', null, ['class'=> 'form-control','rows'=>3]); ?>


	                                      <?php if($errors->has('alamat')): ?>
	                                      <span class="help-block badge badge-danger"><?php echo e($errors->first('alamat')); ?></span>
	                                      <?php endif; ?>
	                                    </div>
	                                    <div class="form-group <?php echo e($errors->has('pesan') ? 'has-error' : ''); ?> m-input">
	                                      <label>Pesan <sup>*</sup></label>

	                                      <?php echo Form::textarea('pesan', null, ['class'=> 'form-control','rows'=>8]); ?>


	                                      <?php if($errors->has('pesan')): ?>
	                                      <span class="help-block badge badge-danger"><?php echo e($errors->first('pesan')); ?></span>
	                                      <?php endif; ?>
	                                    </div>
	                                    <div class="form-group <?php echo e($errors->has('namafile') ? 'has-error' : ''); ?> m-input">
	                                        <label>Upload Foto (Jika Diperlukan)</label>
	                                        <?php echo Form::file('namafile',['class'=>'form-control-file']); ?>

	                                        <?php if($errors->has('namafile')): ?>
	                                        <span class="help-block badge badge-danger"><?php echo e($errors->first('namafile')); ?></span>
	                                        <?php endif; ?>
	                                    </div>
	                                </div>
	                            </div>
	                            
	                            <small><sup>*</sup> <i>Tidak boleh kosong</i></small>
	                            <div class="btn-box">
	                                <button type="submit" class="btn btn-blue">Send Form</button>
	                                <button type="reset" class="btn btn-org">Cancel</button>
	                            </div>
	                    </div>
	                </div>
	            </div>
	        <?php echo Form::close(); ?>

	        </div>
	    </div>
	    <div class="info-box">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6">
	                    
	                </div>
	                <div class="col-md-6 right">
	                    
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>