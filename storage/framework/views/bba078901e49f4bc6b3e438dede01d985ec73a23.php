<?php $__env->startSection('pageTitle', $konten->nama); ?>

<?php $__env->startSection('content'); ?>
<section>
    <div class="content-header">
        <img src="<?php echo e(asset('frontend-asset/images/bg-header3.png')); ?>" />
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="desc">
                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>Informasi</span></small>
                        <h2><?php echo e($konten->judul_informasi); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-box">
        <div class="container">
            <div class="row">
                <div class="col">
                    <label class="content-label"><?php echo e($konten->judul_informasi); ?></label>
                </div>
            </div>
        	<?php echo $__env->make('frontend.message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="row">
                <div class="col-md-12">
						<div align="center">
							<?php if($konten->gambar): ?>
							<img class="img img-responsive" src="<?php echo e(url($konten->gambar)); ?>" alt="" width="50%">
							<?php endif; ?>
						</div>
					<br>
                   	<?php echo htmlspecialchars_decode(stripslashes($konten->konten)); ?>

                    <?php
                        $path = $konten->gambar;
                        $ext = pathinfo($path, PATHINFO_EXTENSION);
                    ?>

                    <?php if($konten->gambar): ?>
                        <?php if($ext == 'pdf'): ?>
                            <object type="application/pdf" data="<?php echo e(url($path)); ?>" width="100%" height="500" style="height: 85vh;" class="domisili">No Support</object>
                        <?php else: ?> 
                            <a href="<?php echo e(url($path)); ?>" target="_blank"><img src="<?php echo e(url($path)); ?>" width="100%"></a>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>