<label class="contact-label"><span>Data Perusahaan</span></label>
<div class="row">
    <div class="col-md-6">
        <div class="form-group <?php echo e($errors->has('badan_usaha_id') ? 'has-error' : ''); ?> m-input">
            <label>Badan Usaha <sup>*</sup></label>
            <?php echo Form::text('badan_usaha', null, ['class'=> 'form-control','placeholder'=>Auth::user()->pmku->badan_usaha,'disabled']); ?>

            <?php if($errors->has('badan_usaha_id')): ?>
            <span class="help-block badge badge-danger"><?php echo e($errors->first('badan_usaha_id')); ?></span>
            <?php endif; ?>
        </div>
        <div class="form-group <?php echo e($errors->has('jenis_usaha_id') ? 'has-error' : ''); ?> m-input">
            <label>Bidang Usaha <sup>*</sup></label>
            <?php echo Form::text('jenis_usaha_id', null, ['class'=> 'form-control','placeholder'=>Auth::user()->pmku->jenisUsaha->jenis_usaha,'disabled']); ?>

            <?php if($errors->has('jenis_usaha_id')): ?>
            <span class="help-block badge badge-danger"><?php echo e($errors->first('jenis_usaha_id')); ?></span>
            <?php endif; ?>
        </div>
        <div class="form-group <?php echo e($errors->has('npwp') ? 'has-error' : ''); ?> m-input">
        <label>NPWP <sup>*</sup></label>
        
        <?php echo Form::text('npwp', null , ['class'=> 'form-control', 'placeholder' => Auth::user()->pmku->npwp ,'disabled']); ?>


        <?php if($errors->has('npwp')): ?>
        <span class="help-block badge badge-danger"><?php echo e($errors->first('npwp')); ?></span>
        <?php endif; ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group <?php echo e($errors->has('nama_perusahaan') ? 'has-error' : ''); ?> m-input">
            <label>Nama Perusahaan <sup>*</sup></label>
            <?php echo Form::text('nama_perusahaan', Auth::user()->nama_perusahaan, ['class'=> 'form-control', 'placeholder' => Auth::user()->nama_instansi, 'disabled'=>'disabled' ]); ?>


            <?php if($errors->has('nama_perusahaan')): ?>
            <span class="help-block badge badge-danger"><?php echo e($errors->first('nama_perusahaan')); ?></span>
            <?php endif; ?>
        </div>
        <div class="form-group <?php echo e($errors->has('nomor_siup') ? 'has-error' : ''); ?> m-input">
            <label>Nomor Siup<sup>*</sup></label>
            
            <?php echo Form::text('nomor_siup', null, ['class'=> 'form-control', 'placeholder' => Auth::user()->pmku->nomor_siup,'disabled']); ?>


            <?php if($errors->has('nomor_siup')): ?>
            <span class="help-block badge badge-danger"><?php echo e($errors->first('nomor_siup')); ?></span>
            <?php endif; ?>
        </div>
        <div class="form-group <?php echo e($errors->has('tanggal_siup') ? 'has-error' : ''); ?> m-input">

            <label>Tanggal Terbit SIUP <sup>*</sup></label>
            
        <input class="form-control" id="date" name="tanggal_siup" placeholder=<?php echo e(Auth::user()->pmku->tanggal_siup); ?> type="text" disabled>

            <?php if($errors->has('tanggal_siup')): ?>
            <span class="help-block badge badge-danger"><?php echo e($errors->first('tanggal_siup')); ?></span>
            <?php endif; ?>
        </div>
        
        
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group <?php echo e($errors->has('file_npwp') ? 'has-error' : ''); ?> m-input">
            <label>NPWP Perusahaan <sup>*</sup></label><br>
            <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modalNPWP" data-ktp= "<?php echo e(url(Auth::user() ? Auth::user()->pmku->file_npwp : 'empty.jpg')); ?>"><i class="fa fa-search"></i> Lihat File NPWP Perusahaan
            </button>
        </div>
        <div class="form-group <?php echo e($errors->has('file_struktur') ? 'has-error' : ''); ?> m-input">
            <label>Upload Dokumen Struktur Organisasi Perusahaan <sup>*</sup></label><br>
            <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modalStruktur" data-struktur= "<?php echo e(url(Auth::user() ? Auth::user()->pmku->file_struktur : 'empty.jpg')); ?>"><i class="fa fa-search"></i> Lihat File Struktur Organisasi Perusahaan</button>
        </div>
        <div class="form-group <?php echo e($errors->has('file_akta') ? 'has-error' : ''); ?> m-input">
            <label>Upload Dokumen Akta SIUP KUM HAM <sup>*</sup></label><br>
            <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modalAkta" data-akta= "<?php echo e(url(Auth::user() ? Auth::user()->pmku->file_akta : 'empty.jpg')); ?>"><i class="fa fa-search"></i> Lihat File Dokumen Akta SIUP KUM HAM</button>
        </div>
    </div>
    <div class="col-md-6">

        <div class="form-group <?php echo e($errors->has('file_domisili') ? 'has-error' : ''); ?> m-input">
            <label>Upload Dokumen Surat Keterangan Domisili <sup>*</sup></label><br>
            <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modalDomisili" data-domisili= "<?php echo e(url(Auth::user() ? Auth::user()->pmku->file_domisili : 'empty.jpg')); ?>"><i class="fa fa-search"></i> Lihat File Surat Keterangan Domisili</button>
        </div>
    </div>
</div>
<label class="contact-label"><span>Data Kantor</span></label>
<div class="row">
    <div class="col-md-6">
        <div class="form-group <?php echo e($errors->has('nomor_akta') ? 'has-error' : ''); ?> m-input">
            <label>No. Akta Pendirian Perusahaan <sup>*</sup></label>

            <?php echo Form::text('nomor_akta', null, ['class'=> 'form-control','placeholder'=>Auth::user()->pmku->nomor_akta,'disabled']); ?>


            <?php if($errors->has('nomor_akta')): ?>
            <span class="help-block badge badge-danger"><?php echo e($errors->first('nomor_akta')); ?></span>
            <?php endif; ?>
        </div>
        <div class="form-group <?php echo e($errors->has('wilayah_id') ? 'has-error' : ''); ?> m-input">
            <label>Wilayah Domisili Kantor <sup>*</sup></label>
            <?php echo Form::text('wilayah_id', null, ['class'=> 'form-control','placeholder'=>Auth::user()->pmku->wilayah->nama_wilayah,'disabled']); ?>

            <?php if($errors->has('wilayah_id')): ?>
            <span class="help-block badge badge-danger"><?php echo e($errors->first('wilayah_id')); ?></span>
            <?php endif; ?>
        </div>
        <div class="form-group <?php echo e($errors->has('alamat_perusahaan') ? 'has-error' : ''); ?> m-input">
            <label>Alamat <sup>*</sup></label>

            <?php echo Form::textarea('alamat_perusahaan', null, ['class'=> 'form-control','rows'=>3,'placeholder'=>Auth::user()->pmku->alamat_perusahaan,'disabled']); ?>


            <?php if($errors->has('alamat_perusahaan')): ?>
            <span class="help-block badge badge-danger"><?php echo e($errors->first('alamat_perusahaan')); ?></span>
            <?php endif; ?>
        </div>
        <div class="form-group <?php echo e($errors->has('fax') ? 'has-error' : ''); ?> m-input">
            <label>Fax</label>

            <?php echo Form::text('fax', null, ['class'=> 'form-control','placeholder'=>Auth::user()->pmku->fax,'disabled']); ?>


            <?php if($errors->has('fax')): ?>
            <span class="help-block badge badge-danger"><?php echo e($errors->first('fax')); ?></span>
            <?php endif; ?>
        </div>
        <div class="form-group <?php echo e($errors->has('hotline') ? 'has-error' : ''); ?> m-input">
            <label>Hotline<sup>*</sup></label>

            <?php echo Form::text('hotline', null, ['class'=> 'form-control','placeholder'=>Auth::user()->pmku->hotline,'disabled']); ?>


            <?php if($errors->has('hotline')): ?>
            <span class="help-block badge badge-danger"><?php echo e($errors->first('hotline')); ?></span>
            <?php endif; ?>
        </div>
        <div class="form-group <?php echo e($errors->has('file_ktp') ? 'has-error' : ''); ?> m-input">
            <label>Upload KTP Penanggung Jawab <sup>*</sup></label><br>
            <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modalKTP" data-ktp= "<?php echo e(url(Auth::user() ? Auth::user()->pmku->file_ktp : 'empty.jpg')); ?>"><i class="fa fa-search"></i> Lihat File KTP Penanggung Jawab
            </button>
        </div>
    </div>
    <div class="col-md-6">
            <div class="form-group <?php echo e($errors->has('tempat_kantor') ? 'has-error' : ''); ?> m-input">
                <label>Tempat Kantor / Pemilik Usaha <sup>*</sup></label>
                <?php
                    if(Auth::user()->pmku->tempat_kantor == 1){
                        $kantor = 'Kantor Pusat';
                    }else{
                        $kantor = 'Kantor Cabang';
                    }
                ?>

                <?php echo Form::text('tempat_kantor', null, ['class'=> 'form-control','placeholder'=>$kantor,'disabled']); ?>


                <?php if($errors->has('tempat_kantor')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('tempat_kantor')); ?></span>
                <?php endif; ?>
            </div>
        <div class="form-group <?php echo e($errors->has('telepon') ? 'has-error' : ''); ?> m-input">
            <label>Telepon Kantor <sup>*</sup></label>

            <?php echo Form::text('telepon', null, ['class'=> 'form-control','placeholder'=>Auth::user()->pmku->telepon,'disabled']); ?>


            <?php if($errors->has('telepon')): ?>
            <span class="help-block badge badge-danger"><?php echo e($errors->first('telepon')); ?></span>
            <?php endif; ?>
        </div>
        <div class="form-group <?php echo e($errors->has('email_perusahaan') ? 'has-error' : ''); ?> m-input">
            <label>Email Kantor Perusahaan <sup>*</sup></label>

            <?php echo Form::text('email_perusahaan', Auth::user()->email, ['class'=> 'form-control', 'placeholder' => Auth::user()->email, 'disabled'=>'disabled']); ?>


            <?php if($errors->has('email_perusahaan')): ?>
            <span class="help-block badge badge-danger"><?php echo e($errors->first('email_perusahaan')); ?></span>
            <?php endif; ?>
        </div>
        <div class="form-group <?php echo e($errors->has('penanggung_jawab') ? 'has-error' : ''); ?> m-input">
            <label>Nama Penanggung Jawab <sup>*</sup></label>

            <?php echo Form::text('penanggung_jawab', null, ['class'=> 'form-control','placeholder'=>Auth::user()->pmku->penanggung_jawab,'disabled']); ?>


            <?php if($errors->has('penanggung_jawab')): ?>
            <span class="help-block badge badge-danger"><?php echo e($errors->first('penanggung_jawab')); ?></span>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php echo $__env->make('frontend.pelayanan.modal-ktp', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('frontend.pelayanan.modal-npwp', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('frontend.pelayanan.modal-struktur', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('frontend.pelayanan.modal-siup', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('frontend.pelayanan.modal-akta', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('frontend.pelayanan.modal-domisili', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>