<?php $__env->startSection('pageTitle','Update Profil'); ?>
<?php $__env->startSection('breadcrumbTitle','Update Profil'); ?>
<?php $__env->startSection('breadcrumbParent','Pengaturan / Profil'); ?>

<?php $__env->startSection('content'); ?>
	<div class="content-wrapper">
		<?php echo $__env->make('layouts.backend.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<!-- Main content -->
		    <div class="row">
		      <div class="col-lg-12">
		        <?php echo Form::model($profil, [
		            'method' => 'PUT',
		            'route' => ['admin.profil.update', $profil->id],
		            'id' => 'profil-form',
		            'files'=> TRUE,
		        ]); ?>


		        <?php echo $__env->make('backend.profil.form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

		        <?php echo Form::close(); ?>

		      </div>
		    </div>
		  <!-- ./row -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.profil.script', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>