<?php $__env->startSection('pageTitle','Tambah Foto'); ?>
<?php $__env->startSection('breadcrumbTitle','Tambah Foto'); ?>
<?php $__env->startSection('breadcrumbParent','Pengaturan / Foto'); ?>

<?php $__env->startSection('content'); ?>
	<div class="content-wrapper">
		<?php echo $__env->make('layouts.backend.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<!-- Main content -->
		    <div class="row">
		      <div class="col-lg-12">
		        <?php echo Form::model($galeriFoto, [
					'method' => 'PUT',
					'route'  => ['admin.galeri-foto.update', $galeriFoto->id],
					'id'     => 'galeri-foto-form',
					'files'  => true,
		        ]); ?>


		        <?php echo $__env->make('backend.galeri-foto.form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

		        <?php echo Form::close(); ?>

		      </div>
		    </div>
		  <!-- ./row -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.berita.script', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>