<div class="wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-th"></i> Tambah Foto</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
                <div class="card-body">
                  <div class="form-group <?php echo e($errors->has('title') ? 'has-error' : ''); ?> m-input">
                    <?php echo Form::label('title'); ?>

                    <?php echo Form::text('title', null, ['class'=> 'form-control']); ?>


                    <?php if($errors->has('title')): ?>
                      <span class="help-block"><?php echo e($errors->first('title')); ?></span>
                    <?php endif; ?>
                  </div>

                  <div class="form-group <?php echo e($errors->has('caption') ? 'has-error' : ''); ?> m-input">
                    <?php echo Form::label('caption'); ?>

                    <?php echo Form::text('caption', null, ['class'=> 'form-control']); ?>


                    <?php if($errors->has('caption')): ?>
                      <span class="help-block"><?php echo e($errors->first('caption')); ?></span>
                    <?php endif; ?>
                  </div>

                  <div class="form-group <?php echo e($errors->has('kategori_foto_id') ? 'has-error' : ''); ?>">
                    <?php echo Form::label('Kategori Foto'); ?>


                    <?php echo Form::select('kategori_foto_id', App\Model\KategoriFoto::pluck('title','id'), null, ['class'=> 'js-selectize form-control','placeholder' => 'Pilih Kategori']); ?>


                    <?php if($errors->has('kategori_foto_id')): ?>
                    <span class="help-block text-danger"><?php echo e($errors->first('kategori_foto_id')); ?></span>
                    <?php endif; ?>
                  </div>

                  <div class="form-group <?php echo e($errors->has('namafile') ? 'has-error' : ''); ?> m-input">
                    <?php echo Form::label('namafile', 'Upload Gambar'); ?><br>
                    <?php if($galeriFoto->namafile): ?>
                      <a href="<?php echo e(url($galeriFoto->namafile)); ?>" target="_blank"><img src="<?php echo e(url($galeriFoto->namafile)); ?>" width="50%"></a><br>
                    <?php endif; ?>
                    <?php echo Form::file('namafile'); ?>


                    <?php if($errors->has('namafile')): ?>
                      <span class="help-block"><?php echo e($errors->first('namafile')); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-info"><i class="fa fa-save"></i>  <?php echo e($galeriFoto->exists ? 'Update' : 'Save'); ?></button>
                  <a href="<?php echo e(route('admin.galeri-foto.index')); ?>" class="btn btn-warning"><i class="fa fa-undo"></i> Cancel</a>
                </div>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          <!-- right column -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
