<?php $__env->startSection('pageTitle','Update PPID'); ?>
<?php $__env->startSection('breadcrumbTitle','Update PPID'); ?>
<?php $__env->startSection('breadcrumbParent','Pengaturan / PPID'); ?>

<?php $__env->startSection('content'); ?>
	<div class="content-wrapper">
		<?php echo $__env->make('layouts.backend.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<!-- Main content -->
		    <div class="row">
		      <div class="col-lg-12">
		        <?php echo Form::model($ppid, [
		            'method' => 'PUT',
		            'route' => ['ppid.update', $ppid->id],
		            'id' => 'ppid-form',
		            'files'=> TRUE,
		        ]); ?>


		        <?php echo $__env->make('backend.ppid.form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

		        <?php echo Form::close(); ?>

		      </div>
		    </div>
		  <!-- ./row -->
	</div>
	<?php echo $__env->make('layouts.backend.footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>