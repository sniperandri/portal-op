<?php $__env->startSection('pageTitle', 'Informasi Produk Hukum'); ?>

<?php $__env->startSection('content'); ?>
<section>
    <div class="content-header">
        <img src="<?php echo e(asset('frontend-asset/images/bg-header3.png')); ?>" />
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="desc">
                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>Informasi</span></small>
                        <h2>Produk Hukum Kantor OP Tanjung Priok</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-box">
        <div class="container">
            <div class="row">
                <div class="col">
                    <label class="content-label"><?php echo e($hukum->judul_informasi); ?></label>
                </div>
            </div>
        	<?php echo $__env->make('frontend.message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="row">
                <div class="col-md-12">
					<?php echo htmlspecialchars_decode(stripslashes($hukum->konten)); ?>

                    <br>
                    <?php if($hukum->gambar): ?>
                    <a href="<?php echo e(url($hukum->gambar)); ?>" download> <span class="btn btn-sm btn-dark"><i class="fa fa-file-pdf-o"></i> Download File</span></a>
                    <?php else: ?>
                    -
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>