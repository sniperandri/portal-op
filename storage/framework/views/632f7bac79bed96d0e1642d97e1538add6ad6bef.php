<?php if(isset($categoryName)): ?>
    <div class="alert alert-info">
        <p>Kategori Berita: <strong><?php echo e($categoryName); ?></strong></p>
    </div>
<?php endif; ?>
<?php if($term = request('term')): ?>
  <div class="alert alert-info">
    <p>Hasil Pencarian Berita dengan Kategori: <strong><?php echo e($term); ?></strong></p>
  </div>
<?php endif; ?>