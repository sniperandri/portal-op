<?php $__env->startSection('pageTitle','Tambah Video'); ?>
<?php $__env->startSection('breadcrumbTitle','Tambah Video'); ?>
<?php $__env->startSection('breadcrumbParent','Pengaturan / Video'); ?>

<?php $__env->startSection('content'); ?>
	<div class="content-wrapper">
		<?php echo $__env->make('layouts.backend.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<!-- Main content -->
		    <div class="row">
		      <div class="col-lg-12">
		        <?php echo Form::model($galeriVideo, [
		            'method' 	=> 'PUT',
		            'route' 	=> ['admin.galeri-video.update', $galeriVideo->id],
		            'id' 			=> 'galeri-video-form'
		        ]); ?>


		        <?php echo $__env->make('backend.galeri-video.form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

		        <?php echo Form::close(); ?>

		      </div>
		    </div>
		  <!-- ./row -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.berita.script', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>