<table class="table table-striped">
  <tr>
    <th style="width: 10px">No</th>
    <th>Judul Video</th>
    <th width="400px">Link Video</th>
    <th class="text-center">Aksi</th>
  </tr>
  <?php $no = paging_number($perPage);?>
  <?php $__currentLoopData = $galeriVideos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $galeriVideo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  	<tr>
  	  <td><?php echo e($no); ?>.</td>
  	  <td><?php echo e($galeriVideo->judul_video); ?></td>
  	  <td>
        <iframe width="350" height="180" src="<?php echo e($galeriVideo->link_video); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
      </td>
      <td align="center">
        <?php echo Form::open(['method' => 'DELETE', 'route' => ['admin.galeri-video.destroy', $galeriVideo->id]]); ?>

        <a href="<?php echo e(route('admin.galeri-video.edit', $galeriVideo->id)); ?>" class="btn btn-sm btn-warning btn-block" title="Edit">
          <i class="fa fa-edit text-black"></i> Edit
        </a>
          <button onclick="return confirm('Apakah Anda yakin untuk menghapus video?')" type="submit" class="btn btn-sm btn-danger btn-block" title="Move to Trash">
            <i class="fa fa-trash"></i> Hapus
          </button>
        <?php echo Form::close(); ?>

      </td>
  	</tr>
    <?php $no++;?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>