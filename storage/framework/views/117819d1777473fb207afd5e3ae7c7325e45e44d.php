<?php $__env->startSection('pageTitle','Pelaporan'); ?>
<?php $__env->startSection('content'); ?>
<section>
    <div class="content-header">
        <img src="<?php echo e(asset('frontend-asset/images/bg-header3.png')); ?>" />
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="desc">
                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>Pelaporan</span></small>
                        <h2>Pelaporan Perusahaan</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-box">
        <div class="container">
            <div class="row">
                <div class="col">
                    <label class="content-label">Pelaporan Perusahaan</label>
                </div>
            </div>
        	
            <div class="row">
                <div class="col">
                    <a href="<?php echo e(route('pelaporan.create')); ?>" class="btn btn-info"><span class="fa fa-plus-circle"></span> Tambah Pelaporan</a>
                    
                    <div class="card-body p-1">
                    <div class="row">
                        <div class="col-md-12" style="padding-left: 10px; padding-right: 30px; padding-top: 10px; padding-bottom: 10px; ">
                            <?php echo $__env->make('frontend.pelaporan.message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                        </div>
                    </div>
                    <?php if(! $pelaporans->count()): ?>
                      <div class="alert alert-danger">
                        Data Tidak Ditemukan
                      </div>
                    <?php else: ?>
                        <?php echo $__env->make('frontend.pelaporan.table', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer clearfix">
                    <div class="clearfix">
                        <?php echo e($pelaporans->appends( Request::query() )->render()); ?>

                      </div>
                      <div class="pull-right">
                        <small><?php echo e($pelaporansCount); ?> <?php echo e(str_plural('Report', $pelaporansCount)); ?></small>
                      </div>
                  </div>
                </div>
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>

    <div class="info-box">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6 right">
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>