<?php $__env->startSection('pageTitle','Profil'); ?>
<?php $__env->startSection('breadcrumb','Profil'); ?>

<?php $__env->startSection('content'); ?>
	<div class="content-wrapper">
	<?php echo $__env->make('layouts.backend.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	    <!-- Main content -->
	    <section class="content">
	      <div class="container-fluid">
	        <div class="row">
	          <div class="col-md-12">
	            <div class="card card-info">
	              <!-- /.card-header -->
	              <div class="card-header">
	                <h3 class="card-title">List Profil</h3>
	                <div class="card-tools">
	                  
	                </div>
	              </div>
				  	

	              <!-- /.card-header -->
	              <div class="card-body p-1">
	              	<div class="row">
	              		<div class="col-md-12" style="padding-left: 10px; padding-right: 30px; padding-top: 10px; padding-bottom: 10px; ">
			                <?php echo $__env->make('backend.profil.message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			                <?php if (app('laratrust')->hasRole('superadmin')) : ?>
	              			<a href="<?php echo e(route('admin.profil.create')); ?>" class="btn btn-info float-left">
	              			  <span>
	              			    <i class="fa fa-plus-circle"></i>
	              			    <span>
	              			      Tambah Profil
	              			    </span>
	              			  </span>
	              			</a>
	              			<?php endif; // app('laratrust')->hasRole ?>
	              			<div class="float-right" style="color: blue;">
	              				<?php $links = [];?>
	              				<?php $__currentLoopData = $statusList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	              				    <?php if($value): ?>
	              				      <?php $selected = Request::get('status') == $key ? 'selected-status' : '' ?>
	              				      <?php $links[] = "<a class=\"{$selected}\" href=\"?status={$key}\">" .ucwords($key) ."({$value}) </a>"?>
	              				    <?php endif; ?>
	              				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	              				<?php echo implode(' | ', $links); ?>

	              			</div>
	              		</div>
	              	</div>

	                <?php if(! $profils->count()): ?>
	                  <div class="alert alert-danger">
	                    Data Tidak Ditemukan
	                  </div>
	                <?php else: ?>
	                    <?php if($onlyTrashed): ?>
	                      <?php echo $__env->make('backend.profil.table-trash', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	                    <?php else: ?>
	                      <?php echo $__env->make('backend.profil.table', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	                    <?php endif; ?>
	                <?php endif; ?>
	              </div>
	              <!-- /.card-body -->
	              <div class="card-footer clearfix">
	                <div class="clearfix">
	                    <?php echo e($profils->appends( Request::query() )->render()); ?>

	                  </div>
	                  <div class="pull-right">
	                    <small><?php echo e($profilsCount); ?> <?php echo e(str_plural('Record', $profilsCount)); ?></small>
	                  </div>
	              </div>

	            </div>
	            <!-- /.card -->
	          </div>

	        </div>
	        <!-- /.row -->
	      </div><!-- /.container-fluid -->
	    </section>
	    <!-- /.content -->
	  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>