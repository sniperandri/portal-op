<?php $__env->startSection('pageTitle', 'Produk Hukum'); ?>

<?php $__env->startSection('content'); ?>
<section>
    <div class="content-header">
        <img src="<?php echo e(asset('frontend-asset/images/bg-header3.png')); ?>" />
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="desc">
                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>Informasi</span></small>
                        <h2>Produk Hukum</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-box">
        <div class="container">
            <div class="row">
                <div class="col">
                    <label class="content-label">Produk Hukum Kepala Kantor OP Tanjung Priok</label>
                </div>
            </div>
        	<?php echo $__env->make('frontend.message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="row">
                <div class="col-md-12">
					<table class="table table-striped">
					  <tr>
					    <th style="width: 10px">No</th>
					    <th>Judul</th>
					    <th>Tahun</th>
					    <th>Bulan</th>
					    <th class="text-center" style="width: 200px">Download</th>
					  </tr>
                      <?php $no = paging_number($perPage);?>
                        <?php $__currentLoopData = $kontens; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $konten): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					    <tr>
					      <td><?php echo e($no); ?></td>
					      <td><a href="<?php echo e(url('info').'/informasi-hukum/'.$konten->slug); ?>"><?php echo e($konten->judul_informasi); ?></a></td>
					      <td><?php echo e($konten->year ? $konten->year->tahun :  '_'); ?></td>
                          <td><?php echo e($konten->month ? $konten->month->nama : '-'); ?></td>
					      <td align="center">
                            <?php if($konten->gambar): ?>
					        <a href="<?php echo e(url($konten->gambar)); ?>" class="btn btn-sm btn-block btn-success" download><i class="fa fa-download"></i> Download</a>
                            <?php else: ?>
                            -
                            <?php endif; ?>
					      </td>
				        <?php echo Form::close(); ?>

					    </tr>
                        <?php $no++;?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</table>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>