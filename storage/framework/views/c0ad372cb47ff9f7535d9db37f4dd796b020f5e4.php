<?php $__env->startSection('pageTitle','Tambah Berita'); ?>
<?php $__env->startSection('breadcrumbTitle','Tambah Berita'); ?>
<?php $__env->startSection('breadcrumbParent','Pengaturan / Berita'); ?>

<?php $__env->startSection('content'); ?>
	<div class="content-wrapper">
		<?php echo $__env->make('layouts.backend.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<!-- Main content -->
		    <div class="row">
		      <div class="col-lg-12">
		        <?php echo Form::model($post, [
		            'method' => 'POST',
		            'route' => 'admin.berita.store',
		            'id' => 'berita-form',
		            'files'=> TRUE,

		        ]); ?>


		        <?php echo $__env->make('backend.berita.form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

		        <?php echo Form::close(); ?>

		      </div>
		    </div>
		  <!-- ./row -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.berita.script', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>