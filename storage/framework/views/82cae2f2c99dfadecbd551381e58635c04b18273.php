<table class="table table-striped">
  <tr>
    <th style="width: 10px">No</th>
    <th>Title</th>
    <th width="400px">Caption</th>
    <th class="text-center">Foto</th>
    <th class="text-center">Aksi</th>
  </tr>
  <?php $no = paging_number($perPage);?>
  <?php $__currentLoopData = $galeriFotos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $galeriFoto): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  	<tr>
  	  <td><?php echo e($no); ?>.</td>
  	  <td><?php echo e($galeriFoto->title); ?></td>
  	  <td><?php echo e($galeriFoto->caption); ?></td>
  	  <td align="center"><img src="<?php echo e(url($galeriFoto->namafile)); ?>" width="250px" height="150px"></td>
      <td align="center">
        <?php echo Form::open(['method' => 'DELETE', 'route' => ['admin.galeri-foto.destroy', $galeriFoto->id]]); ?>

        <a href="<?php echo e(route('admin.galeri-foto.edit', $galeriFoto->id)); ?>" class="btn btn-sm btn-warning btn-block" title="Edit">
          <i class="fa fa-edit text-black"></i> Edit
        </a>
          <button onclick="return confirm('Apakah Anda yakin untuk menghapus foto?')" type="submit" class="btn btn-sm btn-danger btn-block" title="Move to Trash">
            <i class="fa fa-trash"></i> Hapus
          </button>
        <?php echo Form::close(); ?>

      </td>
  	</tr>
    <?php $no++;?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>