<?php $__env->startSection('pageTitle', 'Pelayanan Rekomendasi Surat Izin Kerja Keruk'); ?>

<?php $__env->startSection('content'); ?>
	<section>
	    <div class="content-header">
	        <img src="<?php echo e(asset('frontend-asset/images/bg-header3.png')); ?>" />
	        <div class="container">
	            <div class="row">
	                <div class="col">
	                    <div class="desc">
	                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>Pelayanan</span></small>
	                        <h2>Rekomendasi Surat Izin Kerja Keruk</h2>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="content-box">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <label class="content-label">Rekomendasi Surat Izin Kerja Keruk</label>
                        </div>
                    </div>
                    <?php echo $__env->make('frontend.message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    
	            <?php echo Form::model($rekomendasiSikk, [
	                'method' => 'POST',
	                'route' => 'pelayanan.storeSikk',
	                'id' => 'pelayanan-rekomendasi-sikk',
	                'files' => true,
	            ]); ?>

	            <div class="row">
	                <div class="col">
	                    <?php echo $__env->make('frontend.pelayanan.form-sikk', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	                </div>
	            </div>
	            <?php echo e(Form::hidden('jenis_pelayanan', 'SIKK')); ?>

			    <?php echo Form::close(); ?>

                </div>
	    </div>
	</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>