<?php $__env->startSection('pageTitle','Beranda'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.frontend.slider', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.frontend.contact', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<style>
  .flex-container{
    display: flex;
    justify-content: center;
  }
  .desc a{
    text-decoration: none;
    color: #1F2A22;
  }
  .desc a:hover{
    text-decoration: none;
    color: #3C3487;
    font-size: 22px;
  }
  .angka {
      font-size: 32pt;
      color: orange;
  }
  .angkahijau {
      font-size: 32pt;
      color: green;
  }
  .angkamerah {
      font-size: 32pt;
      color: red;
  }
</style>
<div id="splashscreen"><img src="<?php echo e(url($splashscreen->foto)); ?>"></div>
<div class="content-box">
    <div class="container">
        <div class="row">
            <div class="col">
                <label class="content-label"><?php echo $judulgrafik ? $judulgrafik->konten : ''; ?></label>
            </div>
        </div>
        <div class="row">
            <div class="col" align="center">
                <h4><?php echo e($tanggalDt); ?></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="row" style="background-color: rgb(43,36,114); padding: 15px;">
                    <div class="col-lg-9" style="margin: 0.5% 0.5% 0.5% 0.5%; background-color: #FFB900; border:1px solid #rgb(43,36,114); min-width: 23.5%; color: #fff; padding-bottom: 5px; height: 95px; top: 18%; left: 11%;">
                        <div class="col-lg-12" style="background-color: #fff; margin-top: 10px; padding: 5px 5px 5px 5px;">
                            <table class="col-md-12" border="0">
                                <tr>
                                    <td width="75%"><img src="http://localhost/work/portal-op/public/logo-terminal/average.png" width="75%"></td>
                                    <td><b class="<?php echo e($classAvg); ?>"><?php echo e($averageDt); ?></b></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>

        <div class="container">
            <div class="row" style="background-color: rgb(43,36,114); padding: 15px;">
                <div class="col-lg-2" style="margin: 0.5% 0.5% 0.5% 0.5%; background-color: #FFB900; border:1px solid #rgb(43,36,114); min-width: 23.5%; color: #fff; padding-bottom: 5px; height: 95px; top: 18%; left: 1%;">
                    <div class="col-lg-12" style="background-color: #fff; margin-top: 10px; padding: 5px 5px 5px 5px;">
                        <table class="col-md-12" border="0">
                           <tr>
                                <td width="75%"><img src="http://localhost/work/portal-op/public/logo-terminal/jict.png" width="75%"></td>
                                <td><b class="<?php echo e($classJict); ?>"><?php echo e($jict); ?></b></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-lg-2" style="margin: 0.5% 0.5% 0.5% 0.5%; background-color: #FFB900; border:1px solid #rgb(43,36,114); min-width: 23.5%; color: #fff; padding-bottom: 5px; height: 95px; top: 18%; left: 1%;">
                    <div class="col-lg-12" style="background-color: #fff; margin-top: 10px; padding: 5px 5px 5px 5px;">
                        <table class="col-md-12" border="0">
                            <tr>
                                <td width="75%"><img src="http://localhost/work/portal-op/public/logo-terminal/npct1.png" width="75%"></td>
                                <td><b class="<?php echo e($classNpct1); ?>"><?php echo e($npct1); ?></b></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-lg-2" style="margin: 0.5% 0.5% 0.5% 0.5%; background-color: #FFB900; border:1px solid #rgb(43,36,114); min-width: 23.5%; color: #fff; padding-bottom: 5px; height: 95px; top: 18%; left: 1%;">
                    <div class="col-lg-12" style="background-color: #fff; margin-top: 10px; padding: 5px 5px 5px 5px;">
                        <table class="col-md-12" border="0">
                            <tr>
                                <td width="75%"><img src="http://localhost/work/portal-op/public/logo-terminal/koja.png" width="75%"></td>
				<td><b class="angkahijau"><?php echo e($koja); ?></b></td>
</tr>
                        </table>
                    </div>
                </div>
                <div class="col-lg-2" style="margin: 0.5% 0.5% 0.5% 0.5%; background-color: #FFB900; border:1px solid #rgb(43,36,114); min-width: 23.5%; color: #fff; padding-bottom: 5px; height: 95px; top: 18%; left: 1%;">
                    <div class="col-lg-12" style="background-color: #fff; margin-top: 10px; padding: 5px 5px 5px 5px;">
                        <table class="col-md-12" border="0">
                            <tr>
                                <td width="75%"><img src="http://localhost/work/portal-op/public/logo-terminal/tmal.png" width="75%"></td>
                                <td><b class="<?php echo e($classTmal); ?>"><?php echo e($tmal); ?></b></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div><br>
        <div class="row">
            <div class="col-md-12">
                <div id="daily" style="min-width: 400px; height: 400px; margin: 0 auto;"></div>
                <div style="padding-left: 100px;">*) Keterangan: Data Dwelling Time TER3 2019 tidak lengkap. </div><br />
                <div id="monthly" style="min-width: 400px; height: 400px; margin: 0 auto;"></div><br>
            </div>
        </div> 
    </div>
</div>

<div class="content-box bg-grey">
    <div class="container">
        <div class="row">
            <div class="col">
                <label class="content-label"><?php echo $judulkegiatanotoritas ? $judulkegiatanotoritas->konten : ''; ?></label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="function-box">
                    <span><i class="fa fa-edit"></i></span>
                    <div class="desc">
                        <h5><a href="<?php echo e(route('ppid')); ?>">PPID</a></h5>
                        <p>
                            Profil Pejabat Pengelola Informasi dan Dokumentasi (PPID) Kantor Otoritas Pelabuhan Tanjung Priok dan Formulir pengajuan permohonan informasi secara online.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="function-box">
                    <span><i class="fa fa-map-o"></i></span>
                    <div class="desc">
                        <h5><a href="<?php echo e(url('/info/informasi-publik')); ?>">informasi publik</a></h5>
                        <p>
                            Informasi berkala, informasi serta merta dan informasi setiap saat.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="function-box">
                    <span><i class="fa fa-anchor"></i></span>
                    <div class="desc">
                        <h5><a href="<?php echo e(url('/info/program-dan-kegiatan')); ?>">Program Dan Kegiatan</a></h5>
                        <p>
                            Program-progam dan kegiatan yang tertuang dalam Renstra maupun kegiatan lainnya.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="function-box">
                    <span><i class="fa fa-info-circle"></i></span>
                    <div class="desc">
                    <h5><a href="<?php echo e(url('/info/data-dan-informasi')); ?>">Data Dan Informasi</a></h5>
                        <p>
                            Data dan informasi seperti data penumpang maupun sarana dan prasarana, LHKPN dll...
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="function-box">
                    <span><i class="fa fa-line-chart"></i></span>
                    <div class="desc">
                    <h5><a href="<?php echo e(url('/info/kinerja-kantor-otoritas-pelabuhan')); ?>">Kinerja Kantor Otoritas Pelabuhan Tanjung Priok</a></h5>
                        <p>
                            Pertanggungjawaban pelaksanaan kebijakan, pengukuran kinerja, LAKIP dll...
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="function-box">
                    <span><i class="fa fa-balance-scale"></i></span>
                    <div class="desc">
                        <h5><a href="#">Informasi Hukum</a></h5>
                        <p>
                        <a href="http://jdih.dephub.go.id" target="_blank">JDIH Kementerian Perhubungan</a><br>
                        <a href="<?php echo e(url('/info/informasi-hukum')); ?>">Produk Hukum Kepala Kantor OP Tanjung Priok</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="function-box">
                    <span><i class="fa fa-credit-card"></i></span>
                    <div class="desc">
                        <h5><a href="<?php echo e(url('/info/tarif-pnbp')); ?>">Tarif PNBP</a></h5>
                        <p>
                        Informasi Tarif Penerimaan Negara Bukan Pajak (PNBP)
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="function-box">
                    <span><i class="fa fa-smile-o"></i></span>
                    <div class="desc">
                        <h5><a href="<?php echo e(url('/info/indeks-kepuasan-masyarakat')); ?>">Indeks Kepuasan Masyarakat</a></h5>
                        <p>
                        Informasi Indeks Kepuasan Masyarakat terhadap Kantor Otoritas Pelabuhan Tanjung Priok
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="function-box">
                    <span><i class="fa fa-building-o"></i></span>
                    <div class="desc">
                        <h5><a href="<?php echo e(url('/info/reformasi-birokrasi')); ?>">Reformasi Birokrasi</a></h5>
                        <p>
                        Informasi Reformasi Birokrasi pada Lingkungan Kantor Otoritas Pelabuhan Tanjung Priok.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-box">
    <div class="container">
        <div class="row">
            <div class="col">
                <label class="content-label"><?php echo $berita ? $berita->konten : ''; ?></label>
            </div>
        </div>
        <div class="row">
            <?php $__currentLoopData = $beritas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $berita): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-4">
                <div class="news-box">
                    <div class="img-box">
                        <img src="<?php echo e(url($berita->image)); ?>" width="100%" />
                    </div>
                    <div class="desc">
                        <h5><a href="<?php echo e(route('berita.show', $berita->slug)); ?>"><?php echo e($berita->title); ?></a></h5>
                        <small><i class="fa fa-clock-o"></i> <?php echo e(date('d M Y H:i:s',strtotime($berita->created_at))); ?></small>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>
<div class="content-box bg-grey">
    <div class="container">
        <div class="row">
            <div class="col">
                <label class="content-label"><?php echo $judulgalerifoto ? $judulgalerifoto->konten : ''; ?></label>
            </div>
        </div>
        <div class="row">
            <?php $__currentLoopData = $gallerys; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-4 gambar">
                    <div class="news-box" data-kategori="<?php echo e($gallery->kategoriFoto->slug); ?>">
                        <div class="img-box">
                            <a class="gallery-image-link" href="<?php echo e(url($gallery->namafile)); ?>" data-fancybox="gallery-set" data-caption="<?php echo e($gallery->caption); ?>"><img src="<?php echo e(url($gallery->namafile)); ?>" width="350px" height="180px" /></a>
                        </div>
                        <div class="desc">
                            <h5><a href="#"><?php echo e($gallery->title); ?></a></h5>
                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>
<div class="content-box">
    <div class="container">
        <div class="row">
            <div class="col">
                <label class="content-label"><?php echo $judulgalerivideo ? $judulgalerivideo->konten : ''; ?></label>
            </div>
        </div>
        <div class="row">
            <?php $__currentLoopData = $galleryVideos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $galleryVideo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-4">
                <div class="news-box">
                    <div class="img-box">
                        <iframe width="350" height="180" src="<?php echo e($galleryVideo->link_video); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="desc">
                        <h5><a href="<?php echo e($galleryVideo->link_video); ?>" target="_blank"><?php echo e($galleryVideo->judul_video); ?></a></h5>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>
<div class="content-box bg-grey">
    <div class="container">
        <div class="row">
            <div class="col">
                <label class="content-label">Social Media Feed</label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?php $__currentLoopData = $instagrams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $instagram): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php echo htmlspecialchars_decode(stripslashes($instagram->konten)); ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <div class="col-md-6">
                <?php $__currentLoopData = $facebooks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $facebook): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php echo htmlspecialchars_decode(stripslashes($facebook->konten)); ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
</div>
<div class="content-box">
    <div class="container">
        <div class="row">
            <div class="col">
                <label class="content-label"><?php echo $judullinkterkait ? $judullinkterkait->konten : ''; ?></label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="function-box flex-container">
                    <?php $__currentLoopData = $linkTerkaits; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $linkTerkait): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <span style="margin-right: 50px"><a href="<?php echo e($linkTerkait->nama_instansi); ?>" target="_blank" title="<?php echo e($linkTerkait->deskripsi); ?>"><img src="<?php echo e(url($linkTerkait->logo_instansi)); ?>" height="77px"></a></span>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script src="http://code.highcharts.com/highcharts.js" type="text/javascript"></script>
<script src="http://code.highcharts.com/modules/exporting.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).on('ready', function() {
    // fetch('http://oppriok.dephub.go.id/api/dt')
    fetch('http://localhost/work/portal-op/public/api/dt')
        .then((response) => {
            return response.json();
        })
        .then((dt) => {
            $('#daily').highcharts({
                title: {
                    text: 'Dwelling Time 2019',
                    x: -20 //center
                },
                subtitle: {
                    text: 'Data Dwelling Time Per Hari Tahun 2019',
                    x: -20
                },
                xAxis: {
                    categories: dt['labels']
                },
                yAxis: {
                    title: {
                        text: 'Dwelling Time (hari)'
                    },
                    plotLines: [{
                        value: 3,
                        width: 5,
                        color: 'yellow'
                    }]
                },
                tooltip: {
                    valueSuffix: ''
                },
                legend: {
                    layout: 'vertical',
                    align: 'left',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                
                series: [{
                    name: 'JICT',
                    lineWidth: 5,
                    data: dt['jict']
                }, {
                    name: 'NPCT1',
                    lineWidth: 5,
                    data: dt['npct1']
                }, {
                    name: 'KOJA',
                    lineWidth: 5,
                    data: dt['koja']
                }, {
                    name: 'TMAL',
                    lineWidth: 5,
                    data: dt['tmal']
                }]
            });
        })
    $('#monthly').highcharts({
        title: {
            text: 'Dwelling Time 2018',
            x: -20 //center
        },
        subtitle: {
            text: 'Data Dwelling Time Per Bulan 2018',
            x: -20
        },
        xAxis: {
            categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni',
                'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
        },
        yAxis: {
            title: {
                text: 'Dwelling Time (hari)'
            },
            plotLines: [{
                value: 3,
                width: 5,
                color: 'yellow'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        
        series: [{
            name: 'JICT',
            lineWidth: 5,
            data: [0, 5.18, 3.31, 3.38, 4.42, 5.85, 4.25, 4.59, 4.05, 5.04, 3.32, 3.26]
        }, {
            name: 'NPCT1',
            lineWidth: 5,
            data: [0.79, 2.18, 3.71, 3.20, 0, 0, 2.95, 3.26, 3.51, 2.97, 3.67, 3.05]
        }, {
            name: 'KOJA',
            lineWidth: 5,
            data: [0, 4.77, 2.56, 3.27, 3.47, 5.11, 4.83, 3.61, 3.67, 5.10, 3.04, 3.14]
        },  {
            name: 'TMAL',
            lineWidth: 5,
            data: [0, 0, 2.95, 3.64, 4.17, 5.56, 3.60, 3.03, 3.31, 2.65, 2.57, 2.79]
        }, {
            name: 'TER3',
            lineWidth: 5,
            data: [0, 3.75, 5.77, 3.99, 0.89, 3.59, 3.26, 3.37, 1.97, 2.65, 2.28, 4.16]
        },]
    });
  });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>