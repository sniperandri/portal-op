<?php $__env->startSection('title','Edit Tampilan Depan'); ?>
<?php $__env->startSection('breadcrumb','Edit Tampilan Depan'); ?>
<?php $__env->startSection('content'); ?>
        <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <?php echo $__env->make('layouts.backend.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo e(url('/admin/tampilan-depan')); ?>">Pengaturan Tampilan</a></li>
                            <li class="breadcrumb-item"> Tampilan Depan</li>
                            <li class="breadcrumb-item active"> Edit Tampilan Depan</li>
                        </ol>
                        <div class="card border-primary mb-3 card-info">
                        <div class="card-header">Edit Tampilan Depan</div>
                            <div class="card-body">
                                <?php echo Form::model($tampilanDepan,[
                                    'url'       => route('admin.tampilan-depan.update', $tampilanDepan->id),
                                    'method'    => 'put',
                                    'class'     => 'form-horizontal',
                                    'files'     => true, 
                                ]); ?>

                                
                                <?php echo $__env->make('backend.tampilan-depan._form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                </div>
            </div>
    
          </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      <?php echo $__env->make('backend.tampilan-depan.script', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>