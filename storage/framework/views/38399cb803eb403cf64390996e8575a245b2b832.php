<?php $__env->startSection('pageTitle', 'Pelayanan Rekomendasi TPS'); ?>

<?php $__env->startSection('content'); ?>
	<section>
	    <div class="content-header">
	        <img src="<?php echo e(asset('frontend-asset/images/bg-header3.png')); ?>" />
	        <div class="container">
	            <div class="row">
	                <div class="col">
	                    <div class="desc">
	                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>Pelayanan</span></small>
	                        <h2>Rekomendasi TPS</h2>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="content-box">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <label class="content-label">Rekomendasi TPS</label>
                        </div>
                    </div>
                    <?php echo $__env->make('frontend.message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    
	            <?php echo Form::model($rekomendasiTps, [
	                'method' => 'POST',
	                'route' => 'pelayanan.store',
	                'id' => 'pelayanan-rekomendasi-siup-pbm-form',
	                'files' => true,
	            ]); ?>

	            <div class="row">
	                <div class="col">

	                    <div class="contact-form">
                                <label class="contact-label"><span>PMKU Non Inaportnet - Pelayanan Rekomendasi TPS</span></label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group <?php echo e($errors->has('konten') ? 'has-error' : ''); ?> m-input">
                                                    <?php echo Form::label('Keterangan'); ?>

                                                    <?php echo Form::textarea('konten', null, ['class'=> 'form-control','id'=>'konten']); ?>

                                
                                                    <?php if($errors->has('konten')): ?>
                                                    <span class="help-block text-danger"><?php echo e($errors->first('konten')); ?></span>
                                                    <?php endif; ?>
                                                  </div>
                                        </div>
                                        <div class="col-md-6">
                                        	<div class="form-group <?php echo e($errors->has('gambar') ? 'has-error' : ''); ?> m-input">
                                                <label>Upload Dokumen Bukti Penguasaan Lahan <sup>*</sup></label>
                                                <input type="file" class="form-control-file" name="gambar">
                                                <?php if($errors->has('gambar')): ?>
                                                <span class="help-block badge badge-danger"><?php echo e($errors->first('gambar')); ?></span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
									</div>
									<?php echo e(Form::hidden('jenis_pelayanan', 'RTPS')); ?>

									<small><sup>*</sup> <i>Tidak boleh kosong</i></small>
									<div class="btn-box">
										<button type="submit" class="btn btn-blue"><span>Send Form</span></button>
										<a href="<?php echo e(url('/')); ?>" class="btn btn-warning"> Cancel</a>
									</div>
                                    <?php echo Form::close(); ?>

                                    <?php echo $__env->make('frontend.pelayanan.data-perusahaan', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	                    </div>
	                </div>
	            </div>
			    
                </div>
	    </div>
	</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>