<?php if($konten->konten): ?>
	<?php echo htmlspecialchars_decode(stripslashes($konten->konten)); ?>

<?php endif; ?>

<?php
    $path = $konten->gambar;
    $ext = pathinfo($path, PATHINFO_EXTENSION);
?>

<?php if($konten->gambar): ?>
	<?php if($ext == 'pdf'): ?>
		<object type="application/pdf" data="<?php echo e(url($path)); ?>" width="100%" height="500" style="height: 85vh;" class="domisili">No Support</object>
	<?php else: ?> 
        <a href="<?php echo e(url($path)); ?>" target="_blank"><img src="<?php echo e(url($path)); ?>" width="100%"></a>
    <?php endif; ?>
<?php endif; ?>
