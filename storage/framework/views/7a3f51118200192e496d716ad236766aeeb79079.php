<?php $__env->startSection('pageTitle','List PPID'); ?>
<?php $__env->startSection('breadcrumbTitle','PPID'); ?>
<?php $__env->startSection('breadcrumbParent','Pengaturan'); ?>

<?php $__env->startSection('content'); ?>
	<div class="content-wrapper">
	<?php echo $__env->make('layouts.backend.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	    <!-- Main content -->
	    <section class="content">
	      <div class="container-fluid">
	        <div class="row">
	          <div class="col-md-12">
	            <div class="card card-info">
	              <!-- /.card-header --> 
	              <div class="card-header">
	                <h3 class="card-title">List PPID</h3>
	                <div class="card-tools">
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
										</button>
									</div>
	              </div>

	              <!-- /.card-header -->
	              <div class="card-body p-1">
	              	<div class="row">
	              		<div class="col-md-12" style="padding-left: 10px; padding-right: 30px; padding-top: 10px; padding-bottom: 10px; ">
			                <?php echo $__env->make('backend.ppid.message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	              		</div>
	              	</div>

	                <?php if(! $ppids->count()): ?>
	                  <div class="alert alert-danger">
	                    Data Tidak Ditemukan
	                  </div>
	                <?php else: ?>
	                    <?php if($onlyTrashed): ?>
	                      <?php echo $__env->make('backend.ppid.table-trash', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	                    <?php else: ?>
	                      <?php echo $__env->make('backend.ppid.table', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	                    <?php endif; ?>
	                <?php endif; ?>
	              </div>
	              <!-- /.card-body -->
	              <div class="card-footer clearfix">
	                <div class="clearfix">
	                    <?php echo e($ppids->appends( Request::query() )->render()); ?>

	                  </div>
	                  <div class="pull-right">
	                    <small><?php echo e($ppidsCount); ?> <?php echo e(str_plural('Record', $ppidsCount)); ?></small>
	                  </div>
	              </div>

	            </div>
	            <!-- /.card -->
	          </div>

	        </div>
	        <!-- /.row -->
	      </div><!-- /.container-fluid -->
	    </section>
	    <!-- /.content -->
	  </div>
	  <?php echo $__env->make('layouts.backend.footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>