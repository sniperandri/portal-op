<!DOCTYPE html>
<html lang="en">

<head>
    <?php echo $__env->make('layouts.frontend.css', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>

<body>
    <a class="whistleblower" href="<?php echo e(route('pengaduan.create')); ?>" >WHISTLEBLOWER</a>
    <?php echo $__env->make('layouts.frontend.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <section>
        <?php echo $__env->yieldContent('content'); ?>
    </section>
    <?php echo $__env->make('layouts.frontend.footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.frontend.script', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('script'); ?>
</body>

</html>