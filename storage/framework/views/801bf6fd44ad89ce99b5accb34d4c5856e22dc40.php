<?php $__env->startSection('pageTitle', 'PPID'); ?>

<?php $__env->startSection('content'); ?>
	<section>
        <div class="content-header">
            <img src="<?php echo e(asset('frontend-asset/images/bg-header3.png')); ?>" />
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="desc">
                            <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>PPID</span></small>
                            <h2>Layanan Penyediaan Informasi</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-box">
            <div class="container">
                <div class="row">
                    <?php echo $__env->make('frontend.ppid.form-ppid', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
        <div class="info-box">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        
                    </div>
                    <div class="col-md-6 right">
                        
                    </div>
                </div>
            </div>
        </div>
	</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>