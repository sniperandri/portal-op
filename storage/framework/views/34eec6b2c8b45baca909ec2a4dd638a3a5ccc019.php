<div class="form-group<?php echo e($errors->has('kode_tampilan') ? ' is-invalid' : ''); ?>">
<?php echo Form::label('Kode Tampilan Depan'); ?> 
<?php
    if($errors->has('kode_tampilan')){
        $invalid = 'form-control is-invalid';
    }else{
        $invalid = 'form-control';
    }
?>
<?php echo Form::text('kode_tampilan', null, ['class'=>$invalid, Auth::user()->name == "superadmin" ? '' : 'disabled']); ?>

<?php echo $errors->first('kode_tampilan', '<p class="invalid-feedback">:message</p>'); ?>

</div>

<div class="form-group<?php echo e($errors->has('konten') ? ' is-invalid' : ''); ?>">
<?php echo Form::label('Konten'); ?> 
<?php
    if($errors->has('konten')){
        $invalid = 'form-control is-invalid';
    }else{
        $invalid = 'form-control';
    }
?>
<?php echo Form::textarea('konten', null, ['class'=>$invalid, 'placeholder' => '', 'rows' => 6,'id'=>'konten']); ?>

<?php echo $errors->first('konten', '<p class="invalid-feedback">:message</p>'); ?>

</div>

<div class="form-group <?php echo e($errors->has('foto') ? 'has-error' : ''); ?> m-input">
    <br>
        <?php echo Form::label('Foto'); ?> &nbsp;
        <?php if(!$tampilanDepan->foto): ?>
        <?php else: ?>
            <img src="<?php echo e(url($tampilanDepan->foto)); ?>" width="50%"><br><br>
        <?php endif; ?>
            <?php echo Form::file('foto', null, ['class'=> 'form-control']); ?>


    <?php if($errors->has('foto')): ?>
            <span class="help-block"><?php echo e($errors->first('foto')); ?></span>
    <?php endif; ?>
</div>

<?php echo Form::submit('Simpan', ['class'=>'btn btn-info']); ?>