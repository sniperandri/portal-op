<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="<?php echo e(asset('frontend-asset/js/jquery.min.js')); ?>"></script>
    <script>
    window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')
    </script>
    <script src="<?php echo e(asset('frontend-asset/js/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend-asset/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend-asset/js/bootstrap-filestyle.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend-asset/js/bootstrap-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend-asset/js/slick.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('frontend-asset/js/jquery.fancybox.min.js')); ?>"></script>
    <script>
        $(document).click(function() { $('#splashscreen').fadeOut(500).remove(); });
    </script>
    <script type="text/javascript" src="<?php echo e(asset('frontend-asset/js/script.js')); ?>"></script>
    <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5c185d05423bba0012ec3b1c&product=inline-share-buttons"></script>
    <script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'id'}, 'google_translate_element');
    }
    </script>

    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>