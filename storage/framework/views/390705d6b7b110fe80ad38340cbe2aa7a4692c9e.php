<?php $__env->startSection('pageTitle', 'Pelayanan Fumigasi'); ?>

<?php $__env->startSection('content'); ?>
	<section>
	    <div class="content-header">
	        <img src="<?php echo e(asset('frontend-asset/images/bg-header3.png')); ?>" />
	        <div class="container">
	            <div class="row">
	                <div class="col">
	                    <div class="desc">
	                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>Pelayanan</span></small>
	                        <h2>Fumigasi</h2>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="content-box">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <label class="content-label">Fumigasi</label>
                        </div>
                    </div>
                    <?php echo $__env->make('frontend.message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    
	            <?php echo Form::model($fumigasi, [
	                'method' => 'POST',
	                'route' => 'pelayanan.store',
	                'id' => 'pelayanan-fumigasi-form',
	                'files' => true,
	            ]); ?>

	            <div class="row">
	                <div class="col">

	                    <div class="contact-form">
                                <label class="contact-label"><span>PMKU Non Inaportnet - Pelayanan Fumigasi</span></label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group <?php echo e($errors->has('konten') ? 'has-error' : ''); ?> m-input">
                                                    <?php echo Form::label('Keterangan'); ?>

                                                    <?php echo Form::textarea('konten', null, ['class'=> 'form-control','id'=>'konten']); ?>

                                
                                                    <?php if($errors->has('konten')): ?>
                                                    <span class="help-block text-danger"><?php echo e($errors->first('konten')); ?></span>
                                                    <?php endif; ?>
                                                  </div>
                    
                                        </div>
									</div>
									<?php echo e(Form::hidden('jenis_pelayanan', 'FMGS')); ?>

									<small><sup>*</sup> <i>Tidak boleh kosong</i></small>
									<div class="btn-box">
										<button type="submit" class="btn btn-blue"><span>Send Form</span></button>
										<a href="<?php echo e(url('/')); ?>" class="btn btn-warning"> Cancel</a>
									</div>
                                    <?php echo Form::close(); ?>

                                    <?php echo $__env->make('frontend.pelayanan.data-perusahaan', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	                    </div>
	                </div>
	            </div>
			    
                </div>
	    </div>
	</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>