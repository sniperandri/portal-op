<p class="breadcrumb-list">
    <span><a href="#">Portal OP</a></span>
    <span><a href="#">Profil</a></span>
    <span>Sejarah Singkat Otoritas Pelabuhan</span>
</p>
<div class="news-desc">
    <article>
        <div class="news-header">
            <h2>Sejarah <small>Sejarah Singkat Kantor Otoritas Pelabuhan Utama</small></h2>
        </div>
        <div class="news-img">
            <?php if(!empty($sejarah->gambar1)): ?>
            <img src="<?php echo e(url($sejarah->gambar1)); ?>" width="100%" /><br>
            <?php endif; ?>
            <?php if(!empty($sejarah->gambar2)): ?>
            <img src="<?php echo e(url($sejarah->gambar1)); ?>" width="100%" /><br>
            <?php endif; ?>
            <?php if(!empty($sejarah->gambar3)): ?>
            <img src="<?php echo e(url($sejarah->gambar1)); ?>" width="100%" /><br>
            <?php endif; ?>
            <?php if(!empty($sejarah->gambar4)): ?>
            <img src="<?php echo e(url($sejarah->gambar1)); ?>" width="100%" /><br>
            <?php endif; ?>
            <?php if(!empty($sejarah->gambar5)): ?>
            <img src="<?php echo e(url($sejarah->gambar1)); ?>" width="100%" /><br>
            <?php endif; ?>
        </div>
        <?php echo htmlspecialchars_decode(stripslashes($sejarah->konten)); ?>

    </article>
</div>