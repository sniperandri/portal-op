<?php $__env->startSection('pageTitle', 'Fasilitas Pelabuhan'); ?>

<?php $__env->startSection('content'); ?>
    <section>
        <div class="content-header">
            <img src="<?php echo e(asset('frontend-asset/images/bg-header3.png')); ?>" />
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="desc">
                            <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>Fasilitas Pelabuhan</span></small>
                            <h2>Fasilitas Pelabuhan Tanjung Priok</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                
            <div class="content-box">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="content-label" id="label-fasilitas">Batas DLKr/DLKp Pelabuhan Tanjung Priok dan Pelabuhan Pasar Ikan</label>
                        </div>
                        
                        <div class="col-md-12" style="padding-right:50px" id="fasilitas">
                            
                            <?php if($konten->konten): ?>
                                <?php echo htmlspecialchars_decode(stripslashes($konten->konten)); ?>

                            <?php endif; ?>

                            <!-- <div>
                                 <p>Download File di sini!  <a href="#pdf" type="button" class="btn btn-info">Download</a></p>
                            </div> -->

                            <?php
                                $path = $konten->gambar;
                                $ext = pathinfo($path, PATHINFO_EXTENSION);
                            ?>

                            <?php if($konten->gambar): ?>
                                <?php if($ext == 'pdf'): ?>
                                    <object type="application/pdf" data="<?php echo e(url($path)); ?>" width="100%" height="500" style="height: 85vh;" class="domisili">No Support</object>
                                <?php else: ?> 
                                    <a href="<?php echo e(url($path)); ?>" target="_blank"><img src="<?php echo e(url($path)); ?>" width="100%"></a>
                                <?php endif; ?>
                            <?php endif; ?>

                        </div>
                        
                    </div>
                </div>
            </div>

            </div>
        </div>
        <div class="info-box">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        
                    </div>
                    <div class="col-md-6 right">
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>