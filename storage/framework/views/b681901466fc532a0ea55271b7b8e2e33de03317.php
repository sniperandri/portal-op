<?php $__env->startSection('pageTitle', 'Pelayanan Rekomendasi Pembukaan Kantor Cabang Perusahaan Pelayaran'); ?>

<?php $__env->startSection('content'); ?>
	<section>
	    <div class="content-header">
	        <img src="<?php echo e(asset('frontend-asset/images/bg-header3.png')); ?>" />
	        <div class="container">
	            <div class="row">
	                <div class="col">
	                    <div class="desc">
	                        <small class="breadcrumb-list"><span><a href="#">Portal OP</a></span><span>Pelayanan</span></small>
	                        <h2>Rekomendasi Pembukaan Kantor Cabang Perusahaan Pelayaran</h2>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="content-box">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <label class="content-label">Rekomendasi Pembukaan Kantor Cabang Perusahaan Pelayaran</label>
                        </div>
                    </div>
                    <?php echo $__env->make('frontend.message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    
	            <?php echo Form::model($rekomendasiCabangAp, [
	                'method' => 'POST',
	                'route' => 'pelayanan.store',
	                'id' => 'pelayanan-rekomendasi-cabang-ap-form',
	                'files' => true,
	            ]); ?>

	            <div class="row">
	                <div class="col">

	                    <div class="contact-form">
                                <label class="contact-label"><span>PMKU Non Inaportnet - Pelayanan Rekomendasi Pembukaan Kantor Cabang Perusahaan Pelayaran</span></label>
                                    <div class="row">
                                    	<div class="col-md-6">
                                        	<div class="form-group <?php echo e($errors->has('file_pengangkatan') ? 'has-error' : ''); ?> m-input">
                                        	    <label>Upload Dokumen File Pengangkatan Kepala Cabang <sup>*</sup></label>
                                        	    <input type="file" class="form-control-file" name="file_pengangkatan">
                                        	    <?php if($errors->has('file_pengangkatan')): ?>
                                        	    <span class="help-block badge badge-danger"><?php echo e($errors->first('file_pengangkatan')); ?></span>
                                        	    <?php endif; ?>
                                        	</div>
                                        	<div class="form-group <?php echo e($errors->has('file_npwp_cabang') ? 'has-error' : ''); ?> m-input">
                                        	    <label>Upload Dokumen NPWP Kantor Cabang <sup>*</sup></label>
                                        	    <input type="file" class="form-control-file" name="file_npwp_cabang">
                                        	    <?php if($errors->has('file_npwp_cabang')): ?>
                                        	    <span class="help-block badge badge-danger"><?php echo e($errors->first('file_npwp_cabang')); ?></span>
                                        	    <?php endif; ?>
                                        	</div>
                                        	<div class="form-group <?php echo e($errors->has('file_ktp_cabang') ? 'has-error' : ''); ?> m-input">
                                        	    <label>Upload KTP Kepala Cabang <sup>*</sup></label>
                                        	    <input type="file" class="form-control-file" name="file_ktp_cabang">
                                        	    <?php if($errors->has('file_ktp_cabang')): ?>
                                        	    <span class="help-block badge badge-danger"><?php echo e($errors->first('file_ktp_cabang')); ?></span>
                                        	    <?php endif; ?>
                                        	</div>
                                        	<div class="form-group <?php echo e($errors->has('file_siupalkk') ? 'has-error' : ''); ?> m-input">
                                        	    <label>Upload Dokumen SIUPAL/SIUPKK <sup>*</sup></label>
                                        	    <input type="file" class="form-control-file" name="file_siupalkk">
                                        	    <?php if($errors->has('file_siupalkk')): ?>
                                        	    <span class="help-block badge badge-danger"><?php echo e($errors->first('file_siupalkk')); ?></span>
                                        	    <?php endif; ?>
                                        	</div>
                                        	<div class="form-group <?php echo e($errors->has('voyage_report') ? 'has-error' : ''); ?> m-input">
                                        	    <label>Upload Voyage Report <sup>*</sup></label>
                                        	    <input type="file" class="form-control-file" name="voyage_report">
                                        	    <?php if($errors->has('voyage_report')): ?>
                                        	    <span class="help-block badge badge-danger"><?php echo e($errors->first('voyage_report')); ?></span>
                                        	    <?php endif; ?>
                                        	</div>
                                    	</div>
                                        <div class="col-md-12">
										<div class="form-group <?php echo e($errors->has('domisili') ? 'has-error' : ''); ?> m-input">
                                                <?php echo Form::label('Domisili Kantor Cabang'); ?>

                                                <?php echo Form::textarea('domisili', null, ['class'=> 'form-control','id'=>'domisili']); ?>

                            
                                                <?php if($errors->has('domisili')): ?>
                                                <span class="help-block text-danger"><?php echo e($errors->first('domisili')); ?></span>
                                                <?php endif; ?>
                                              </div>

                                        <div class="form-group <?php echo e($errors->has('konten') ? 'has-error' : ''); ?> m-input">
                                                <?php echo Form::label('Keterangan (Opsional)'); ?>

                                                <?php echo Form::textarea('konten', null, ['class'=> 'form-control','id'=>'konten']); ?>

                            
                                                <?php if($errors->has('konten')): ?>
                                                <span class="help-block text-danger"><?php echo e($errors->first('konten')); ?></span>
                                                <?php endif; ?>
                                              </div>
                                        </div>
									</div>
									<?php echo e(Form::hidden('jenis_pelayanan', 'RCAP')); ?>

									<small><sup>*</sup> <i>Tidak boleh kosong</i></small>
									<div class="btn-box">
										<button type="submit" class="btn btn-blue"><span>Send Form</span></button>
										<a href="<?php echo e(url('/')); ?>" class="btn btn-warning"> Cancel</a>
									</div>
                                    <?php echo Form::close(); ?>

                                    <?php echo $__env->make('frontend.pelayanan.data-perusahaan', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	                    </div>
	                </div>
	            </div>
			    
                </div>
	    </div>
	</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>