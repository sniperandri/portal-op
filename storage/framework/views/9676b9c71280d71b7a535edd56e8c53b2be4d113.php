<div class="wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-9">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-th"></i> Tambah Berita</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
                <div class="card-body">
                  <div class="form-group <?php echo e($errors->has('title') ? 'has-error' : ''); ?> m-input">
                    <?php echo Form::label('Judul Berita'); ?>

                    <?php echo Form::text('title', null, ['class'=> 'form-control']); ?>


                    <?php if($errors->has('title')): ?>
                      <span class="badge badge-danger"><?php echo e($errors->first('title')); ?></span>
                    <?php endif; ?>
                  </div>

                  <div class="form-group <?php echo e($errors->has('body') ? 'has-error' : ''); ?> m-input">
                    <?php echo Form::label('body'); ?>

                    <?php echo Form::textarea('body', null, ['class'=> 'form-control','id'=>'konten']); ?>


                    <?php if($errors->has('body')): ?>
                    <span class="help-block text-danger"><?php echo e($errors->first('body')); ?></span>
                    <?php endif; ?>
                  </div>

                </div>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          <div class="col-md-3">
              <div class="card card-info">
                <div class="card-header">
                  <h3 class="card-title"> Kategori</h3>
                </div>
                <div class="card-body">
                  <div class="form-group <?php echo e($errors->has('category_id') ? 'has-error' : ''); ?>">
                    <?php echo Form::select('category_id', App\Model\KategoriBerita::pluck('title','id'), null, ['class'=> 'js-selectize form-control','placeholder' => 'Pilih Kategori']); ?>


                    <?php if($errors->has('category_id')): ?>
                    <span class="help-block text-danger"><?php echo e($errors->first('category_id')); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
              <div class="card card-info">
                <div class="card-header">
                  <h3 class="card-title"> Tags</h3>
                </div>
                <div class="card-body">
                  <div class="form-group">
                      <?php echo Form::text('post_tags[]', null, ['class' => 'form-control input-tags']); ?>

                  </div>
                </div>
              </div>
              <div class="card card-info">
                <div class="card-header">
                  <h3 class="card-title"> Foto Berita</h3>
                </div>
                <div class="card-body">
                  <?php if(!$post->image): ?>

                  <?php else: ?>
                    <img src="<?php echo e(url($post->image)); ?>" width="100%"><br><br>
                  <?php endif; ?>
                  <div class="form-group <?php echo e($errors->has('image') ? 'has-error' : ''); ?>">
                    <?php echo Form::file('image'); ?>


                    <?php if($errors->has('image')): ?>
                    <span class="help-block text-danger"><?php echo e($errors->first('image')); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
              <!-- /.card -->
              <div class="card card-info">
                <div class="card-header">
                  <h3 class="card-title"> Publikasikan</h3>
                </div>
                <!-- /.card-header -->
                  <!-- /.card-body -->

                  <div class="card-footer">
                      <div align="center">
                        <?php if($post->exists): ?>
                        <?php echo Form::submit('Update',['class' => 'btn btn-info']); ?>

                        <?php else: ?>
                        <?php echo Form::submit('Publish',['class' => 'btn btn-info']); ?>

                        <?php endif; ?>
                      </div>
                  </div>
              </div>
          </div>
          <!-- right column -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
