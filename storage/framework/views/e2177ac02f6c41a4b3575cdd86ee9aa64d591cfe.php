<div class="contact-form">
    <label class="contact-label"><span>Register Username dan Password Portal OP Tanjung Priok</span></label>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group <?php echo e($errors->has('kode_perusahaan') ? 'has-error' : ''); ?> m-input">
                <?php $placeholderKodePerusahaan = $perusahaan->data? $perusahaan->data->ina_perusahaan->kode_perusahaan : '';?>
                <?php echo Form::hidden('kode_perusahaan', $placeholderKodePerusahaan, ['class'=> 'form-control','placeholder'=>$placeholderKodePerusahaan]); ?>


                <?php if($errors->has('kode_perusahaan')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('kode_perusahaan')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('no_pmku') ? 'has-error' : ''); ?> m-input">
                <?php $placeholderNoPmku = $perusahaan->data? $perusahaan->data->ina_perusahaan->no_pmku : '';?>
                <?php echo Form::hidden('no_pmku', $placeholderNoPmku, ['class'=> 'form-control','placeholder'=>$placeholderNoPmku]); ?>


                <?php if($errors->has('no_pmku')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('no_pmku')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('id') ? 'has-error' : ''); ?> m-input">
                <?php $placeholderId = $perusahaan->data? $perusahaan->data->ina_perusahaan->id : '';?>
                <?php echo Form::hidden('id', $placeholderId, ['class'=> 'form-control','placeholder'=>$placeholderId]); ?>


                <?php if($errors->has('id')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('id')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?> m-input">
                <label>Username yang Akan Digunakan<sup>*</sup></label>

                <?php echo Form::text('name', null, ['class'=> 'form-control']); ?>


                <?php if($errors->has('name')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('name')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('password') ? 'has-error' : ''); ?> m-input">
                <label>Password <sup>*</sup></label>

                <?php echo Form::password('password', ['class'=> 'form-control']); ?>


                <?php if($errors->has('password')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('password')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('password_confirmation') ? 'has-error' : ''); ?> m-input">
                <label>Ulangi Password <sup>*</sup></label>

                <?php echo Form::password('password_confirmation', ['class'=> 'form-control']); ?>


                <?php if($errors->has('password_confirmation')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('password_confirmation')); ?></span>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <label class="contact-label"><span>Data Perusahaan</span></label>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group <?php echo e($errors->has('badan_usaha') ? 'has-error' : ''); ?> m-input">
                <label>Badan Usaha <sup>*</sup></label>
                <?php echo Form::select('badan_usaha',$listBadanUsaha,null,['class'=>'selectpicker','title' => 'Pilih Badan Usaha ...','id'=>'badan-usaha-id','data-live-search'=>'true']); ?>

                <?php if($errors->has('badan_usaha')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('badan_usaha')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('jenis_usaha_id') ? 'has-error' : ''); ?> m-input">
                <label>Bidang Usaha <sup>*</sup></label>
                <?php $placeholderJenisUsaha = $perusahaan->data? $perusahaan->data->ina_perusahaan->kode_tipe_perusahaan : '';?>
                <?php echo Form::text('jenis_usaha_id', $placeholderJenisUsaha , ['class'=> 'form-control', 'placeholder' => $placeholderJenisUsaha ]); ?>

                <?php if($errors->has('jenis_usaha_id')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('jenis_usaha_id')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('nomor_siup') ? 'has-error' : ''); ?> m-input">
                <label>Nomor SIUP <sup>*</sup></label>
                <?php $placeholderSiup = $perusahaan->data ? $perusahaan->data->ina_perusahaan->nomor_siupal : '';?>
                <?php echo Form::text('nomor_siup', $placeholderSiup, ['class'=> 'form-control','placeholder'=>$placeholderSiup]); ?>


                <?php if($errors->has('nomor_siup')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('nomor_siup')); ?></span>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group <?php echo e($errors->has('tanggal_siup') ? 'has-error' : ''); ?> m-input">

                <label>Tanggal Terbit SIUP <sup>*</sup></label>
                <?php $placeholderTanggal = $perusahaan->data ? $perusahaan->data->system_response->tglIzin : 'yyyy-mm-dd';?>
                <input class="form-control" id="date" name="tanggal_siup" placeholder="<?php echo e($placeholderTanggal); ?>" type="text" value="<?php echo e($placeholderTanggal); ?>" />

                <?php if($errors->has('tanggal_siup')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('tanggal_siup')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('nama_perusahaan') ? 'has-error' : ''); ?> m-input">
                <label>Nama Perusahaan <sup>*</sup></label>
                <?php $placeholderNamaPerusahaan = $perusahaan->data ? $perusahaan->data->ina_perusahaan->nama_perusahaan : '';?>
                <?php echo Form::text('nama_perusahaan', $placeholderNamaPerusahaan, ['class'=> 'form-control','placeholder'=>$placeholderNamaPerusahaan]); ?>


                <?php if($errors->has('nama_perusahaan')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('nama_perusahaan')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('npwp') ? 'has-error' : ''); ?> m-input">
                <label>NPWP <sup>*</sup></label>
                <?php $placeholderNpwp = $perusahaan->data? $perusahaan->data->ina_perusahaan->npwp : '';?>
                <?php echo Form::text('npwp', $placeholderNpwp, ['class'=> 'form-control','placeholder'=>$placeholderNpwp]); ?>


                <?php if($errors->has('npwp')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('npwp')); ?></span>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group <?php echo e($errors->has('file_npwp') ? 'has-error' : ''); ?> m-input">
                <label>Upload NPWP Perusahaan <sup>*</sup></label>
                <?php echo Form::file('file_npwp',['class'=>'form-control-file']); ?>

                <?php if($errors->has('file_npwp')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('file_npwp')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('file_struktur') ? 'has-error' : ''); ?> m-input">
                <label>Upload Dokumen Struktur Organisasi Perusahaan <sup>*</sup></label>
                <input type="file" class="form-control-file" name="file_struktur">
                <?php if($errors->has('file_struktur')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('file_struktur')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('file_akta') ? 'has-error' : ''); ?> m-input">
                <label>Upload Dokumen Akta SIUP KUM HAM <sup>*</sup></label>
                <input type="file" class="form-control-file" name="file_akta">
                <?php if($errors->has('file_akta')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('file_akta')); ?></span>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-6">

            <div class="form-group <?php echo e($errors->has('file_domisili') ? 'has-error' : ''); ?> m-input">
                <label>Upload Dokumen Surat Keterangan Domisili <sup>*</sup></label>
                <input type="file" class="form-control-file" name="file_domisili">
                <?php if($errors->has('file_domisili')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('file_domisili')); ?></span>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <label class="contact-label"><span>Data Kantor</span></label>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group <?php echo e($errors->has('nomor_akta') ? 'has-error' : ''); ?> m-input">
                <label>No. Akta Pendirian Perusahaan <sup>*</sup></label>

                <?php echo Form::text('nomor_akta', null, ['class'=> 'form-control']); ?>


                <?php if($errors->has('nomor_akta')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('nomor_akta')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('wilayah_id') ? 'has-error' : ''); ?> m-input">
                <label>Wilayah Domisili Kantor <sup>*</sup></label>
                <?php echo Form::select('wilayah_id',App\Model\Wilayah::pluck('nama_wilayah','id'),null,['class'=>'selectpicker','title' => 'Domisili Kantor ...','id'=>'nama-wilayah-id','data-live-search'=>'true']); ?>

                <?php if($errors->has('wilayah_id')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('wilayah_id')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('alamat_perusahaan') ? 'has-error' : ''); ?> m-input">
                <label>Alamat <sup>*</sup></label>

                <?php echo Form::textarea('alamat_perusahaan', null, ['class'=> 'form-control','rows'=>3]); ?>


                <?php if($errors->has('alamat_perusahaan')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('alamat_perusahaan')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('fax') ? 'has-error' : ''); ?> m-input">
                <label>Fax</label>
                <?php $placeholderFax = $perusahaan->data? $perusahaan->data->system_response->nofax : '';?>
                <?php echo Form::text('fax', $placeholderFax, ['class'=> 'form-control','placeholder'=>$placeholderFax]); ?>


                <?php if($errors->has('fax')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('fax')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('hotline') ? 'has-error' : ''); ?> m-input">
                <label>Hotline <sup>*</sup></label>
                <?php $placeholderHotline = $perusahaan->data? $perusahaan->data->system_response->notelpon : '';?>
                <?php echo Form::text('hotline', $placeholderHotline, ['class'=> 'form-control','placeholder'=>$placeholderHotline]); ?>


                <?php if($errors->has('hotline')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('hotline')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('file_ktp') ? 'has-error' : ''); ?> m-input">
                <label>Upload KTP Penanggung Jawab <sup>*</sup></label>
                <input type="file" class="form-control-file" name="file_ktp">
                <?php if($errors->has('file_ktp')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('file_ktp')); ?></span>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-6">
                <div class="form-group <?php echo e($errors->has('tempat_kantor') ? 'has-error' : ''); ?> m-input">
                    <label>Tempat Kantor / Pemilik Usaha <sup>*</sup></label>
                    <?php echo Form::select('tempat_kantor',$listTempatKantor,null,['class'=>'selectpicker','title' => 'Pilih Kantor Pusat / Cabang ...','id'=>'tempat-kantor','data-live-search'=>'true']); ?>

                    <?php if($errors->has('tempat_kantor')): ?>
                    <span class="help-block badge badge-danger"><?php echo e($errors->first('tempat_kantor')); ?></span>
                    <?php endif; ?>
                </div>
            <div class="form-group <?php echo e($errors->has('telepon') ? 'has-error' : ''); ?> m-input">
                <label>Telepon Kantor <sup>*</sup></label>

                <?php echo Form::text('telepon', null, ['class'=> 'form-control']); ?>


                <?php if($errors->has('telepon')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('telepon')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('email') ? 'has-error' : ''); ?> m-input">
                <label>Email Kantor Perusahaan <sup>*</sup></label>
                <?php $placeholderEmail = $perusahaan->data? $perusahaan->data->system_response->email : '';?>

                <?php echo Form::text('email', $placeholderEmail, ['class'=> 'form-control', 'placeholder'=>$placeholderEmail]); ?>


                <?php if($errors->has('email')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('email')); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('penanggung_jawab') ? 'has-error' : ''); ?> m-input">
                <label>Nama Penanggung Jawab <sup>*</sup></label>

                <?php echo Form::text('penanggung_jawab', null, ['class'=> 'form-control']); ?>


                <?php if($errors->has('penanggung_jawab')): ?>
                <span class="help-block badge badge-danger"><?php echo e($errors->first('penanggung_jawab')); ?></span>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <small><sup style="color:red">*</sup> <i>Tidak boleh kosong</i></small>
    <div class="btn-box">
        <button type="submit" class="btn btn-blue"><span>Send Form</span></button>
        <a href="<?php echo e(url('/')); ?>" class="btn btn-warning"> Cancel</a>
    </div>
</div>