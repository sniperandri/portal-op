<?php $__env->startSection('pageTitle','Pengaturan Galeri Video'); ?>
<?php $__env->startSection('breadcrumb','Galeri Video'); ?>

<?php $__env->startSection('content'); ?>
	<div class="content-wrapper">
	<?php echo $__env->make('layouts.backend.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	    <!-- Main content -->
	    <section class="content">
	      <div class="container-fluid">
	        <div class="row">
	          <div class="col-md-12">
	            <div class="card card-info">
	              <!-- /.card-header -->
	              <div class="card-header">
	                <h3 class="card-title">Pengaturan Galeri Video</h3>
	                <div class="card-tools">

	                </div>
	              </div>
				  	

	              <!-- /.card-header -->
	              <div class="card-body p-1">
	              	<div class="row">
	              		<div class="col-md-12" style="padding-left: 10px; padding-right: 30px; padding-top: 10px; padding-bottom: 10px; ">
			                <?php echo $__env->make('backend.galeri-video.message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	              			<a href="<?php echo e(route('admin.galeri-video.create')); ?>" class="btn btn-info float-left">
	              			  <span>
	              			    <i class="fa fa-plus-circle"></i>
	              			    <span>
	              			      Tambah Video
	              			    </span>
	              			  </span>
	              			</a>

	              		</div>
	              	</div>

	                <?php if(! $galeriVideos->count()): ?>
	                  <div class="alert alert-danger">
	                    Data Tidak Ditemukan
	                  </div>
	                <?php else: ?>
	                    <?php if($onlyTrashed): ?>
	                      <?php echo $__env->make('backend.galeri-video.table-trash', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	                    <?php else: ?>
	                      <?php echo $__env->make('backend.galeri-video.table', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	                    <?php endif; ?>
	                <?php endif; ?>
	              </div>
	              <!-- /.card-body -->
	              <div class="card-footer clearfix">
	                <div class="clearfix">
	                    <?php echo e($galeriVideos->appends( Request::query() )->render()); ?>

	                  </div>
	                  <div class="pull-right">
	                    <small><?php echo e($galeriVideosCount); ?> <?php echo e(str_plural('Record', $galeriVideosCount)); ?></small>
	                  </div>
	              </div>

	            </div>
	            <!-- /.card -->
	          </div>

	        </div>
	        <!-- /.row -->
	      </div><!-- /.container-fluid -->
	    </section>
	    <!-- /.content -->
	  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>