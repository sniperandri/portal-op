<?php echo Form::model($model, ['url' => $form_url, 'method' => 'delete', 'class' => 'form-inline js-confirm', 'data-confirm' => $confirm_message]); ?>

<a href="<?php echo e($edit_url); ?>" class="btn btn-warning btn-sm btn-block">Edit</a>
<?php if (app('laratrust')->hasRole('superadmin')) : ?>
<?php echo Form::submit('Delete', ['class'=>'btn btn-sm btn-danger btn-block']); ?>

<?php endif; // app('laratrust')->hasRole ?>
<?php echo Form::close(); ?>