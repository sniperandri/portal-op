<table class="table table-striped">
  <tr>
    <th style="width: 10px">No</th>
    <th>Judul</th>
    <th>Konten</th>
    <th class="text-center">Gambar</th>
    <th class="text-center">Aksi</th>
  </tr>
  <?php $no = paging_number($perPage);?>
  <?php $__currentLoopData = $profils; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $profil): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  	<tr>
  	  <td><?php echo e($no); ?>.</td>
      <td><?php echo e($profil->title); ?></td>
      <td><?php echo substr(htmlspecialchars_decode(stripslashes($profil->konten)),0,250); ?> ...</td>
      <td align="center"><img src="<?php echo e(url($profil->gambar1)); ?>" height="70px" /></td>
      <td align="center">
        <?php echo Form::open(['method' => 'DELETE', 'route' => ['admin.profil.destroy', $profil->id]]); ?>

        <a href="<?php echo e(route('admin.profil.edit', $profil->id)); ?>" class="btn btn-sm btn-warning btn-block" title="Edit">
          <i class="fa fa-edit text-black"></i> Edit
        </a>
        <?php if (app('laratrust')->hasRole('superadmin')) : ?>
          <button onclick="return confirm('Apakah Anda yakin untuk menghapus data?')" type="submit" class="btn btn-sm btn-danger btn-block" title="Move to Trash">
            <i class="fa fa-trash"></i> Hapus
          </button>
        <?php endif; // app('laratrust')->hasRole ?>
        <?php echo Form::close(); ?>

      </td>
  	</tr>
    <?php $no++;?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>