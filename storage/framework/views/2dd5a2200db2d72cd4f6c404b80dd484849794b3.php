<div class="form-group<?php echo e($errors->has('kode') ? ' is-invalid' : ''); ?>">
<?php echo Form::label('Kode Laporan'); ?> 
<?php
    if($errors->has('kode')){
        $invalid = 'form-control is-invalid';
    }else{
        $invalid = 'form-control';
    }
?>
<?php echo Form::text('kode', null, ['class'=>$invalid, 'placeholder' => 'Kode Laporan']); ?>

<?php echo $errors->first('kode', '<p class="invalid-feedback">:message</p>'); ?>

</div>

<div class="form-group<?php echo e($errors->has('nama') ? ' is-invalid' : ''); ?>">
<?php echo Form::label('Nama Laporan'); ?> 
<?php
    if($errors->has('nama')){
        $invalid = 'form-control is-invalid';
    }else{
        $invalid = 'form-control';
    }
?>
<?php echo Form::text('nama', null, ['class'=>$invalid, 'placeholder' => 'Nama Laporan']); ?>

<?php echo $errors->first('nama', '<p class="invalid-feedback">:message</p>'); ?>

</div>

<div class="form-group<?php echo e($errors->has('keterangan') ? ' is-invalid' : ''); ?>">
<?php echo Form::label('Keterangan Laporan'); ?> 
<?php
    if($errors->has('keterangan')){
        $invalid = 'form-control is-invalid';
    }else{
        $invalid = 'form-control';
    }
?>
<?php echo Form::textarea('keterangan', null, ['class'=> 'form-control','rows'=>8, 'placeholder' => '']); ?>

<?php echo $errors->first('keterangan', '<p class="invalid-feedback">:message</p>'); ?>

</div>

<?php echo Form::submit('Simpan', ['class'=>'btn btn-info']); ?>