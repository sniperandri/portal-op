<p class="breadcrumb-list">
    <span><a href="#">Portal OP</a></span>
    <span><a href="#">Profil</a></span>
    <span>Visi Misi Otoritas Pelabuhan</span>
</p>
<div class="news-desc">
    <article>
        <div class="news-header">
            <h2>Visi dan Misi <small>Visi dan Misi Kantor Otoritas Pelabuhan Utama Tanjung Priok</small></h2>
        </div>
        <div class="news-img">
            <?php if(!empty($visimisi->gambar1)): ?>
            <img src="<?php echo e(url($visimisi->gambar1)); ?>" width="100%" /><br>
            <?php endif; ?>
            <?php if(!empty($visimisi->gambar2)): ?>
            <img src="<?php echo e(url($visimisi->gambar1)); ?>" width="100%" /><br>
            <?php endif; ?>
            <?php if(!empty($visimisi->gambar3)): ?>
            <img src="<?php echo e(url($visimisi->gambar1)); ?>" width="100%" /><br>
            <?php endif; ?>
            <?php if(!empty($visimisi->gambar4)): ?>
            <img src="<?php echo e(url($visimisi->gambar1)); ?>" width="100%" /><br>
            <?php endif; ?>
            <?php if(!empty($visimisi->gambar5)): ?>
            <img src="<?php echo e(url($visimisi->gambar1)); ?>" width="100%" /><br>
            <?php endif; ?>
        </div>
        <?php echo htmlspecialchars_decode(stripslashes($visimisi->konten)); ?>

    </article>
</div>