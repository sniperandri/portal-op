<table class="table table-striped">
  <tr>
    <th style="width: 10px">No</th>
    <th>Judul</th>
    <th>Kategori</th>
    <th>Body</th>
    <th>Dilihat</th>
    <th>Tanggal Publikasi</th>
    <th class="text-center">Aksi</th>
  </tr>
  <?php $no = paging_number($perPage);?>
  <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  	<tr>
  	  <td><?php echo e($no); ?>.</td>
  	  <td><?php echo e($post->title); ?></td>
      <td><?php echo e($post->category->title); ?></td>
      <td><?php echo substr(htmlspecialchars_decode(stripslashes($post->body)),0,250); ?> ...</td>
      <td align="center"><?php echo e($post->view_count); ?> kali</td>
      <td><?php echo e($post->date); ?></td>
      <td align="center">
        <?php echo Form::open(['method' => 'DELETE', 'route' => ['admin.berita.destroy', $post->id]]); ?>

        <a href="<?php echo e(route('admin.berita.edit', $post->id)); ?>" class="btn btn-sm btn-warning btn-block" title="Edit">
          <i class="fa fa-edit text-black"></i> Edit
        </a>
          <button onclick="return confirm('Apakah Anda yakin untuk menghapus berita?')" type="submit" class="btn btn-sm btn-danger btn-block" title="Move to Trash">
            <i class="fa fa-trash"></i> Hapus
          </button>
        <?php echo Form::close(); ?>

      </td>
  	</tr>
    <?php $no++;?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>