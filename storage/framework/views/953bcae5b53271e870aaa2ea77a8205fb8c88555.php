<table class="table table-striped">
  <tr>
    <th style="width: 10px">No</th>
    <th>Nama Instansi</th>
    <th>Deskripsi</th>
    <th>Logo</th>
    <th class="text-center">Aksi</th>
  </tr>
  <?php $no = paging_number($perPage);?>
  <?php $__currentLoopData = $linkTerkaits; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $linkTerkait): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  	<tr>
  	  <td><?php echo e($no); ?>.</td>
  	  <td align="center"><?php echo e($linkTerkait->nama_instansi); ?></td>
  	  <td align="center"><?php echo e($linkTerkait->deskripsi); ?></td>
  	  <td align="center"><?php echo e($linkTerkait->logo_instansi); ?></td>
      <td align="center" ="center">
        <?php echo Form::open(['style'=>'display:inline-block;', 'method' => 'PUT', 'route' => ['admin.link-terkait.restore', $linkTerkait->id]]); ?>

          <button title="Restore" class="btn btn-warning btn-sm btn-block">
              <i class="fa fa-refresh text-black"></i> Restore
          </button>
        <?php echo Form::close(); ?>

        <?php echo Form::open(['style'=>'display:inline-block;', 'method' => 'DELETE', 'route' => ['admin.link-terkait.force-destroy', $linkTerkait->id]]); ?>

          <button title="Permanent Delete" onclick="return confirm('You are about to delete data permanently. Are you sure?')" type="submit" class="btn btn-danger btn-sm btn-block">
            <i class="fa fa-times-circle"></i> Hapus Permanen
          </button>
        <?php echo Form::close(); ?>

      </td>
  	</tr>
    <?php $no++;?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>