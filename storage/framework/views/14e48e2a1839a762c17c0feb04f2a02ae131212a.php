<div class="col-md-3">
    <div class="sidebar-list">
        <div class="sidebar-box sidebar-search">
            <form action="<?php echo e(route('berita.index')); ?>">
            <div class="input-group">
                <input type="text" class="form-control" name="term" value="<?php echo e(request('term')); ?>" placeholder="Cari Berita" />
                <div class="input-group-append">
                    <button class="btn btn-blue" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
            </form>
        </div>
        <div class="sidebar-box sidebar-news">
            <label>Berita Terbaru</label>
            <ul class="list">
                <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $new): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li>
                    <h6><a href="<?php echo e(route('berita.show', $new->slug)); ?>"><?php echo e($new->title); ?></a></h6>
                    <small><i class="fa fa-clock-o"></i> <?php echo e($new->date); ?></small>
                </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
        <div class="sidebar-box sidebar-news">
            <label>Berita Terpopuler</label>
            <ul class="list">
                <?php $__currentLoopData = $populer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pop): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li>
                    <h6><a href="<?php echo e(route('berita.show', $pop->slug)); ?>"><?php echo e($pop->title); ?></a></h6>
                    <small><i class="fa fa-eye"></i>Dilihat <?php echo e($pop->view_count); ?> kali</small>
                </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
        <div class="sidebar-box sidebar-news">
            <label>Kategori Berita</label>

            <div class="sidebar-box sidebar-nav">
                <ul class="list">
                    <?php $__currentLoopData = $kats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                        $url = url()->current();
                    ?>
                    <?php if(strpos($url, $kat->slug) !== false): ?>
                    <li class="active">
                    <?php else: ?>
                    <li>
                    <?php endif; ?>
                        <a href="<?php echo e(route('frontend.berita.kategori', $kat->slug)); ?>"><?php echo e($kat->title); ?></a></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
        </div>
    </div>
</div>