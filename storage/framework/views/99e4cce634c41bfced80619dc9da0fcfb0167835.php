<table class="table table-striped">
  <tr>
    <th style="width: 10px">No</th>
    <th>Nama Instansi</th>
    <th>Deskripsi</th>
    <th>Logo</th>
    <th class="text-center">Aksi</th>
  </tr>
  <?php $no = paging_number($perPage);?>
  <?php $__currentLoopData = $informasis; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $informasi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  	<tr>
  	  <td><?php echo e($no); ?>.</td>
  	  <td align="center"><?php echo e($informasi->nama_instansi); ?></td>
  	  <td align="center"><?php echo e($informasi->deskripsi); ?></td>
  	  <td align="center"><?php echo e($informasi->gambar); ?></td>
      <td align="center" ="center">
        <?php echo Form::open(['style'=>'display:inline-block;', 'method' => 'PUT', 'route' => ['admin.informasi.restore', $informasi->id]]); ?>

          <button title="Restore" class="btn btn-warning btn-sm btn-block">
              <i class="fa fa-refresh text-black"></i> Restore
          </button>
        <?php echo Form::close(); ?>

        <?php echo Form::open(['style'=>'display:inline-block;', 'method' => 'DELETE', 'route' => ['admin.informasi.force-destroy', $informasi->id]]); ?>

          <button title="Permanent Delete" onclick="return confirm('You are about to delete data permanently. Are you sure?')" type="submit" class="btn btn-danger btn-sm btn-block">
            <i class="fa fa-times-circle"></i> Hapus Permanen
          </button>
        <?php echo Form::close(); ?>

      </td>
  	</tr>
    <?php $no++;?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>