<div class="col-md-12" style="padding-right: 50px;" id="ppid">
        <?php echo $__env->make('frontend.message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo Form::model($ppid, [
            'method' => 'POST',
            'route' => 'ppid.store',
            'id' => 'frontend-ppid-form',
            'files' => true,
        ]); ?>

        <div class="row">
            <p align="justify"><em>Sejak Undang-Undang Nomor 14 Tahun 2008 Tentang Keterbukaan Informasi Publik (UU KIP) diberlakukan secara efektif pada tanggal 30 April 2010 telah mendorong bangsa Indonesia satu langkah maju ke depan, menjadi bangsa yang transparan dan akuntabel dalam mengelola sumber daya publik. UU KIP sebagai instrumen hukum yang mengikat merupakan tonggak atau dasar bagi seluruh rakyat Indonesia untuk bersama-sama mengawasi secara langsung pelayanan publik yang diselenggarakan oleh Badan Publik.</em></p>
            <p align="justify"><em>Keterbukaan informasi adalah salah satu pilar penting yang akan mendorong terciptanya iklim transparansi. Terlebih di era yang serba terbuka ini, keinginan masyarakat untuk memperoleh informasi semakin tinggi. Diberlakukannya UU KIP merupakan perubahan yang mendasar dalam kehidupan bermasyarakat, berbangsa dan bernegara, oleh sebab itu perlu adanya kesadaran dari seluruh elemen bangsa agar setiap lembaga dan badan pemerintah dalam pengelolaan informasi harus dengan prinsip good governance, tata kelola yang baik dan akuntabilitas.</em></p>
            <p align="justify"><em>Sejalan dengan amanah Undang-Undang Nomor 14 Tahun 2008 tentang Keterbukaan Informasi Publik, Kementerian Perhubungan telah melakukan beberapa upaya menyelaraskan aspek legal dengan menetapkan Peraturan Menteri Perhubungan Nomor PM. 46 Tahun 2018 Tentang Pedoman Pengelolaan Informasi dan Dokumentasi di lingkungan Kementerian Perhubungan.</em></p>
            <div class="col-md-6">
                <div class="contact-form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group <?php echo e($errors->has('nama_lengkap') ? 'has-error' : ''); ?> m-input">
                                    <label>Nama Lengkap<sup>*</sup></label>

                                    <?php echo Form::text('nama_lengkap', null, ['class'=> 'form-control']); ?>


                                    <?php if($errors->has('nama_lengkap')): ?>
                                    <span class="help-block badge badge-danger"><?php echo e($errors->first('nama_lengkap')); ?></span>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group <?php echo e($errors->has('alamat') ? 'has-error' : ''); ?> m-input">
                                    <label>Alamat <sup>*</sup></label>

                                    <?php echo Form::textarea('alamat', null, ['class'=> 'form-control','rows'=>3]); ?>


                                    <?php if($errors->has('alamat')): ?>
                                    <span class="help-block badge badge-danger"><?php echo e($errors->first('alamat')); ?></span>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group <?php echo e($errors->has('pekerjaan') ? 'has-error' : ''); ?> m-input">
                                    <label>Pekerjaan<sup>*</sup></label>

                                    <?php echo Form::text('pekerjaan', null, ['class'=> 'form-control']); ?>


                                    <?php if($errors->has('pekerjaan')): ?>
                                    <span class="help-block badge badge-danger"><?php echo e($errors->first('pekerjaan')); ?></span>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group <?php echo e($errors->has('jenis_id') ? 'has-error' : ''); ?> m-input">
                                    <label>Jenis Identitas<sup>*</sup></label>

                                    <?php echo Form::select('jenis_id',['1' => 'KTP', '2' => 'NPWP'],null,['class'=>'selectpicker','title' => 'Pilih jenis identitas ...','id'=>'identitas-id','data-live-search'=>'true']); ?>


                                    <?php if($errors->has('jenis_id')): ?>
                                    <span class="help-block badge badge-danger"><?php echo e($errors->first('jenis_id')); ?></span>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group <?php echo e($errors->has('nomor_id') ? 'has-error' : ''); ?> m-input">
                                    <label>Nomor Identitas<sup>*</sup></label>

                                    <?php echo Form::text('nomor_id', null, ['class'=> 'form-control']); ?>


                                    <?php if($errors->has('nomor_id')): ?>
                                    <span class="help-block badge badge-danger"><?php echo e($errors->first('nomor_id')); ?></span>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group <?php echo e($errors->has('email') ? 'has-error' : ''); ?> m-input">
                                    <label>Email <sup>*</sup></label>

                                    <?php echo Form::text('email', null, ['class'=> 'form-control']); ?>


                                    <?php if($errors->has('email')): ?>
                                    <span class="help-block badge badge-danger"><?php echo e($errors->first('email')); ?></span>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group <?php echo e($errors->has('telepon') ? 'has-error' : ''); ?> m-input">
                                    <label>Telepon<sup>*</sup></label>

                                    <?php echo Form::text('telepon', null, ['class'=> 'form-control']); ?>


                                    <?php if($errors->has('telepon')): ?>
                                    <span class="help-block badge badge-danger"><?php echo e($errors->first('telepon')); ?></span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group <?php echo e($errors->has('file_berkas') ? 'has-error' : ''); ?> m-input">
                                    <label>Unggah Berkas Identitas</label>
                                    <?php echo Form::file('file_berkas',['class'=>'form-control-file']); ?>

                                    <?php if($errors->has('file_berkas')): ?>
                                    <span class="help-block badge badge-danger"><?php echo e($errors->first('file_berkas')); ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="contact-form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group <?php echo e($errors->has('rincian_info') ? 'has-error' : ''); ?> m-input">
                                    <label>Rincian Informasi yang Dibutuhkan<sup>*</sup></label>

                                    <?php echo Form::textarea('rincian_info', null, ['class'=> 'form-control','rows'=>8, 'placeholder' => 'Rincian Informasi yang Dibutuhkan']); ?>


                                    <?php if($errors->has('rincian_info')): ?>
                                    <span class="help-block badge badge-danger"><?php echo e($errors->first('rincian_info')); ?></span>
                                    <?php endif; ?>
                                </div>
                                
                                <div class="form-group <?php echo e($errors->has('tujuan_info') ? 'has-error' : ''); ?> m-input">
                                    <label>Tujuan Penggunaan Informasi<sup>*</sup></label>

                                    <?php echo Form::textarea('tujuan_info', null, ['class'=> 'form-control','rows'=>6, 'placeholder' => 'Tujuan Penggunaan Informasi']); ?>


                                    <?php if($errors->has('tujuan_info')): ?>
                                    <span class="help-block badge badge-danger"><?php echo e($errors->first('tujuan_info')); ?></span>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group <?php echo e($errors->has('cara_info') ? 'has-error' : ''); ?> m-input">
                                    <label>Cara Memperoleh Informasi </label>
                                    <br>
                                    <?php echo Form::checkBox('cara_info[]',1); ?> Melihat / Membaca / Mendengarkan / Mencatat
                                    <br>
                                    <?php echo Form::checkBox('cara_info[]',2); ?> Mendapatkan Copy Salinan (hard copy)
                                    <br>
                                    <?php echo Form::checkBox('cara_info[]',3); ?> Mendapatkan Soft Copy
                                    <?php if($errors->has('cara_info')): ?>
                                    <span class="help-block"><?php echo e($errors->first('cara_info')); ?></span>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group <?php echo e($errors->has('cara_salinan') ? 'has-error' : ''); ?> m-input">
                                    <label>Cara Mendapatkan Salinan <sup>*</sup> </label>
                                    <br>
                                    <?php echo Form::radio('cara_salinan',1); ?> Mengambil Langsung
                                    <br>
                                    <?php echo Form::radio('cara_salinan',2); ?> Kurir
                                    <br>
                                    <?php echo Form::radio('cara_salinan',3); ?> POS
                                    <br>
                                    <?php echo Form::radio('cara_salinan',4); ?> Faksimili
                                    <br>
                                    <?php echo Form::radio('cara_salinan',5); ?> Email
                                    <?php if($errors->has('cara_salinan')): ?>
                                    <span class="help-block"><?php echo e($errors->first('cara_salinan')); ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>	                            
                </div>
            </div>
            <div class="col-md-12 contact-form">
                <small><sup style="color:red;">*</sup> <i>Tidak boleh kosong</i></small>
                <br><small> <i>Dengan mengklik tombol "Kirim Permohonan" di bawah, saya menyatakan bahwa informasi yang saya isikan diatas adalah BENAR. Pengisian yang salah dan tidak lengkap akan menyebabkan permohonan data Anda TIDAK AKAN DIPROSES.</i></small>
                <div class="btn-box">
                    <button type="submit" class="btn btn-blue">Kirim Permohonan</button>
                    <button type="reset" class="btn btn-org">Cancel</button>
                </div>
            </div>
        </div>
    <?php echo Form::close(); ?>

</div>